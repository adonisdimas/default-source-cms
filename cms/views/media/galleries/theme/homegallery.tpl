<div class="gallery content-width-inner">
			<div class="row top-bottom-padding-small">
            	<div class="col-sm-4 col-xs-12 gallery-item small">
					<?php if ($gallery->popup == "None"): ?>
						<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[0]?>" alt="<?php echo $gallery->title;?>">
					<?php else: ?>
						<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$images[0]?>">
							<span></span>
							<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[0]?>" alt="<?php echo $gallery->title;?>">
						</a>
					<?php endif; ?>
            	</div>
            	<div class="col-sm-4 col-xs-12 gallery-item small">
					<?php if ($gallery->popup == "None"): ?>
						<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[1]?>" alt="<?php echo $gallery->title;?>">
					<?php else: ?>
						<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$images[1]?>">
							<span></span>
							<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[1]?>" alt="<?php echo $gallery->title;?>">
						</a>
					<?php endif; ?>
            	</div>
            	<div class="col-sm-4 col-xs-12 gallery-item small">
					<?php if ($gallery->popup == "None"): ?>
						<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[2]?>" alt="<?php echo $gallery->title;?>">
					<?php else: ?>
						<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$images[2]?>">
							<span></span>
							<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[2]?>" alt="<?php echo $gallery->title;?>">
						</a>
					<?php endif; ?>
            	</div>
            </div>
            <div class="row top-bottom-padding-small">
            	<div class="col-sm-6 gallery-item big">
					<?php if ($gallery->popup == "None"): ?>
						<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[3]?>" alt="<?php echo $gallery->title;?>">
					<?php else: ?>
						<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$images[3]?>">
							<span></span>
							<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[3]?>" alt="<?php echo $gallery->title;?>">
						</a>
					<?php endif; ?>
            	</div>
            	<div class="col-sm-6">
            		<div class="gallery-item big">	
						<?php if ($gallery->popup == "None"): ?>
							<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[4]?>" alt="<?php echo $gallery->title;?>">
						<?php else: ?>
							<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$images[4]?>">
								<span></span>
								<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[4]?>" alt="<?php echo $gallery->title;?>">
							</a>
						<?php endif; ?>
            		</div>	            								
            		<div class="button">
						<a class="border" href="gallery">VIEW MORE</a>
					</div>
            	</div>	            	
            </div>				
</div>	
