<div class="masonry-gallery row">
	<?php $j=1;foreach($images as $key => $image): ?>
		<div class="gallery-item">
			<?php if ($gallery->popup == "None"): ?>
				<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[$key]?>" alt="<?php echo $gallery->title;?>">
			<?php else: ?>
				<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$image?>">
					<span></span>
					<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR.'/'.$thumbnails[$key]?>" alt="<?php echo $gallery->title;?>">
				</a>
			<?php endif; ?>
		</div>
	<?php endforeach ?>
</div>