<!-- Slider main container -->
<div class="swiper-container">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        <?php foreach($slider[1] as $slider_item): ?>
        <div class="swiper-slide">
			<div data-caption="<?php echo $slider_item->caption;  ?>" data-background="<?php echo ROOT.'/'.SLIDERS_PATH.'/'.$slider[0]->title.'/'.$slider_item->media_path;  ?>" class="swiper-slide swiper-lazy" style="background-size:cover!important;">
			    <div class="swiper-lazy-preloader"></div>
			</div>
        </div>
        <?php endforeach ?>
    </div>
</div>