<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Username</th>
		        <th>Email</th>
		        <th>Role</th>
		        <th>Status</th>
		        <th>Operations</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->username?></td>
			        <td><?=$entity->email?></td>
			        <td><?=$entity->role?></td>
			        <td><?=$entity->status?></td>
			        <td><a href="users/edit/<?=$entity->id?>">Edit</a> <?php if(count($entities)>1): ?>| <a onclick = "if (! confirm('Continue?')) { return false; }" href="users/delete/<?=$entity->id?>">Delete</a><?php endif; ?></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No Users Created yet. Try adding a new <a href="users/add">one</a></h3>
		  <?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/users/update/<?=$entity->id?>">
	     <input type="text" value="<?=$entity->username?>" class="form-control" name="username" placeholder="Username" required="" />
	     <input type="password" value="" class="form-control" name="password" placeholder="Password" required=""/>      
	     <input type="email" value="<?=$entity->email?>" class="form-control" name="email" placeholder="Email" required="" autofocus="" />
	  	<select class="form-control" name="role" id="sel1">
		  	 <option value="admin" <?php if($entity->role =='admin'){ echo 'selected';}?>>Administrator</option>
		  	 <option value="editor" <?php if($entity->role =='editor'){ echo 'selected';}?>>Editor</option>
	 	</select>
	     <div class="checkbox">
		    <label>
		      <input name="status" type="checkbox" <?=$entity->status!= NULL ? 'checked' : ''?>> Status
		    </label>
		 </div>
	     <button class="btn btn-lg btn-primary btn-block" type="submit">Update User</button>   
	</form> 
<?php else:?>
 	<form class="create" method="post" action="insert">
	     <input type="text" class="form-control" name="username" placeholder="Username" required="" />
	     <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
	     <input type="email" class="form-control" name="email" placeholder="Email" required="" autofocus="" />
	  	 <select class="form-control" name="role" id="sel1">
	  	 	<option value="admin">Administrator</option>
			<option value="editor">Editor</option>
	 	 </select>
	     <div class="checkbox">
		    <label>
		      <input name="status" type="checkbox"> Status
		    </label>
		 </div>
	     <button class="btn btn-lg btn-primary btn-block" type="submit">Add User</button>   
	</form>
<?php endif; ?>
<?php $this->stop() ?>