<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<div class="table-responsive">          
			<?php if(isset($entities)): ?>          
		  	<table class="table paginated">
			    <thead>
			      <tr>
			        <th>Title</th>
			        <th>Value</th>
			        <th>Region Assigned</th>
			        <th>Language</th>
			      </tr>
			    </thead>
				<?php foreach($entities as $key => $page): ?>
					<tbody id="page<?=$key+1?>" style="<?php if($key!=0){echo 'display: none;';} ?>">
						<?php foreach($page as $entity): ?>	
						  <tr>
					        <td><?=$entity->title?></td>
					        <td><?=$entity->value?></td>
					        <td><?=$entity->region_title?></td>
					        <td><?=$entity->lang_id?></td>
					        <td><a href="translations/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="translations/delete/<?=$entity->id?>">Delete</a></td>
					      </tr>							
						<?php endforeach ?>
					</tbody>
				<?php endforeach ?>								    
			</table>
			<div class="text-center">
				<?php if(count($entities)>1): ?>
				  <ul class="pagination">
				  	<?php foreach($entities as $key => $page): ?>
				    	<li class="<?php if($key==0){echo 'active';} ?>"><a href="#page<?=$key+1?>"><?=$key+1?></a></li>
				    <?php endforeach ?>
				  </ul>
			  	<?php endif; ?>
		  	</div>
		  <?php else: ?>
		  	<h2>No translations Created yet. Try adding a new <a href="translations/add">one</a></h2>
		  <?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/translations/update/<?=$entity->id?>">
	     <div class="row">
	      	<div class="col-md-6">
	     		<input type="text" class="form-control" value="<?=$entity->title?>" name="title" placeholder="Title" required="" />
	     	</div>
	      	<div class="col-md-6">
				<select class="form-control" name="region_title" id="sel1">
					<option>None</option>
					<?php foreach($regions as $key => $region): ?>
						<option value="<?=$region->title?>" <?php if($region->title ==$entity->region_title){ echo 'selected';}?>><?=$region->title?></option>
					<?php endforeach ?>
				</select>	     		
	     	</div>
	     </div>
	     <div class="row">
		     <div class="col-md-6">
		     	<input type="text" class="form-control" value="<?=$entity->value?>" name="value" placeholder="Value" required="" />
		     </div>
	 		<div class="col-md-6">
				<select class="form-control" name="lang_id" id="sel1">
				  	<?php foreach($languages as $key => $language): ?>
		    		<option value="<?=$language->id?>" <?php if($language->id ==$entity->lang_id){ echo 'selected';}?>><?=$language->title?></option>
		    		<?php endforeach ?>
		  		</select>
			 </div>	     
		 </div>	  
		 <div class="row"> <div class="col-md-12">
	    	 <button class="btn btn-lg btn-primary btn-block" type="submit">Update Translation</button> 
	    	 </div>  
	     </div>
	</form> 
<?php else:?>
 	<form class="create" method="post" action="insert">
	     <div class="row">
	      	<div class="col-md-6">
	     		<input type="text" class="form-control" name="title" placeholder="Title" required="" />
	     	</div>
	      	<div class="col-md-6">
				<select class="form-control" name="region_title" id="sel1">
			  	<option>None</option>
			  	<?php foreach($regions as $key => $region): ?>
		    	<option value="<?=$region->title?>"><?=$region->title?></option>
		    	<?php endforeach ?>
				 </select>	     		
	     	</div>	     	
	     </div>
	     <div class="row">
		     <div class="col-md-6">
		     	<input type="text" class="form-control" name="value" placeholder="Value" required="" />
		     </div>
	 		<div class="col-md-6">
					  <select class="form-control" name="lang_id" id="sel1">
					  	<?php foreach($languages as $key => $language): ?>
			    		<option value="<?=$language->id?>"><?=$language->title?></option>
			    		<?php endforeach ?>
			  		</select>
			 </div>	     
		 </div>
		 <div class="row">
		 	<div class="col-md-12">
	     		<button class="btn btn-lg btn-primary btn-block" type="submit">Add Translation</button>  
	     	</div> 
	     </div>
	</form>
<?php endif; ?>
<?php $this->stop() ?>