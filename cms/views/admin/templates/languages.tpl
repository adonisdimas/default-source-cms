<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Locale</th>
		        <th>Title</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->locale?></td>
			        <td><?=$entity->title?></td>
			        <td><a href="languages/edit/<?=$entity->id?>">Edit</a> <?php if(count($entities)>1): ?>| <a onclick = "if (! confirm('Continue?')) { return false; }" href="languages/delete/<?=$entity->id?>">Delete</a><?php endif; ?></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No Languages Created yet. Try adding a new <a href="languages/add">one</a></h3>
		  <?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/languages/update/<?=$entity->id?>">
	     <input type="text" value="<?=$entity->locale?>" class="form-control" name="locale" placeholder="locale" required="" />
	     <input type="text" value="<?=$entity->title?>" class="form-control" name="title" placeholder="title" required="" />
	     <button class="btn btn-lg btn-primary btn-block" type="submit">Update Language</button>   
	</form> 
<?php else:?>
 	<form class="create" method="post" action="insert">
	     <input type="text" class="form-control" name="locale" placeholder="locale" required="" />
	     <input type="text" class="form-control" name="title" placeholder="title" required="" />
	     <button class="btn btn-lg btn-primary btn-block" type="submit">Add Language</button>   
	</form>
<?php endif; ?>
<?php $this->stop() ?>