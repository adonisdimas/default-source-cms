<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Type</th>
		        <th>Pop up</th>
		        <th>Operations</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><?=$entity->type?></td>
			        <td><?=$entity->popup?></td>
			        <td><a href="galleries/edit/<?=$entity->id?>">Edit</a> | <a href="galleries/media/<?=$entity->id?>">Browse Media</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="galleries/delete/<?=$entity->id?>">Delete</a></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
	<?php else: ?>
	  	<h3>No Galleries Created yet. Try adding a new <a href="galleries/add">one</a></h3>
	<?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/galleries/update/<?=$entity->id?>">
 		<div class="row">
	 		<div class="col-md-6">
	 		  <label for="title">Title (Must be unique, a folder with the same name will be created for the gallery media):</label>
	 			<input type="text" class="form-control" name="title" value="<?=$entity->title?>" placeholder="Title" required="" disabled/>
	 		</div>
			<div class="col-md-6">
					 <label for="sel1">Style Type:</label>
			  		<select class="form-control selectpicker" name="type" id="sel1">
					 <?php foreach($styles as $style): ?>
						<option <?php if($style == $entity->type){ echo 'selected';}?>><?=$style?></option>
				 	 <?php endforeach ?>
			 	</select>		 
			</div>
		 	<br />
			<div class="col-md-6">
				<label for="sel2">Pop Up:</label>
			  		<select class="form-control selectpicker" name="popup" id="sel2">
					 <?php foreach($popup as $pop): ?>
						<option <?php if($pop ==$entity->popup){ echo 'selected';}?>><?=$pop?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>
			<div class="col-md-6">
				<label for="sel3">Assign to pages:</label>
			  	<select class="form-control selectpicker" name="page_titles[]" id="sel3" multiple>
					 <?php foreach($pages as $page): ?>
						<option <?php if(strpos($entity->page_titles,$page->title)!== false){ echo 'selected';}?>><?=$page->title?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>		
		  <div class="col-md-12"><button class="btn btn-lg btn-primary btn-block" type="submit">Update Gallery</button></div>   
		</div>
	</form> 
<?php else:?>
 	<form class="create" method="post" action="insert">
 		<div class="row">
	 		<div class="col-md-6">
	 		  <label for="title">Title (Must be unique, a folder with the same name will be created for the gallery media):</label>
	 			<input type="text" class="form-control" name="title" placeholder="Title" required="" />
	 		</div>
			<div class="col-md-6">
					 <label for="sel1">Style Type:</label>
			  		<select class="form-control selectpicker" name="type" id="sel1">
					 <?php foreach($styles as $style): ?>
						<option><?=$style?></option>
				 	 <?php endforeach ?>
			 	</select>		 
			</div>
		 	<br />
			<div class="col-md-6">
				<label for="sel2">Pop Up:</label>
			  		<select class="form-control selectpicker" name="popup" id="sel2">
					 <?php foreach($popup as $pop): ?>
						<option><?=$pop?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>
			<div class="col-md-6">
				<label for="sel3">Assign to pages:</label>
			  	<select class="form-control selectpicker" name="page_titles[]" id="sel3" multiple>
					 <?php foreach($pages as $page): ?>
						<option ><?=$page->title?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>		
		  <div class="col-md-12"><button class="btn btn-lg btn-primary btn-block" type="submit">Add Gallery</button></div>   
		</div> 
	</form>
<?php endif; ?>
<?php $this->stop() ?>