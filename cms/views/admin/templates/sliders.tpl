<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Type</th>
		        <th>Operations</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><?=$entity->type?></td>
			        <td><a href="<?=ROOT.ADMIN_PATH?>/slider_items/edit/<?=$entity->id?>">Add/Edit Slider Items</a> | <a href="sliders/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="sliders/delete/<?=$entity->id?>">Delete</a></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
	<?php else: ?>
	  	<h3>No sliders Created yet. Try adding a new <a href="sliders/add">one</a></h3>
	<?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/sliders/update/<?=$entity->id?>">
 		<div class="row">
	 		<div class="col-md-12">
	 		  <label for="title">Slider Title (Must be unique, a folder with the same name will be created for the slider media):</label>
			  <input type="text" class="form-control" name="title" value="<?=$entity->title?>" placeholder="Title" required="" disabled/>
	 		</div>
			<div class="col-md-6">
					 <label for="sel1">Slider Type:</label>
			  		<select class="form-control selectpicker" name="type" id="sel1">
					 <?php foreach($styles as $style): ?>
						<option <?php if($style == $entity->type){ echo 'selected';}?>><?=$style?></option>
				 	 <?php endforeach ?>
			 	</select>		 
			</div>
		 	<br />
			<div class="col-md-6">
				<label for="sel3">Assign to pages:</label>
			  	<select class="form-control selectpicker" name="page_titles[]" id="sel3" multiple>
					 <?php foreach($pages as $page): ?>
						<option <?php if(strpos($entity->page_titles,$page->title)!== false){ echo 'selected';}?>><?=$page->title?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>		
		  <div class="col-md-12"><button class="btn btn-lg btn-primary btn-block" type="submit">Update Slider</button></div>   
		</div>
	</form>
<?php elseif (isset($slider_items)):?>
 	<form class="create" method="post" action="<?=ROOT.ADMIN_PATH?>/slider_items/insert" enctype="multipart/form-data">
 		<div class="row">
		  <div class="col-md-12">
	 		<ul class="nav nav-tabs" role="tablist">
		 		<?php foreach($languages as $key => $language): ?>
		 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
					<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
				</li>
	 			<?php endforeach ?>
 			</ul>
			<div class="tab-content">
 			<?php foreach($languages as $key => $language): ?>
 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
		     	<div class="auto-elements">
		     		<div class="elements sortable">
		     		<?php if(count($slider_items[$key])<=0): ?>
			      	<div class="row after-add-more added-element grabbable">
			         	<div class="col-md-6">
				            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][title]" placeholder="Title" required/>
				        </div>
				        <div class="col-md-6">
				            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][caption]" placeholder="Caption" />
				        </div>
				        <div class="col-md-12">
						    <div class="form-group">
						        <label>Slider Image</label>
						        <div class="input-group">
						            <span class="input-group-btn">
						                <span class="btn btn-primary btn-file">
						                    Browse <i class="glyphicon glyphicon-folder-open"></i><input type="file" class="imgInp" name="slider_item[<?=$language->id?>][][media_path]" accept=".png, .jpg, .jpeg">
						                </span>
						            </span>
						            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][media_path]" readonly>
						        </div>
						        <img class='img-upload' style="width:250px;"/>
						    </div>
				        </div>
			        </div>
					<?php else:?>
			     		<?php foreach($slider_items[$key] as $index => $slider_item): ?>
				      	<div class="row after-add-more added-element grabbable">
				         	<div class="col-md-6">
					            <input type="text" class="form-control" value="<?=$slider_item->title?>" name="slider_item[<?=$language->id?>][][title]" placeholder="Title" />
					        </div>
					        <div class="col-md-6">
					            <input type="text" class="form-control" value="<?=$slider_item->caption?>" name="slider_item[<?=$language->id?>][][caption]" placeholder="Caption" />
					        </div>
					        <div class="col-md-12">
							    <div class="form-group">
						        <label>Slider Image</label>
							        <div class="input-group">
							            <span class="input-group-btn">
							                <span class="btn btn-primary btn-file">
							                    Browse <i class="glyphicon glyphicon-folder-open"></i><input type="file" class="imgInp" name="slider_item[<?=$language->id?>][][media_path]" accept=".png, .jpg, .jpeg">
							                </span>
							            </span>
							            <input type="text" class="form-control" value="<?=$slider_item->media_path?>" name="slider_item[<?=$language->id?>][][media_path]" readonly>
							        </div>
							        <img class='img-upload' src="<?=SUBFOLDER.'/'.SLIDERS_PATH.'/'.$slider->title.'/'.$slider_item->media_path?>" style="width:250px;"/>
						   	 	</div>
					        </div>								               
				        </div>
				       <?php endforeach ?>									
					<?php endif; ?>
				   </div>
			        <!-- Copy Fields -->
			        <div class="copy hide">
			          <div class="row added-element grabbable" style="margin:5px 0">
			         	<div class="col-md-6">
				            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][title]" placeholder="Title"/>
				        </div>
				        <div class="col-md-6">
				            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][caption]" placeholder="Caption" />
				        </div>
				        <div class="col-md-12">
						    <div class="form-group">
						        <label>Slider Image</label>
						        <div class="input-group">
						            <span class="input-group-btn">
						                <span class="btn btn-primary btn-file">
						                    Browse <i class="glyphicon glyphicon-folder-open"></i> <input type="file" class="imgInp" name="slider_item[<?=$language->id?>][][media_path]" accept=".png, .jpg, .jpeg">
						                </span>
						            </span>
						            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][media_path]" readonly>
						        </div>
						        <img class='img-upload' style="width:250px;"/>
						    </div>
				        </div>         
			          </div>
			        </div>
		     	</div>
		     	<div class="input-group-btn"> 
					<button class="btn btn-success add-btn add-slider" type="button"><i class="glyphicon glyphicon-plus"></i> Add Slider Item</button>
					<button class="btn btn-danger remove-btn" type="button"><i class="glyphicon glyphicon-minus"></i> Remove Slider Item</button>
				</div>
 			</div>
 			<?php endforeach ?>
			</div>
			</br>
    		<button class="btn btn-lg btn-primary btn-block" type="submit">Update Slider Items</button>  	
    		<input type="hidden" class="form-control" name="slider_id" value="<?=$slider->id?>" /> 
    		<input type="hidden" class="form-control" name="slider_title" value="<?=$slider->title?>" /> 			  
		  </div>		
		</div> 
	</form> 
<?php else:?>
 	<form class="create" method="post" action="insert" enctype="multipart/form-data">
 		<div class="row">
	 		<div class="col-md-12">
	 		  <label for="title">Slider Title (Must be unique, a folder with the same name will be created for the Slider media):</label>
	 		  <input type="text" class="form-control" name="title" placeholder="Title" required="" />
	 		</div>
			<div class="col-md-6">
					 <label for="sel1">Slider Type:</label>
			  		<select class="form-control selectpicker" name="type" id="sel1">
					 <?php foreach($styles as $style): ?>
						<option><?=$style?></option>
				 	 <?php endforeach ?>
			 	</select>		 
			</div>
		 	<br />
			<div class="col-md-6">
				<label for="sel3">Assign to pages:</label>
			  	<select class="form-control selectpicker" name="page_titles[]" id="sel3" multiple>
					 <?php foreach($pages as $page): ?>
						<option ><?=$page->title?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>
		  <div class="col-md-12">
	 		<ul class="nav nav-tabs" role="tablist">
	 		<?php foreach($languages as $key => $language): ?>
		 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
					<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
				</li>
	 		<?php endforeach ?>
	 		</ul>
			<div class="tab-content">
	 		<?php foreach($languages as $key => $language): ?>
	 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
			     	<div class="auto-elements">
				      	<div class="elements sortable">
					      	<div class="row after-add-more added-element grabbable">
					         	<div class="col-md-6">
						            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][title]" placeholder="Title" required/>
						        </div>
						        <div class="col-md-6">
						            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][caption]" placeholder="Caption" />
						        </div>
						        <div class="col-md-12">
								    <div class="form-group">
								        <label>Slider Image</label>
								        <div class="input-group">
								            <span class="input-group-btn">
								                <span class="btn btn-primary btn-file">
								                    Browse <i class="glyphicon glyphicon-folder-open"></i><input type="file" class="imgInp" name="slider_item[<?=$language->id?>][][media_path]" accept=".png, .jpg, .jpeg">
								                </span>
								            </span>
								            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][media_path]" readonly>
								        </div>
								        <img class='img-upload' style="width:250px;"/>
								    </div>
						        </div>
					        </div>
				        </div>
				        <!-- Copy Fields -->
				        <div class="copy hide">
				          <div class="row added-element grabbable" style="margin:5px 0">
				         	<div class="col-md-6">
					            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][title]" placeholder="Title"/>
					        </div>
					        <div class="col-md-6">
					            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][caption]" placeholder="Caption" />
					        </div>
					        <div class="col-md-12">
							    <div class="form-group">
							        <label>Slider Image</label>
							        <div class="input-group">
							            <span class="input-group-btn">
							                <span class="btn btn-primary btn-file">
							                    Browse <i class="glyphicon glyphicon-folder-open"></i> <input type="file" class="imgInp" name="slider_item[<?=$language->id?>][][media_path]" accept=".png, .jpg, .jpeg">
							                </span>
							            </span>
							            <input type="text" class="form-control" name="slider_item[<?=$language->id?>][][media_path]" readonly>
							        </div>
							        <img class='img-upload' style="width:250px;"/>
							    </div>
					        </div>         
				          </div>
				        </div>
			     	</div>
			     	<div class="input-group-btn"> 
						<button class="btn btn-success add-btn add-slider" type="button"><i class="glyphicon glyphicon-plus"></i> Add Slider Item</button>
						<button class="btn btn-danger remove-btn" type="button"><i class="glyphicon glyphicon-minus"></i> Remove Slider Item</button>
					</div>
	 			</div>
	 		<?php endforeach ?>
			</div>
			</br>
		    <button class="btn btn-lg btn-primary btn-block" type="submit">Add Slider</button>  			  
		  </div>		
		</div> 
	</form>
<?php endif; ?>
<?php $this->stop() ?>