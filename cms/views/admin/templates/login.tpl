<?php $this->layout('layout', ['title' => $this->e($title),'locale' => $this->e($locale)]) ?>
<?php $this->start('head') ?>
<link href="<?=ADMIN_ASSETS?>/css/login.css" rel="stylesheet">
<?php $this->stop() ?>
<?php $this->start('main-content') ?>
 <div class="wrapper">
 	<div class="login">
		 <?php if (isset($_GET['user'])): ?>
		        <?php if ($_GET['user']==1): ?>
		        <div class="alert alert-success">
				  	<strong>New user created succesfully!</strong> 
				</div>
		        <?php elseif($_GET['user']==0):?>
		          <div class="alert alert-warning">
					 <strong>Username already exists!</strong></br> Please try again. 
				  </div>		        
		        <?php else:?>
		          <div class="alert alert-danger">
					  <strong>An error occured.</strong> </br> Please try again. 
				  </div>			        
		        <?php endif; ?>
		 <?php endif; ?>
       <ul class="nav nav-tabs">
             <li class="active"><a href="#login" data-toggle="tab">Login</a></li>
       </ul>
       <div class="tab-content">
           <div class="tab-pane active in" id="login">            
			   <form class="signin" method="post" action="<?=ROOT.ADMIN_PATH?>/authenticate">       
			     <h2 class="signin-heading"><i class="fa fa-cube" aria-hidden="true"></i> DProject CMS</h2>
			    <?php if (isset($_GET['error'])): ?>
				<div class="alert alert-danger">
				  <strong>Wrong Username or Password</strong></br> Please try again. 
				</div>
			 	<?php endif; ?>
			     <input type="text" class="form-control" name="username" placeholder="Username" required="" />
			     <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
			     <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>   
			   </form>                    
          </div>
          <div class="tab-pane fade" id="create-user">
 			   <form class="create-user" method="post" action="<?=ROOT.ADMIN_PATH?>/users/insert">
			     <h2 class="signin-heading">New User</h2>
			     <input type="text" class="form-control" name="username" placeholder="Username" required="" />
			     <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
			     <input type="email" class="form-control" name="email" placeholder="Email" required="" autofocus="" />
			     <button class="btn btn-lg btn-primary btn-block" type="submit">Add User</button>   
			     <input type="hidden" name="type" value="user"/>
			   </form>           
          </div>
       </div>
	</div>
 </div>
<?php $this->stop() ?>
<?php $this->start('scripts') ?>
<?php $this->stop() ?>