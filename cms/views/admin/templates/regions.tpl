<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><a href="regions/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="regions/delete/<?=$entity->id?>">Delete</a></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No Regions Created yet. Try adding a new <a href="regions/add">one</a></h3>
		  <?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/regions/update/<?=$entity->id?>">
	     <input type="text" value="<?=$entity->title?>" class="form-control" name="title" placeholder="title" required="" />
	     <button class="btn btn-lg btn-primary btn-block" type="submit">Update Region</button>   
	</form> 
<?php else:?>
 	<form class="create" method="post" action="insert">
	     <input type="text" class="form-control" name="title" placeholder="title" required="" />
	     <button class="btn btn-lg btn-primary btn-block" type="submit">Add Region</button>   
	</form>
<?php endif; ?>
<?php $this->stop() ?>