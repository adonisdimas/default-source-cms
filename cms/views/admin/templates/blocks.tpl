<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Classes</th>
		        <th>Order</th>
		        <th>Region Assigned</th>
		        <th>Operations</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><?=$entity->classes?></td>
			        <td><?=$entity->order?></td>
			        <td><?=$entity->region_title?></td>
			        <td><a href="blocks/edit/<?=$entity->id?>">Edit</a> <?php if($userrole == 'admin'):?>| <a onclick = "if (! confirm('Continue?')) { return false; }" href="blocks/delete/<?=$entity->id?>">Delete</a> <?php endif;?></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No Blocks Created yet. <?php if($userrole == 'admin'):?>Try adding a new <a href="blocks/add">one</a><?php endif;?></h3>
		  <?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/blocks/update/<?=$entity->id?>" enctype="multipart/form-data">
 		<div class="row">
 			<div class="col-md-6">
 	  			<input type="text" class="form-control" name="title" placeholder="Block Title" value="<?=$entity->title?>" required="" />
 	  		</div>
 			<div class="col-md-6"> 	  	
 	  			<input type="text" class="form-control" name="classes" placeholder="Block Classes" value="<?=$entity->classes?>" />		
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6">
				<label for="sel1">Assign Region:</label>
				  <select class="form-control" name="region_title" id="sel1">
				  	<option>None</option>
				  	<?php foreach($regions as $key => $region): ?>
				    	<option value="<?=$region->title?>" <?php if($region->title ==$entity->region_title){ echo 'selected';}?>><?=$region->title?></option>
				    <?php endforeach ?>
				  </select>
			</div>
			<div class="col-md-6">
				<label for="sel2">Assign Order(The smaller the higher):</label>
			  <select class="form-control" name="order" id="sel2">
			  	<?php $numbs = array(0,1,2,3,4,5,6,7,8,9,10);
		  	foreach($numbs as $num): ?>
			    	<option value="<?=$num?>" <?php if($num ==$entity->order){ echo 'selected';}?>><?=$num?></option>
			    <?php endforeach ?>
			  </select>			
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
		 		<ul class="nav nav-tabs" role="tablist">
		 		<?php foreach($languages as $key => $language): ?>
			 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
						<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
					</li>
		 		<?php endforeach ?>
		 		</ul>
				<div class="tab-content">
		 		<?php foreach($languages as $key => $language): ?>
		 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
		 				<div class="row">
		 					<div class="col-md-12">
				 				<div class="checkbox">
								    <label>
								      <input name="content[<?=$key?>][hide_title]" type="checkbox" <?=$contents[$key]->hide_title!= NULL ? 'checked' : ''?>> Hide title
								    </label>
				 				</div>	
			 				</div>
			 				<div class="col-md-6">
				     			<input type="text" class="form-control" name="content[<?=$key?>][title]" value="<?=$contents[$key]->title?>" placeholder="Title" />
				     		</div>
				     		<div class="col-md-6">
				     			<input type="text" class="form-control" name="content[<?=$key?>][link]" value="<?=$contents[$key]->link?>" placeholder="Link" />
				     		</div>
		 				</div>
		 				
						<div class="row">
							<div class="col-md-12">
							    <div class="form-group">
							        <div align="left">Image:</div>
							        <div class="input-group">
							            <span class="input-group-btn">
							                <span class="btn btn-primary btn-file">
							                    Browse <i class="glyphicon glyphicon-folder-open"></i><input type="file" class="imgInp" name="content[<?=$key?>][media_path]" accept=".png, .jpg, .jpeg">
							                </span>
							            </span>
							            <input type="text" value="<?=$contents[$key]->media_path?>" class="form-control" name="content[<?=$key?>][media_path]" readonly>
							        </div>
							        <?php if(!empty($contents[$key]->media_path)): ?>
							       	 	<img class='img-upload' src="<?=SUBFOLDER.'/'.MEDIA_PATH.'/'.$contents[$key]->media_path?>" style="width:250px;"/>
							        <?php else: ?>
							       	 	<img class='img-upload' style="width:250px;"/>
							        <?php endif; ?>
							    </div>
							</div>
						</div>
				     	<div class="row">
				     		<div class="col-md-12">
				     			<div align="left">Content:</div>
				     			<textarea cols="70" rows="5" name="content[<?=$key?>][content]" class="ckeditor form-control" ><?=$contents[$key]->content?></textarea>
				     		</div>
				     	</div>
				     	<input type="hidden" class="form-control" name="content[<?=$key?>][lang_id]" value="<?=$language->id?>" />	
		 			</div>
		 		<?php endforeach ?>
				</div>
				</br>
			    <button class="btn btn-lg btn-primary btn-block" type="submit">Update Block</button>  
			 </div>
		</div> 
	</form> 
<?php else:?>
 	<form class="create" method="post" action="insert" enctype="multipart/form-data">
 	 	<div class="row">
	 		<div class="col-md-6">
	 	  	<input type="text" class="form-control" name="title" placeholder="Block Title" required="" />
	 	  </div>
	 		<div class="col-md-6">
 	 		 	<input type="text" class="form-control" name="classes" placeholder="Block Classes"/>
			</div>
		</div> 
 	 	<div class="row">
	 		<div class="col-md-6">	  
				<label for="sel1">Assign Region:</label>
				  <select class="form-control" name="region_title" id="sel1">
				  	<option>None</option>
				  	<?php foreach($regions as $key => $region): ?>
				    	<option value="<?=$region->title?>"><?=$region->title?></option>
				    <?php endforeach ?>
				  </select>
				</div>
	 		<div class="col-md-6">	  
				<label for="sel2">Assign Order(The smaller the higher):</label>
				  <select class="form-control" name="order" id="sel2">
				  	<?php $numbs = array(0,1,2,3,4,5,6,7,8,9,10);
		  	foreach($numbs as $num): ?>
				    	<option value="<?=$num?>"><?=$num?></option>
				    <?php endforeach ?>
				  </select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
		 		<ul class="nav nav-tabs" role="tablist">
		 		<?php foreach($languages as $key => $language): ?>
			 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
						<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
					</li>
		 		<?php endforeach ?>
		 		</ul>
				<div class="tab-content">
		 		<?php foreach($languages as $key => $language): ?>
		 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
		 				<div class="row">
		 					<div class="col-md-12">
			 				<div class="checkbox">
							    <label>
							      <input name="content[<?=$key?>][hide_title]" type="checkbox"> Hide title
							    </label>
			 				</div>	
			 				</div>
			 				<div class="col-md-6">
				     			<input type="text" class="form-control" name="content[<?=$key?>][title]" placeholder="Title" />
				     		</div>
				     		<div class="col-md-6">
				     			<input type="text" class="form-control" name="content[<?=$key?>][link]" placeholder="Link" />
				     		</div>
		 				</div>
						<div class="row">
							<div class="col-md-12">
							    <div class="form-group">
							        <div align="left">Image:</div>
							        <div class="input-group">
							            <span class="input-group-btn">
							                <span class="btn btn-primary btn-file">
							                    Browse <i class="glyphicon glyphicon-folder-open"></i><input type="file" class="imgInp" name="content[<?=$key?>][media_path]" accept=".png, .jpg, .jpeg">
							                </span>
							            </span>
							            <input type="text" class="form-control" name="content[<?=$key?>][media_path]" readonly>
							        </div>
							        <img class='img-upload' style="width:250px;"/>
							    </div>
						    </div>
						</div>
				     	<div class="row">
				     		<div class="col-md-12">
				     			<div align="left">Content:</div>
				     			<textarea cols="70" rows="5" name="content[<?=$key?>][content]" class="ckeditor form-control" ></textarea>
				     		</div>
				     	</div>
				     	<input type="hidden" class="form-control" name="content[<?=$key?>][lang_id]" value="<?=$language->id?>" />	
		 			</div>
		 		<?php endforeach ?>
				</div>
				</br>
			  <button class="btn btn-lg btn-primary btn-block" type="submit">Add New Block</button>
		  </div>
	  </div>   
	</form>
<?php endif; ?>
<?php $this->stop() ?>