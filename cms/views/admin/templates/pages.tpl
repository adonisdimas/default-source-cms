<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Uri</th>
		        <th>Template Assigned</th>
		        <th>Operations</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><?=$entity->uri?></td>
			        <td><?=$entity->template_title?></td>
			        <td><a href="<?=ROOT.ADMIN_PATH?>/pages_translation/edit/<?=$entity->id?>">Add/Edit Content</a>  | <?php if($userrole == 'admin'):?> <a href="pages/edit/<?=$entity->id?>">Edit</a> | <?php endif;?><a target="_new" href="<?=ROOT?><?=$entity->uri?>"> View </a> <?php if($userrole == 'admin'):?>| <a onclick = "if (! confirm('Continue?')) { return false; }" href="pages/delete/<?=$entity->id?>">Delete</a><?php endif;?></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No Pages Created yet. <?php if($userrole == 'admin'):?>Try adding a new <a href="pages/add">one</a><?php endif;?></h3>
		  <?php endif; ?>
	</div>	
	<?php elseif (isset($entity)):?>
		<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/pages/update/<?=$entity->id?>">
 			<div class="row">
		 		<div class="col-md-6">
		 		    <label>Page Title:</label>
		 			<input type="text" class="form-control" name="title" placeholder="Page Title" value="<?=$entity->title?>" required="" />
		 		</div>
				<div class="col-md-6">
			  	 	<label>Uri:</label>
			 		<input type="text" class="form-control" name="uri" placeholder="Page uri" value="<?=$entity->uri?>" required="" />
			 	</div>
			</div>	
			<div class="row">	 		
				<div class="col-md-6">
					<label for="sel2">Select Regions:</label>
				  	<select class="form-control selectpicker" name="region_titles[]" id="sel2" multiple>
						 <?php foreach($regions as $region): ?>
							<option <?php if(strpos($entity->region_titles,$region->title)!== false){ echo 'selected';}?>><?=$region->title?></option>
					 	<?php endforeach ?>
				 	</select>		 
				</div>	
				<div class="col-md-6">
					<label for="sel1">Assign Template:</label>
				  	<select class="form-control" name="template_title" id="sel1">
				  	 <option>None</option>
					 <?php foreach($templates as $key => $template): ?>
						<option value="<?=$template->title?>" <?php if($template->title ==$entity->template_title){ echo 'selected';}?>><?=$template->title?></option>
					 <?php endforeach ?>
				 	</select>
				</div>
			</div>
			<div class="row">	
		    	<div class="col-md-12"><button class="btn btn-lg btn-primary btn-block" type="submit">Update Page</button></div>   
		    </div>
	 	</form> 	
	<?php elseif (isset($page_translations)):?>
  		 <form class="create" method="post" action="<?=ROOT.ADMIN_PATH?>/pages_translation/update/<?=$page_id?>">
	 	 	<div class="row">
			  <div class="col-md-12">
		 		<ul class="nav nav-tabs" role="tablist">
		 		<?php foreach($languages as $key => $language): ?>
			 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
						<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
					</li>
		 		<?php endforeach ?>
		 		</ul>
				<div class="tab-content">
		 		<?php foreach($languages as $key => $language): ?>
		 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
					    <div class="elements">
				      		<div class="row">
					         	<div class="col-md-12">
						            <input type="text" class="form-control" value="<?=$page_translations[$key][0]->meta_title?>" name="page_translation[<?=$language->id?>][meta_title]" placeholder="Meta Title" value="">
						        </div>
						        <br />
						        <div class="col-md-12">
						        	<textarea name="page_translation[<?=$language->id?>][meta_description]" cols="50" rows="3" class="form-control" placeholder="Meta Description"><?=$page_translations[$key][0]->meta_description?></textarea>
						        </div>
						        <br />
						        <div class="col-md-12">
						        	<textarea name="page_translation[<?=$language->id?>][meta_keywords]" cols="50" rows="3" class="form-control" placeholder="Meta Keywords"><?=$page_translations[$key][0]->meta_keywords?></textarea>
						        </div>
						        <br />
					         	<div class="col-md-12">
						            <input type="text" class="form-control" name="page_translation[<?=$language->id?>][title]" value="<?=$page_translations[$key][0]->title?>" placeholder="Title">
						        </div>
						        <br />
						     	<div class="auto-elements">
						     		<?php if(count($page_contents[$key])>0): ?>
							     		<?php foreach($page_contents[$key] as $index => $page_content): ?>
								      	<div class="elements">
									      	<div class="row after-add-more added-element">
									         	<div class="col-md-12">
						        					<textarea name="page_content[<?=$language->id?>][][content]" cols="50" rows="3" class="ckeditor form-control" placeholder="Content" required><?=$page_content->content?></textarea>
										        </div>
									        </div>
								        </div>
								       <?php endforeach ?>
								    <?php else: ?>								       
								      	<div class="elements">
									      	<div class="row after-add-more added-element">
									         	<div class="col-md-12">
						        					<textarea name="page_content[<?=$language->id?>][][content]" cols="50" rows="3" class="ckeditor form-control" placeholder="Content" required> </textarea>
										        </div>
									        </div>
								        </div>
								    <?php endif; ?>
							        <!-- Copy Fields -->
							        <div class="copy hide">
							          <div class="row added-element" style="margin:5px 0">
							         	<div class="col-md-12">
				        					<textarea id="editor_<?=$language->id?>" name="page_content[<?=$language->id?>][][content]" cols="50" rows="3" class="form-control" placeholder="Content"></textarea>
								        </div>           
							          </div>
							        </div>
						     	</div>
						     	<div class="input-group-btn"> 
									<button class="btn btn-success add-btn-content" type="button"><i class="glyphicon glyphicon-plus"></i> Add Page Content</button>
									<button class="btn btn-danger remove-btn" type="button"><i class="glyphicon glyphicon-minus"></i> Remove Page Content</button>
								</div>						        
						     </div>							        
				     	</div>
		 			</div>
		 		<?php endforeach ?>
				</div>
				</br>
			    <button class="btn btn-lg btn-primary btn-block" type="submit">Add Page</button>  			  
			  </div>
			</div> 
		</form>
	<?php else:?>
 	<form class="create" method="post" action="<?=ROOT.ADMIN_PATH?>/pages/insert">
 	 	<div class="row">
		  	<div class="col-md-6">
		  	    <label>Page Title:</label>
		 		<input type="text" class="form-control" name="title" placeholder="Page Title" required="" />
		 	</div>
			<div class="col-md-6">
		  	 	<label>Add Uri (must be unique for each page):</label>
		 		<input type="text" class="form-control" name="uri" placeholder="Page uri" required="" />
		 	</div>
		</div>
	 	<div class="row">
			<div class="col-md-6">
				<label for="sel2">Select Regions:</label>
			  	<select class="form-control selectpicker" name="region_titles[]" id="sel2" multiple>
					 <?php foreach($regions as $region): ?>
						<option><?=$region->title?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>	
			<div class="col-md-6">
				<label for="sel1">Assign Template:</label>
			  	<select class="form-control" name="template_title" id="sel1">
			  	 <option>None</option>
				 <?php foreach($templates as $key => $template): ?>
				  	<option value="<?=$template->title?>"><?=$template->title?></option>
				 <?php endforeach ?>
			 	</select>
			</div>
		 </div>
		 <div class="row">
		  <div class="col-md-12">
	 		<ul class="nav nav-tabs" role="tablist">
	 		<?php foreach($languages as $key => $language): ?>
		 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
					<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
				</li>
	 		<?php endforeach ?>
	 		</ul>
			<div class="tab-content">
	 		<?php foreach($languages as $key => $language): ?>
	 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
				    <div class="elements">
			      		<div class="row">
				         	<div class="col-md-12">
					            <input type="text" class="form-control" name="page_translation[<?=$language->id?>][meta_title]" placeholder="Meta Title">
					        </div>
					        <br />
					        <div class="col-md-12">
					        	<textarea name="page_translation[<?=$language->id?>][meta_description]" cols="50" rows="3" class="form-control" placeholder="Meta Description"></textarea>
					        </div>
					        <br />
					        <div class="col-md-12">
					        	<textarea name="page_translation[<?=$language->id?>][meta_keywords]" cols="50" rows="3" class="form-control" placeholder="Meta Keywords"></textarea>
					        </div>
					        <br />
				         	<div class="col-md-12">
					            <input type="text" class="form-control" name="page_translation[<?=$language->id?>][title]" placeholder="Title">
					        </div>
					        <br />
					     	<div class="auto-elements">
						      	<div class="elements">
							      	<div class="row after-add-more added-element">
							         	<div class="col-md-12">
				        					<textarea name="page_content[<?=$language->id?>][][content]" cols="50" rows="3" class="ckeditor form-control" placeholder="Content" required></textarea>
								        </div>
							        </div>
						        </div>
						        <!-- Copy Fields -->
						        <div class="copy hide">
						          <div class="row added-element" style="margin:5px 0">
						         	<div class="col-md-12">
			        					<textarea id="editor_<?=$language->id?>" name="page_content[<?=$language->id?>][][content]" cols="50" rows="3" class="form-control" placeholder="Content"></textarea>
							        </div>           
						          </div>
						        </div>
					     	</div>
					     	<div class="input-group-btn"> 
								<button class="btn btn-success add-btn-content" type="button"><i class="glyphicon glyphicon-plus"></i> Add Page Content</button>
								<button class="btn btn-danger remove-btn" type="button"><i class="glyphicon glyphicon-minus"></i> Remove Page Content</button>
							</div>						        
					     </div>							        
			     	</div>
	 			</div>
	 		<?php endforeach ?>
			</div>
			</br>
		    <button class="btn btn-lg btn-primary btn-block" type="submit">Add Page</button>  			  
		  </div>
		 </div>
	 </form>
<?php endif; ?>
<?php $this->stop() ?>