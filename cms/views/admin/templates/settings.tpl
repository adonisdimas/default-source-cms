<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'userrole' => $userrole, 'active' =>$active]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Value</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><?=$entity->value?></td>
			        <td><a href="settings/edit/<?=$entity->id?>">Edit</a> <?php if($entity->title!="Active Theme" && $entity->title!="Default Language" && $entity->title!="Google MAP API KEY" && $entity->title!="Google Analytics ID" && $entity->title!="Book Online Link" && $entity->title!="Contact Form Send To" && $entity->title!="Contact Form Email Subject"): ?>| <a onclick = "if (! confirm('Continue?')) { return false; }" href="settings/delete/<?=$entity->id?>">Delete</a><?php endif; ?></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No settings Created yet. Try adding a new <a href="settings/add">one</a></h3>
		  <?php endif; ?>
	</div>	
<?php elseif (isset($entity)):?>
 	<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/settings/update/<?=$entity->id?>">
	     <div class="row">
	      	<div class="col-md-6">
	     		<input type="text" class="form-control" value="<?=$entity->title?>" name="title" placeholder="Title" disabled/>
	     	</div>
		     <div class="col-md-6">
		     	<?php if($entity->title=="Active Theme"): ?>
			     	<select class="form-control" name="value" id="sel1">
					  	<?php
					  	$themes =$GLOBALS['available_themes'];
					  	foreach($themes as $theme): ?>
			    			<option value="<?=$theme?>" <?php if($theme==$entity->value){ echo 'selected';}?>><?=$theme?></option>
			    		<?php endforeach ?>
			  		</select>	
			  	<?php elseif($entity->title=="Default Language"): ?>	     
			     	<select class="form-control" name="value" id="sel1">
					  	<?php foreach($languages as $language): ?>
			    			<option value="<?=$language->locale?>" <?php if($language->locale==$entity->value){ echo 'selected';}?>><?=$language->title?></option>
			    		<?php endforeach ?>
			  		</select>				  			
		     	<?php else: ?>
			  		<input type="text" class="form-control" value="<?=$entity->value?>" name="value" placeholder="Value" required="" />
			  	<?php endif; ?>
		     </div>    
		 </div>	  
		 <div class="row"> 
		 	<div class="col-md-12">
	    	 <button class="btn btn-lg btn-primary btn-block" type="submit">Update Setting</button> 
	     	</div>  
	     </div>
	</form> 
	<?php if($entity->title == "Active Theme"): ?>
		<div class="container-fluid">
			<div class="row">
				<strong> <?=$entity->value?> Theme Preview:</strong>
				<br><p>
<div style="width: 100%;height:600px;overflow: hidden;margin: 0 auto; background: url(<?=ROOT.'/'.VIEWS_THEME_PATH.'/'.$entity->value?>/preview.png) repeat top;background-size: 100%;"></div>
			</div></p>
		</div>
	<?php endif; ?>
<?php else:?>
 	<form class="create" method="post" action="insert">
	     <div class="row">
	      	<div class="col-md-6">
	     		<input type="text" class="form-control" name="title" placeholder="Title" required="" />
	     	</div>
		     <div class="col-md-6">
		     	<input type="text" class="form-control" name="value" placeholder="Value" required="" />
		     </div>     
		 </div>
		 <div class="row">
		 	<div class="col-md-12">
	     		<button class="btn btn-lg btn-primary btn-block" type="submit">Add Setting</button>  
	     	</div> 
	     </div>
	</form>
<?php endif; ?>
<?php $this->stop() ?>