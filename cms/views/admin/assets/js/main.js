/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.manageStickyHeader();
			app.manageSelectElements();
			app.manageAppendingElements();
			app.manageSortabeElements();
			app.manageInputImagePreview();
			app.manageMedia();
			app.managePagination();
			$(window).on('resize', app.onResize);
		},
		'manageInputImagePreview' : function() {
	    	$(document).on('change', '.btn-file :file', function() {
	    		var input = $(this),
	    			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	    		input.trigger('fileselect', [label]);
	    		});

	    		$('.btn-file :file').on('fileselect', function(event, label) {
	    		    
	    		    var input = $(this).parents('.input-group').find(':text'),
	    		        log = label;
	    		    
	    		    if( input.length ) {
	    		        input.val(log);
	    		    } else {
	    		        if( log ) alert(log);
	    		    }
	    	    
	    		});
	    		function readURL(input) {
	    		    if (input[0].files && input[0].files[0]) {
	    		        var reader = new FileReader();
	    		        reader.onload = function (e) {
	    		          input.parents('.form-group').find('.img-upload').attr('src', e.target.result);
	    		        }
	    		        reader.readAsDataURL(input[0].files[0]);
	    		    }
	    		}
	    		$(".imgInp").change(function(){
	    			readURL($(this));
	    		}); 			
		},
		'manageSelectElements' : function() {
			$('.selectpicker').selectpicker();
		},
		'managePagination' : function() {
			$(".pagination li").click(function(){ 
				$(this).siblings('li').removeClass('active');
				$(this).addClass('active');
				$('table.paginated tbody').hide();
				$($(this).children('a').attr('href')).toggle();				
			});
		},
		'manageMedia' : function() {
			$('input.selectOnClick').click(function(){ $(this).select(); });
			$("#input-ficons-1").fileinput({
			    uploadAsync: false,
			    multiple: true,
			    theme: "fa",
			    previewFileIcon: '<i class="fa fa-file"></i>',
			    previewFileIconSettings: {
			        'docx': '<i class="fa fa-file-word-o text-primary"></i>',
			        'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
			        'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
			        'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
			        'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
			        'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
			    }
			});
			$("#input-gallery1").fileinput({
			    uploadAsync: false,
			    multiple: true,
			    theme: "fa"
			});
			$("#input-gallery2").fileinput({
			    uploadAsync: false,
			    multiple: true,
			    theme: "fa"
			});
		},
		'manageSortabeElements' : function() {
		    $(".sortable").sortable({
		        tolerance: 'pointer',
		        revert: 'invalid',
		        placeholder: 'span2 well placeholder tile',
		        forceHelperSize: true
		    });
		},
		'manageAppendingElements' : function() {
		      $(".add-btn-content").click(function(){ 
		    	  $elem=$(this).parent().parent().find(".auto-elements").children('.copy');
		    	  $count = $(this).parent().parent().find(".auto-elements .elements .row").length;
		    	  $id = $elem.find('textarea').attr("id");
		    	  $elem.find('textarea').attr("id",$id+$count);
		    	  $elem.find('textarea').attr("class","ckeditor form-control");		
		    	  var html = $elem.html();
		    	  $(this).parent().parent().find(".auto-elements .elements .row:last").after(html);
		    	  CKEDITOR.replace($id+$count);
		    	  CKEDITOR.add;
		          $(this).parent().find(".remove-btn").show();
		      });
	    	  if($(".add-btn").parent().parent().find(".auto-elements .elements .row.added-element").length>2){
	    		  $(".add-btn").parent().find(".remove-btn").show();
	    	  }
		      $(".add-btn").click(function(){ 
		    	  var html = $(this).parent().parent().find(".auto-elements").children('.copy').html();
		    	  $(this).parent().parent().find(".auto-elements .elements .row:last").after(html);
		          $(this).parent().find(".remove-btn").show();
		      });
		      $(".remove-btn").click(function(){ 
		    	  if($(this).parent().parent().find(".auto-elements .elements .row.added-element").length>1){
			          $(this).parent().parent().find(".auto-elements .elements .row.added-element:last").remove();
			          if($(this).parent().parent().find(".auto-elements .elements .row").length==1){
			        	  $(this).parent().find(".remove-btn").hide();
			          }
		    	  }else{
		        	  $(this).parent().find(".remove-btn").hide();
		    	  }
		      });
		      $(".add-slider").click(function(){ 
				  app.manageInputImagePreview();
		      });
		},
		'manageStickyHeader' : function() {
			var header = $('header');
		    $(window).scroll(function () {
	    		if ($(this).scrollTop() > 100) {
		            header.addClass("sticky");
	    		}else{
	    			header.removeClass("sticky");
	    		}
		    });
		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
				if ( !$('#dropdown').hasClass('active') ) {
					 if($(window).width()>1260){
						$("header .menu").css( "display", "block");
				    }else{
						$("header .menu").css( "display", "none");
				     }
				}
			}, 10);
		}
	};
	$(document).on('ready', app.init());
})(jQuery);
