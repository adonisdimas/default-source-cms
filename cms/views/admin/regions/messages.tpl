<?php if (isset($_GET['insert'])): ?>
        <?php if ($_GET['insert']==1): ?>
        <div class="alert alert-success">
		  	<strong>New entity created succesfully!</strong> 
		</div>
        <?php elseif($_GET['insert']==0):?>
          <div class="alert alert-warning">
			 <strong>Already exists!</strong></br> Please try again. 
		  </div>		        
        <?php else:?>
          <div class="alert alert-danger">
			  <strong>An error occured.</strong> </br> Please try again. 
		  </div>			        
        <?php endif; ?>
     <?php endif; ?>
 <?php if (isset($_GET['delete'])): ?>
    <?php if ($_GET['delete']==1): ?>
        <div class="alert alert-success">
		   <strong>Deleted succesfully!</strong> 
		</div>	        
        <?php else:?>
        <div class="alert alert-danger">
		   <strong>An error occured.</strong> </br> Please try again. 
		</div>			        
     <?php endif; ?>
 <?php endif; ?>  
 <?php if (isset($_GET['update'])): ?>
    <?php if ($_GET['update']==1): ?>
        <div class="alert alert-success">
		   <strong>Updated succesfully!</strong> 
		</div>	        
        <?php else:?>
        <div class="alert alert-danger">
		   <strong>An error occured.</strong> </br> Please try again. 
		</div>			        
     <?php endif; ?>
<?php endif; ?>	