<div class="nav-side-menu">
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
        <div class="menu-list">
            <ul id="menu-content" class="menu-content collapse out">
                <li class="<?php if($active[0] =='dashboard'){ echo 'active';}?>">
                  <a href="<?=ROOT.ADMIN_PATH?>/dashboard"><i class="fa fa-dashboard fa-lg"></i> Dashboard</a>
                </li>
                <li  data-toggle="collapse" data-target="#pages" class="collapsed <?php if($active[0] =='pages'){ echo 'active';}?>">
                  <a href="#"><i class="fa fa-file fa-lg"></i> Pages<span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse  <?php if($active[0] =='pages'){ echo 'in';}?>" id="pages">
                    <li class="<?php if($active[1] =='pages' or $active[1] =='pages/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/pages">All Pages</a></li>
                    <?php if($userrole == 'admin'):?>
                    	<li class="<?php if($active[1] =='pages/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/pages/add">Add New Page</a></li>
                    <?php endif;?>
                </ul>                
                <li  data-toggle="collapse" data-target="#menus" class="collapsed <?php if($active[0] =='menus'){ echo 'active';}?>">
                  <a href="#"><i class="fa fa-bars fa-lg"></i> Menus<span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse  <?php if($active[0] =='menus'){ echo 'in';}?>" id="menus">
                    <li class="<?php if($active[1] =='menus' or $active[1] =='menus/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/menus">All Menus</a></li>
                    <?php if($userrole == 'admin'):?>
                    	<li class="<?php if($active[1] =='menus/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/menus/add">Add New Menu</a></li>
                    <?php endif;?>
                </ul> 
                <?php if($userrole == 'admin'):?>                 
	                <li  data-toggle="collapse" data-target="#blocks" class="collapsed <?php if($active[0] =='blocks'){ echo 'active';}?>">
	                  <a href="#"><i class="fa fa-database fa-lg"></i> Blocks<span class="arrow"></span></a>
	                </li>
	                <ul class="sub-menu collapse  <?php if($active[0] =='blocks'){ echo 'in';}?>" id="blocks">
	                    <li class="<?php if($active[1] =='blocks' or $active[1] =='blocks/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/blocks">All Blocks</a></li>
	                    <?php if($userrole == 'admin'):?>
	                    	<li class="<?php if($active[1] =='blocks/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/blocks/add">Add New Block</a></li>
	                    <?php endif;?>
	                </ul> 
                <?php endif;?>
                <?php if($userrole == 'admin'):?>
	                <li  data-toggle="collapse" data-target="#media" class="collapsed <?php if($active[0] =='media'){ echo 'active';}?>">
	                  <a href="#"><i class="fa fa-picture-o fa-lg"></i> Media<span class="arrow"></span></a>
	                </li>
	                <ul class="sub-menu collapse  <?php if($active[0] =='media'){ echo 'in';}?>" id="media">
	                    <li class="<?php if($active[1] =='media' or $active[1] =='media'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/media">All Media</a></li>
		                <li  data-toggle="collapse" data-target="#sliders" class="collapsed has-submenu">
		                  <a href="#"><i class="fa fa-sliders fa-sm" aria-hidden="true"></i> Sliders<span class="arrow"></span></a>
			                <ul class="sub-menu collapse  <?php if($active[0] =='media' && ($active[1] =='sliders'  or $active[1] =='sliders/add' or $active[1] =='sliders/edit')){ echo 'in';}?>" id="sliders">
			                    <li class="<?php if($active[1] =='sliders' or $active[1] =='sliders/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/sliders">All Sliders</a></li>
			                    <li class="<?php if($active[1] =='sliders/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/sliders/add">Add New Slider</a></li>
			                </ul>                 
		                </li>
		                <li  data-toggle="collapse" data-target="#galleries" class="collapsed has-submenu">
		                  <a href="#"><i class="fa fa-file-image-o fa-sm" aria-hidden="true"></i> Galleries<span class="arrow"></span></a>
			                <ul class="sub-menu collapse  <?php if($active[0] =='media' && ($active[1] =='galleries'  or $active[1] =='galleries/add' or $active[1] =='galleries/edit')){ echo 'in';}?>" id="galleries">
			                    <li class="<?php if($active[1] =='galleries' or $active[1] =='galleries/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/galleries">All Galleries</a></li>
			                    <li class="<?php if($active[1] =='galleries/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/galleries/add">Add New Gallery</a></li>
			                </ul>                 
		                </li>
	                </ul>
                <?php endif;?> 
                <?php if($userrole == 'admin'):?>        
		                <li data-toggle="collapse" data-target="#settings" class="collapsed <?php if($active[0] =='settings'){ echo 'active';}?>">
		                  <a href="#"><i class="fa fa-cogs fa-lg"></i> Settings <span class="arrow"></span></a>
		                </li>  
		                <ul class="sub-menu collapse <?php if($active[0] =='settings'){ echo 'in';}?>" id="settings">
		  					<li  data-toggle="collapse" data-target="#sett" class="has-submenu collapsed">
			                  <a href="#"><i class="fa fa-cog fa-sm"></i> Custom Settings<span class="arrow"></span></a>
			                	  <ul class="sub-menu collapse <?php if($active[0] =='settings' && ($active[1] =='settings' or $active[1] =='settings/add' or $active[1] =='settings/edit')){ echo 'in';}?>" id="sett">
			                    	<li class="<?php if($active[1] =='settings' or $active[1] =='settings/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/settings">All Custom Settings</a></li>
			                    	<li class="<?php if($active[1] =='settings/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/settings/add">Add New Custom Setting</a></li>
			                	  </ul> 
			                </li>
		  					<li  data-toggle="collapse" data-target="#languages" class="has-submenu collapsed">
			                  <a href="#"><i class="fa fa-flag fa-sm"></i> Languages<span class="arrow"></span></a>
			                	  <ul class="sub-menu collapse <?php if($active[0] =='settings' && ($active[1] =='languages' or $active[1] =='languages/add' or $active[1] =='languages/edit')){ echo 'in';}?>" id="languages">
			                    	<li class="<?php if($active[1] =='languages' or $active[1] =='languages/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/languages">All Languages</a></li>
			                    	<li class="<?php if($active[1] =='languages/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/languages/add">Add New Language</a></li>
			                		</ul> 
			                </li>
		  					<li  data-toggle="collapse" data-target="#translations" class="has-submenu collapsed">
			                  <a href="#"><i class="fa fa-language" aria-hidden="true"></i>Translations<span class="arrow"></span></a>
			                	  <ul class="sub-menu collapse <?php if($active[0] =='settings' && ($active[1] =='translations' or $active[1] =='translations/add' or $active[1] =='translations/edit')){ echo 'in';}?>" id="translations">
			                    	<li class="<?php if($active[1] =='translations' or $active[1] =='translations/edit'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/translations">All Translations</a></li>
			                    	<li class="<?php if($active[1] =='translations/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/translations/add">Add New Translations</a></li>
			                		</ul> 
			                </li>
		                  <li data-toggle="collapse" data-target="#users" class="has-submenu collapsed">
		                 		<a href="#"><i class="fa fa-users fa-sm"></i> Users<span class="arrow"></span></a>
			                	<ul class="sub-menu collapse <?php if($active[0] =='settings' && ($active[1] =='users'  or $active[1] =='users/add' or $active[1] =='users/edit')){ echo 'in';}?>" id="users">
			                    <li class="<?php if($active[1] =='users'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/users">All Users</a></li>
			                    <li class="<?php if($active[1] =='users/add'){ echo 'active';}?>"><a href="<?=ROOT.ADMIN_PATH?>/users/add">Add New User</a></li>
			                	</ul>                	
		                	</li>
		                </ul>
				<?php endif;?>                                            
            </ul>
     </div>
     <div class="row credits"> 
		<p>Created by <a href="http://www.dproject.gr" target="new"> Dproject</a></p>
	 </div>
</div>