<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<head>
  <title><?=$this->e($title)?></title>
	<meta charset="utf-8" />
	<meta content="IE=edge" http-equiv="X-UA-Compatible" />
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="<?=ADMIN_ASSETS?>/css/lib/bootstrap.min.css" rel="stylesheet">
	<link href="<?=ADMIN_ASSETS?>/css/lib/font-awesome.min.css" rel="stylesheet">
	<link href="<?=ADMIN_ASSETS?>/css/lib/fileinput.min.css" rel="stylesheet">
	<link href="<?=ADMIN_ASSETS?>/css/lib/fileinput.min.css" rel="stylesheet">	
	<link href="<?=ADMIN_ASSETS?>/css/lib/bootstrap-select.min.css" rel="stylesheet">	
	<link href="<?=ADMIN_ASSETS?>/css/style.css" rel="stylesheet">
	<?=$this->section('head')?>
</head>
<body>
	<?=$this->section('header')?>
	<?=$this->section('main-content')?>
	<?=$this->section('footer')?>
  	<script src="<?=ADMIN_ASSETS?>/js/lib/jquery.min.js"></script>
  	<script src="<?=ADMIN_ASSETS?>/js/lib/jquery-ui.min.js"></script>
  	<script src="<?=ADMIN_ASSETS?>/js/lib/bootstrap.min.js"></script>
  	<script src="<?=ADMIN_ASSETS?>/js/lib/canvas-to-blob.min.js"></script>
	<script src="<?=ADMIN_ASSETS?>/js/lib/sortable.min.js"></script>
	<script src="<?=ADMIN_ASSETS?>/js/lib/fileinput.min.js"></script>
	<script src="<?=ADMIN_ASSETS?>/js/lib/bootstrap-select.min.js"></script>
  	<script src="<?=ADMIN_ASSETS?>/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">var media_path = "<?=SUBFOLDER?><?=ADMIN_PATH?>/media_box#view";</script>
   	<script src="<?=ADMIN_ASSETS?>/ckeditor/config.js"></script>
  	<script src="<?=ADMIN_ASSETS?>/js/main.js"></script>
	<?=$this->section('scripts')?>
</body>
</html>