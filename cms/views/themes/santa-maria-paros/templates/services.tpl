<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down animated bounce infinite"><i class="fa fa-angle-down fa-5x" aria-hidden="true"></i></a>
		</div>
	<?php endif; ?>
	<div class="row scroll-section">
		<?php if (isset($regions['book-online'])): ?>
			<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
		<?php endif; ?>
		<div class="row main-content top-bottom-padding-large">
			<div class="content-width row content-padding">
				<div class="row lines"></div>
				<h1><?=$page_translation->title;?></h1>
				<div class="row">
					<div class="row two-column-text">
						<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>
					</div>
				</div>			
			</div>
		</div>
		<?php foreach($page_contents as $key => $page_content): ?>
			<?php if($key!=0):?>
				<div class="row main-content top-bottom-padding-large <?php if($key % 2 == 1){echo 'background-color1';}?>">
					<div class="content-width row content-padding">
						<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>
					</div>
				</div>
			<?php endif; ?>
		<?php endforeach ?>
	</div>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
	<?php endif; ?>
	<div class="row top-bottom-padding-large"></div>
</main>
<?php $this->stop() ?>