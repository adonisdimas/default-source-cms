<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
					<?php if(isset($sliders['pages'])): ?>
						<?php foreach($sliders['pages'] as $key => $slider): ?>
					 		<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>
						<?php endforeach ?>
					<?php endif; ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down animated bounce infinite"><i class="fa fa-angle-down fa-5x" aria-hidden="true"></i></a>
		</div>
	<?php endif; ?>
	<!--  Main-content Section -->
	<div class="row scroll-section home-section content-padding">
		<?php if (isset($regions['book-online'])): ?>
			<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
		<?php endif; ?>
		<div class="row main-content">
			<div class="content-width-inner row center-align">
				<div class="row lines"></div>
				<h1><?=$page_translation->title;?></h1>
				<div class="row">
					<h3>Relaxation beyond any expectation</h3>
				</div>
					<div class="row">
						<?php foreach($page_contents as $key => $page_content): ?>
							<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>
						<?php endforeach ?>
					</div>
				</div>
			</div>
	</div>
	<div class="row top-bottom-padding"></div>	
	<!--  GALLERY-->
	<section class="row top-bottom-padding background-color1">
		<div class="content-width-inner center-align">
			<h2><?=$translations['Photo Gallery Title'];?></h2>
		</div>
		<?php if (isset($galleries['pages'])): ?>
			<?php foreach($galleries['pages'] as $key => $gallery): ?>
	  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>
	 		<?php endforeach ?>
		<?php endif; ?>
	</section>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['why-book'])): ?>
		<?php $this->insert('regions/why-book', ['region' =>$regions['why-book']]); ?>
	<?php endif; ?>
</main>
<?php $this->stop() ?>