<ul class="<?=$classes?>">
	<?php foreach($menu as $key => $menu_item): ?>
  		<li <?php if(count($menu)==$key+1){echo 'class="last"';}?>>
  			<a href="<?=ROOT?><?=$menu_item->link?>" <?php if($_SERVER['REQUEST_URI'] == SUBFOLDER.$menu_item->link){ echo 'class="active"';} ?>><?=$menu_item->title?></a>
  		</li>
 	<?php endforeach ?>
</ul>