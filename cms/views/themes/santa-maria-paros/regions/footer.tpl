<footer>
	<div class="row">
		<div class="content-width content-padding">
			<?php if (isset($region['blocks'])): ?>
				<?php foreach($region['blocks'] as $key => $block): ?>
					<div class="<?=$block['contents'][0];?>">
						<?php if($key==0):?>
							<div class="teaser one">
								<img src="<?php echo ROOT.'/'.MEDIA_PATH.'/'.$block['contents'][1][0]->media_path;?>" alt="">
								<div class="button">
									<a href="<?=$block['contents'][1][0]->link;?>"><?=$block['contents'][1][0]->title;?> <i class="fa fa-long-arrow-right fa-3x" aria-hidden="true"></i></a>
								</div>
								<span class="shadow-btn"></span>
							</div>
						<?php elseif($key==1):?>
							<div class="teaser two">
								<img src="<?php echo ROOT.'/'.MEDIA_PATH.'/'.$block['contents'][1][0]->media_path;?>" alt="">
								<div class="button">
									<a href="<?=$block['contents'][1][0]->link;?>"><?=$block['contents'][1][0]->title;?> <i class="fa fa-long-arrow-right fa-3x" aria-hidden="true"></i></a>
								</div>
								<span class="shadow-btn"></span>
							</div>						
						<?php else:?>
							<div class="row text">
								<h4><?=$block['contents'][1][0]->title;?></h4>
								<?=$block['contents'][1][0]->content;?>
							</div>
							<div class="row copyright">
								<p>&copy; <?php echo date("Y") ?> - <?php echo $region['translations']['Created by']; ?> <a target="new" href="www.dproject.gr">Dproject</a></p>
							</div>						
						<?php endif;?>
					</div>
				<?php endforeach ?>
			<?php endif; ?>	
		</div>
	</div>
</footer>