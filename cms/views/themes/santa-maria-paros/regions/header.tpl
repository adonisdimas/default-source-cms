<header>
	<div class="content-width-outer">
		<div class="row">
			<div class="row pattern background-color1">
				<div class="row">
					<div class="language-selector">
						<ul>
				<li><a class='<?php if($this->e($locale) == "en"){echo "active";} ?>' href="/en<?php echo remove_locale($this->e($locale),URI)?>">EN</a></li>
				/<li><a class='<?php if($this->e($locale) == "el"){echo "active";} ?>'href="/el<?php echo remove_locale($this->e($locale),URI)?>">EL</a></li>
				/<li><a class='<?php if($this->e($locale) == "fr"){echo "active";} ?>'href="/fr<?php echo remove_locale($this->e($locale),URI)?>">FR</a></li>
						</ul>
					</div>
					<div class="row logo">
						<a href="<?=ROOT?>/<?=$GLOBALS['default_locale']?>/"></a>
					</div>
				</div>
			</div>
			<div class="row navigation content-padding">
				<div class="nav-bar1 content-width">
					<div id="dropdown"><span class="glyphicon glyphicon-menu-hamburger fa-2x" aria-hidden="true"></span></div>
					<nav class="menu">
						<?php if (isset($region['menus'])): ?>
							<?php foreach($region['menus'] as $menu): ?>
								<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>						
							<?php endforeach ?>
						<?php endif; ?>
						<ul class="language-selector-mobile">
							<li><a class='<?php if($this->e($locale) == "en"){echo "active";} ?>' href="/en<?php echo remove_locale($this->e($locale),URI)?>">EN</a></li>
							<li><a class='<?php if($this->e($locale) == "el"){echo "active";} ?>'href="/el<?php echo remove_locale($this->e($locale),URI)?>">EL</a></li>
							<li><a class='<?php if($this->e($locale) == "fr"){echo "active";} ?>'href="/fr<?php echo remove_locale($this->e($locale),URI)?>">FR</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<?php if (isset($region['blocks'])): ?>
		<div class="row">
			<?php foreach($region['blocks'] as $block): ?>
				<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
			<?php endforeach ?>
		</div>
	<?php endif; ?>
</header>