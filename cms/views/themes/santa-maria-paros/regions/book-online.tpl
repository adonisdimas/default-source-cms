<div class="book-online">
	<form class="book-online-form form-inline" id="booking_form" name="booking_form" method="post" action="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>" target="new">
		<div class="form-group date">
	      <input type="text" placeholder="<?php echo $region['translations']['Book Online form label 1'];?>" class="calender" id="datepicker1">
	    </div>
		<div class="form-group date">
	      <input type="text" placeholder="<?php echo $region['translations']['Book Online form label 2'];?>" class="calender" id="datepicker2">
	    </div>
	   	<div class="form-group select">
			<select name="rooms" class="form-select" id="form-rooms">
				<option selected disabled><?php echo $region['translations']['Book Online form label 3'];?></option>
				<option value="1" class="rooms-option">1</option>
				<option value="2" class="rooms-option">2</option>
				<option value="3" class="rooms-option">3</option>
				<option value="4" class="rooms-option">4</option>
			</select>
		</div>
		<div class="form-group select">
			<select name="rooms" class="form-select" id="form-rooms">
				<option selected disabled><?php echo $region['translations']['Book Online form label 4'];?></option>
				<option value="1" class="rooms-option">1</option>
				<option value="2" class="rooms-option">2</option>
				<option value="3" class="rooms-option">3</option>
				<option value="4" class="rooms-option">4</option>
			</select>
		</div>
		<div class="form-group">
		<input type="submit" value="<?php echo $region['translations']['Book Online button label'];?>" class="" onclick="if (ValidateDates11a('<?php echo $language ?>')){document.getElementById('booking_form').submit();}">
		</div>
	</form>
	<div class="online-button"><a href="" target="new" title="overlay" style=""></a></div>
	<a id="book-online-resp" target="new" href=""><?php echo $region['translations']['Book Online button label'];?></a>
</div>