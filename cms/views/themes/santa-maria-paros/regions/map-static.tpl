<section class="row top-bottom-padding-large">
	<div class="content-width center-align content-padding">
		<h2><?php echo $region['translations']['Map Title']; ?></h2>
	</div>
	<div class="col-md-12">
		<div class="map-wrapper">
			<div class="map">
				<div id="map-canvas"></div>
			</div>
		</div>
	</div>
</section>
