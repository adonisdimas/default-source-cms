<div class="content-padding content-width">
	<div class="row caption">
		<p><?php echo $region['translations']['Caption']; ?></p>
	</div>		
</div>	
<?php if(isset($sliders)): ?>	
	<?php foreach($sliders as $key => $slider): ?>
 		<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>						
	<?php endforeach ?>
<?php endif; ?>