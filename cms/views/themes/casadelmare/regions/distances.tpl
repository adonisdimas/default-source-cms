<? if (isset($region['blocks'])): ?>
	<div class="row">
		<?php foreach($region['blocks'] as $block): ?>
			<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
		<?php endforeach ?>
	</div>
<? endif; ?>