<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>	
	<div class="row slider-wrapper">
		<div class="content-padding content-width">
			<div class="row caption">
				<?php if (isset($translations['Caption'])){ echo $translations['Caption']; } ?>
			</div>		
		</div>	
		<?php if (isset($sliders['pages'])): ?>	
			<?php foreach($sliders['pages'] as $key => $slider): ?>
	  				<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>						
	 		<?php endforeach ?>
	 		<a href="#" class="scroll-down"><img src="img/icons/arrow-down2.png" /></a>				
		<?php endif; ?>			
	</div>
	<!--  Main-content Section -->
	<div class="row scroll-section background-color6 border-bottom">
		<div class="content-padding content-width content-padding">
			<div class="row top-bottom-padding">
				<div class="main-content col-md-12">
					<h1><?=$page_translation->title;?></br>
					<?php $this->insert('components/page-contents', ['page_content' => $page_contents[0]]); ?>
				</div>				
			</div>
		</div>
		<?php if (isset($regions['distances'])): ?>
			<div class="row background-color3">
				<?php $this->insert('regions/distances', ['region' =>$regions['distances']]); ?>
			</div>	
		<?php endif; ?>			
		<?php if (isset($page_contents[1])): ?>
			<div class="row content-padding content-width top-bottom-padding-large">
				<?php $this->insert('components/page-contents', ['page_content' => $page_contents[1]]); ?>
			</div>
		<?php endif; ?>		
		<?php if (isset($galleries['pages'])): ?>	
			<?php foreach($galleries['pages'] as $key => $gallery): ?>
	  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>						
	 		<?php endforeach ?>
			<div class="col-md-12 center-align">
				<div class="button gallery"><a href=""><?php if (isset($translations['Gallery Button title'])){ echo $translations['Gallery Button title']; }?></a></div>
			</div>		 		
		<?php endif; ?>					
	</div>
	<?php if (isset($regions['content'])): ?>
		<div class="row">
			<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
		</div>	
	<?php endif; ?>

	<?php if (isset($regions['glance'])): ?>
		<div class="row background-color3">
			<?php $this->insert('regions/glance', ['region' =>$regions['glance']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['map'])): ?>
		<div class="row top-bottom-padding background-color6">
			<?php $this->insert('regions/map', ['region' =>$regions['map']]); ?>
		</div>			
	<?php endif; ?>		
	<div class="row top-bottom-padding background-color6"></div>
	<div class="row top-bottom-padding background-color6"></div>
</main>
<?php $this->stop() ?>