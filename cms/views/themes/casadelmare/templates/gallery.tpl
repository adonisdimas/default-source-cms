<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>	
	<div class="row top-bottom-padding background-color6">
		<div class="content-padding content-width content-padding">
			<div class="row">
				<div class="col-md-12 center-align"><h1><?=$page_translation->title;?></h1>
					<?php $this->insert('components/page-contents', ['page_contents' => $page_contents]); ?>
				</div>
				<div class="col-md-12 top-bottom-padding ">
					<?php if (isset($galleries['pages'])): ?>	
						<?php foreach($galleries['pages'] as $key => $gallery): ?>
				  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>						
				 		<?php endforeach ?> 		
					<?php endif; ?>	
				</div>
			</div>
			
		</div>
	</div>	
	<?php if (isset($regions['content'])): ?>
		<div class="row">
			<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
		</div>	
	<?php endif; ?>
	<?php if (isset($regions['distances'])): ?>
		<div class="row background-color3">
			<?php $this->insert('regions/distances', ['region' =>$regions['distances']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['glance'])): ?>
		<div class="row background-color3">
			<?php $this->insert('regions/glance', ['region' =>$regions['glance']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['map'])): ?>
		<div class="row top-bottom-padding background-color6">
			<?php $this->insert('regions/map', ['region' =>$regions['map']]); ?>
		</div>			
	<?php endif; ?>			
</main>
<?php $this->stop() ?>