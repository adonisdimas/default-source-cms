<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main class="accommodation-page">	
	<div class="row slider-wrapper">
		<div class="content-padding content-width">
			<div class="row caption">
				<?php if (isset($translations['Caption'])){ echo $translations['Caption']; } ?>
			</div>		
		</div>	
		<?php if (isset($sliders['pages'])): ?>	
			<?php foreach($sliders['pages'] as $key => $slider): ?>
	  				<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>						
	 		<?php endforeach ?>
	 		<a href="#" class="scroll-down"><img src="img/icons/arrow-down2.png" /></a>				
		<?php endif; ?>			
	</div>
	<!--  Main-content Section -->
	<div class="row scroll-section background-color6">
		<div class="content-padding content-width-inner2">
			<div class="row top-bottom-padding">
				<div class="col-md-12 main-content">
					<h1><?=$page_translation->title;?></br>
					<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>					
				</div>				
			</div>
		</div>
	</div>
	<div class="row main-content background-color6">
		<?php if (isset($page_contents)): ?>	
			<?php foreach($page_contents as $key => $page_content): ?>
				<?php if($key>0): ?>
					<div class="col-md-12">
						<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>					
					</div>
				<?php endif; ?>								
	 		<?php endforeach ?>
		<?php endif; ?>								
	</div>
	<?php if (isset($regions['content'])): ?>
		<div class="row">
			<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
		</div>	
	<?php endif; ?>
	<?php if (isset($regions['distances'])): ?>
		<div class="row background-color3">
			<?php $this->insert('regions/distances', ['region' =>$regions['distances']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['glance'])): ?>
		<div class="row background-color3">
			<?php $this->insert('regions/glance', ['region' =>$regions['glance']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['map'])): ?>
		<div class="row top-bottom-padding background-color6">
			<?php $this->insert('regions/map', ['region' =>$regions['map']]); ?>
		</div>			
	<?php endif; ?>
</main>
<?php $this->stop() ?>