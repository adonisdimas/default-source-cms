<?php $this->layout('layout', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets,'galleries_assets' => $galleries_assets,'settings' => $settings]) ?>
<?php $this->start('head') ?>
	<?=$this->section('head')?>
<?php $this->stop() ?>
<?php $this->start('modal') ?>
	<?php if (isset($regions['offer'])): ?>
		<?php $this->insert('regions/offer', ['region' => $regions['offer']]); ?>
	<?php endif; ?>					
<?php $this->stop() ?>
<?php $this->start('header') ?>
	<?php if (isset($regions['header'])): ?>
		<?php $this->insert('regions/header', ['region' => $regions['header']]); ?>
	<?php endif; ?>					
<?php $this->stop() ?>
	<?=$this->section('main-content')?>
<?php $this->start('footer') ?>
	<?php if (isset($regions['footer'])): ?>
		<?php $this->insert('regions/footer', ['region' => $regions['footer']]); ?>		
	<?php endif; ?>					
<?php $this->stop() ?>
<?php $this->start('scripts') ?>
	<?=$this->section('scripts')?>
<?php $this->stop() ?>