<?php $this->layout('base', ['page_title'=>$page_title,'page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<div class="content-padding-xlarge">
		<?php if (isset($regions['slider'])): ?>
			<div class="row slider-wrapper">
			 	  <?php if (isset($regions['slider'])): ?>
				  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
				  <?php endif; ?>
				 <a href="#" class="scroll-down">SCROLL</a>
			</div>
		<?php endif; ?>
		<!--  Main-content Section -->
		<div class="row scroll-section home-section">
			<div class="row main-content title-align-mobile">
				<div class="row">
					<div class="row top-bottom-padding-large">
						<h1><?=$page_translation->title;?></h1>
						<div class="row top-bottom-padding-large highlight-text">
							<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>
						</div>
					</div>
					<div class="row">
						<?php $this->insert('components/page-content', ['page_content' => $page_contents[1]]); ?>
					</div>
					<?php if (isset($page_contents)): ?>
						<?php foreach($page_contents as $key => $page_content): ?>
							<?php if ($key>1): ?>
								<div class="row top-bottom-padding-large">
									<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>
								</div>
							<?php endif; ?>
						<?php endforeach ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if (isset($regions['consulting-services'])): ?>
		<section class="row services-teasers">
			<div class="content-padding-xlarge ">
				<?php $this->insert('regions/consulting-services', ['region' =>$regions['consulting-services']]); ?>
			</div>
		</section>
	<?php endif; ?>
	<?php if (isset($regions['epic-focus-on'])): ?>
		<section class="row focus-area">
			<?php $this->insert('regions/epic-focus-on', ['region' =>$regions['epic-focus-on']]); ?>
		</section>
	<?php endif; ?>
	<?php if (isset($regions['banner-contact'])): ?>
		<?php $this->insert('regions/banner-contact', ['region' =>$regions['banner-contact']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
		<div class="row top-bottom-padding-large"></div>
	<?php endif; ?>
</main>
<?php $this->stop() ?>