<?php $this->layout('base', ['page_title'=>$page_title,'page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<div class="content-padding-xlarge">
		<?php if (isset($regions['slider'])): ?>
			<div class="row slider-wrapper">
			 	  <?php if (isset($regions['slider'])): ?>
				  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
				  <?php endif; ?>
				 <a href="#" class="scroll-down">SCROLL</a>
			</div>
		<?php endif; ?>
		<!--  Main-content Section -->
		<div class="row scroll-section home-section padding-right">
			<div class="row main-content title-align-mobile">
				<?php if (isset($page_contents) && !empty($page_translation->title)): ?>
				<div class="row top-bottom-padding">
					<h1><?=$page_translation->title;?></h1>
					<div class="row padding-right">
						<?php foreach($page_contents as $key => $page_content): ?>
							<div class="col-md-12">
								<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>
							</div>
				 		<?php endforeach ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if (isset($regions['consulting-services'])): ?>
		<section class="row services-teasers">
			<div class="content-padding-xlarge ">
				<?php $this->insert('regions/consulting-services', ['region' =>$regions['consulting-services']]); ?>
			</div>
		</section>
	<?php endif; ?>
	<?php if (isset($regions['epic-focus-on'])): ?>
		<section class="row focus-area">
			<?php $this->insert('regions/epic-focus-on', ['region' =>$regions['epic-focus-on']]); ?>
		</section>
	<?php endif; ?>
	<?php if (isset($regions['banner-contact'])): ?>
		<?php $this->insert('regions/banner-contact', ['region' =>$regions['banner-contact']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['banner-small'])): ?>
		<?php $this->insert('regions/banner-small', ['region' =>$regions['banner-small']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['banner-large'])): ?>
		<?php $this->insert('regions/banner-large', ['region' =>$regions['banner-large']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
		<div class="row top-bottom-padding-large"></div>
	<?php endif; ?>
</main>
<?php $this->stop() ?>