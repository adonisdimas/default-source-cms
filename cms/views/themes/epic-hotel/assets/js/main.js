/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    };
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.manageStickyHeader();
			app.manageOffer();
			app.manageAnimations();
			app.manageSlider();
			app.manageGalleries();
			app.manageDropdown();
			app.manageBookOnline();
			app.manageScroll();
			app.manageGooglemap();
			$(window).on('resize', app.onResize);
		},
		'manageStickyHeader' : function() {
			var header = $('header');
		    $(window).scroll(function () {
	    		if ($(this).scrollTop() > 100) {
		            header.addClass("sticky");
	    		}else{
	    			header.removeClass("sticky");
	    		}
		    });
		},
		'manageOffer' : function() {
			if ( $("#offer-modal" ).length ) {
				$('#offer-modal').modal('show');
			}
		},
		'manageAnimations' : function() {
			 if($(window).width()>992){
				 $('.fotorama__arr').hide();
				setTimeout(function(){
					$('.slider-wrapper .caption').addClass('animated fadeInLeft');
					$('.slider-wrapper .caption').show();
				}, 1200);
				window.sr = ScrollReveal({ reset: true });
				sr.reveal('.reveal', { duration: 1800 });
			 }
		},
		'manageDropdown' : function() {
			$("#dropdown").on("click", function(){
				$(".navigation-box").css( "right", "-2px");
				$(".navigation-box").css( "opacity", "1");
			});
			$(".navigation-box .close-menu span").on("click", function(){
				$(".navigation-box").css( "right", "-1000px");
				$(".navigation-box").css( "opacity", "0");
			});
			//$(".navigation-box li.submenu").on("click", function(e){
			//	$(this).toggleClass('opened');
			//	console.log($(this).attr('href'));
			//	if($(this).hasClass('opened')){
			//		$(this).children('ul').css( "display", "block");
			//	}else{
			//		$(this).children('ul').css( "display", "none");
			//	}
			//});
		},
		'manageSlider' : function() {
				if($('.swiper-container').length){
					//initialize swiper when document ready
					var mySwiper = new Swiper ('.swiper-container', {
						// Optional parameters
						direction: 'horizontal',
						loop: true,
						effect: 'slide',
						calculateHeight: true,
						slidesPerView: 1,
						preloadImages: false,
						lazy: true
					})
					function fixSwiper(){
						$('.swiper-slide').css({minHeight: $('.swiper-container').height()});
					}
					fixSwiper();
					$(window).resize(function(){
						fixSwiper();
					})
				}
			    if($('.fotorama').length){
			    	 if($(window).width()>992){
						 $('.fotorama').fotorama({
							  maxwidth: '100%',
							  width: '100%',
							  height:'85%',
							  maxheight: '85%',
							  fit:'cover',
							  allowfullscreen: false,
							  transition : 'crossfade',
							  autoplay : '3000',
							  loop: true,
							  nav:false,
							  arrows: 'always'
						  });
					 }else{
						 $('.fotorama').fotorama({
							  maxwidth: '100%',
							  width: '100%',
							  height:'50%',
							  maxheight: '85%',
							  fit:'cover',
							  allowfullscreen: false,
							  transition : 'crossfade',
							  autoplay : '3000',
							  loop: true,
							  nav:false,
							  arrows: 'always'
						  });
					 }
				  }
				  if($('.bxslider').length){
					var slider = $('.bxslider').bxSlider({
						captions: true,
						auto: ($('.bxslider').children().length > 1) ? true : false,infiniteLoop :  true,
						autoControls: false,
						preventDefaultSwipeX: false,
						hideControlOnEnd: false,
						touchEnabled: true,
	                    preventDefaultSwipeY:false,
	                    swipeThreshold:1,
	                    useCSS: false,
	                    easing: 'swing',
	                    responsive:true,
	                    speed:1250,
	                    preloadImages:'all',
	                    swipeRight: function(event, direction, distance, duration, fingerCount) {
							slider.goToPrevSlide();
						},
						swipeLeft: function(event, direction, distance, duration, fingerCount) {
							slider.goToNextSlide();
						},
						pager:false,
					});
				  }
				if($(".lightslider > li").length>1){
				    $('.lightslider').lightSlider({
				    	item:1,
				        loop:true,
				        slideMove:1,
				    	autoWidth:true,
				        adaptiveHeight: true,
				        pager:true
				    });
				}
		},
		'manageGalleries' : function() {
			 if ($('.fancybox').length){
					$('a.fancybox[rel=group]').fancybox({
		        		'transitionIn'		: 'elastic',
		        		'transitionOut'		: 'fade',
		        		'titlePosition' 	: 'over',
		        		'padding' : 0,
		        		'overlayColor' : '#000000',
		        		'overlayOpacity' : '0.6',
		        		'autoScale'		: true
					});
			 }
			 if ($('.light-gallery').length){
				 $('.light-gallery').lightGallery({
					    mode: 'lg-fade',
					    cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
					    thumbnail:true,
					    animateThumb: false,
					    showThumbByDefault: false
					});
			 }
         if ($('.masonry-gallery').length){
         	$('.masonry-gallery').masonry({
               itemSelector: '.gallery-item',
               columnWidth:1,
               gutter:5,
               fitWidth: true
             });
         }
         if ($('.teasers-carousel').length){
			  $(".teasers-carousel").owlCarousel({
				  	items:1,
				    margin:5,
				    loop:true,
				    nav: true,
				    pagination : true,
			        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
			  });
         }
         if ($('.carousel-gallery').length){
				  $(".carousel-gallery").owlCarousel({
					  	items:6,
					    margin:5,
					    loop:true,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });
         }
			$('a.gallery-thumb').fancybox({
     		'transitionIn'		: 'elastic',
     		'transitionOut'		: 'fade',
     		'titlePosition' 	: 'over',
     		'padding' : 0,
     		'overlayColor' : '#000000',
     		'overlayOpacity' : '0.6',
     		'autoScale'		: true
			});
         if ($('.teaser-gallery').length){
				$(".teaser-gallery .teaser-item").on("click", function(e){
					e.preventDefault();
					$(this).find('a')[0].click();
				});
				  $(".teaser-gallery").owlCarousel({
					  	items:6,
					    margin:2,
					    loop:true,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });

         }
		},
		'manageBookOnline' : function() {
			$("a.book-mobile").on("click", function(e){
				 e.preventDefault();
				$(".book-online-form").toggle();
				//$("header .menu").css( "display", "block");
				$(this).toggleClass("active");
			});
			var datepicker1 = $("#datepicker1").datepicker({
			    dateFormat: 'dd-M-yy',
			    changeMonth: true,
			    minDate: new Date(),
			    maxDate: '+2y',
			    onSelect: function(date){

			        var selectedDate = new Date(date);
			        var msecsInADay = 86400000;
			        var endDate = new Date(selectedDate.getTime() + msecsInADay);

			       //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
			        $("#datepicker2").datepicker( "option", "minDate", endDate );
			        $("#datepicker2").datepicker( "option", "maxDate", '+2y' );

			    }
			});
			var datepicker2 = $("#datepicker2").datepicker({
			    dateFormat: 'dd-M-yy',
			    changeMonth: true
			});
			var currentDate = new Date();
			datepicker1.datepicker("setDate", currentDate);
			currentDate.setDate(currentDate.getDate()+2);
			datepicker2.datepicker("setDate", currentDate);
		},
		'manageScroll' : function() {
			//Check to see if the window is top if not then display button
			$(window).scroll(function(){
				if($(window).width()>1200){
					if ($(this).scrollTop() > 100) {
						$('.scrollToTop').fadeIn();
					} else {
						$('.scrollToTop').fadeOut();
					}
			     }
			});
			//Click event to scroll to top
			$('.scrollToTop').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : 0},800);
				return false;
			});
			//Click event to scroll to top
			$('.scroll-down').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : $(".scroll-section").offset().top-130},800);
				return false;
			});
		},
		'manageGooglemap' : function() {
			if ( $( "#map-canvas" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	 var infowindow = new google.maps.InfoWindow({
                		    content: "<strong>Epic Hotel</strong> , Greece"
                	 });
					var mapOptions = {
						zoom: 15,
						center: new google.maps.LatLng(36.905872, 27.284698),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false,
	    				styles:[]
					};
					var map = new google.maps.Map(document.getElementById('map-canvas'),
					  mapOptions);
					window.map = map;
					var customIcon = 'img/icons/pin.png';
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(36.905872, 27.284698),
						icon: customIcon,
						map: map
					});
	          		marker.addListener('click', function() {
	          		   infowindow.open(map, marker);
	          		});
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        //var styledMapType = new google.maps.StyledMapType(
			        	//	[],
			          //{name: 'Styled Map'});
					//var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					//map.mapTypes.set("Map Style", mapType);
					//map.setMapTypeId("Map Style");
					};

                window.onload = function() {setupMap()};
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}
		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
			}, 10);
		}
	};
	$(document).on('ready', app.init);
})(jQuery);