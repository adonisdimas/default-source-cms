<form class="contact-form" method="post" action='<?=SUBFOLDER?><?=ADMIN_PATH?>/mailer'>
    <div class="form-group row">
    	<div class="col-md-6">
    		<label for="firstname"><?=$region['translations']['Contact Form Name']; ?></label>
    		<input type="text" name="firstname" required="">
    	</div>
	    <div class="col-md-6">
	    	<label for="surname"><?=$region['translations']['Contact Form Surname']; ?></label>
	    	<input type="text" name="surname" required="">
	    </div>
    </div>
    <div class="form-group row">
    	<div class="col-md-6">
    		<label for="email"><?=$region['translations']['Contact Form Email']; ?></label>
    		<input type="email" name="email" required="">
    	</div>
	    <div class="col-md-6">
	    	<label for="telephone"><?=$region['translations']['Contact Form Tel']; ?></label>
	    	<input type="tel" name="telephone">
	    </div>
    </div>
    <div class="form-group row">
    	<label for="message"><?=$region['translations']['Contact Form Message']; ?></label>
    	<textarea name="message" rows="10" cols="30" required=""></textarea>
    </div>
    <div class="form-group row">
    	<div class="col-md-4">
    		<label for="code"><?=$region['translations']['Contact Form Code']; ?></label>
    		<input type="text" name="code" id="code" class="textfield_2">
    		<img class="sec_code" src="<?=ROOT.ADMIN_PATH?>/captcha?randstring=<?php echo rand(999,9999); ?>" alt="verification image, type it in the box" name="ver_img" width="40" height="18" vspace="0" align="top" id="ver_img">
                <?php if(isset( $_GET['send'])&& ($_GET['send'] == -1 or $_GET['send'] == 0)){?>
                <div style="background-color:#D70000; color:#FFFFFF; height:30px; padding:0px; padding-left:5px;width:185px;margin-left:0px;font-size:10px;font-size: 10px;position:absolute;top: 80px;
    left: 0;">
                <div align="center"><?php echo $region['translations']['Contact Form Failure Message']; ?></div>
                </div>
                <?php ;}?>
                <?php if(isset( $_GET['send'])&&$_GET['send'] == 1){?>
                <div style="background-color:#83831f; color:#FFFFFF; padding:0px; height:30px; padding-left:5px;width:185px;margin-left:0px;position: absolute;font-size: 10px;position:absolute;top: 80px;
    left: 0;">
                <div><?php echo $region['translations']['Contact Form Success Message']; ?></div>
                </div>
                <?php ;}?>
    	</div>
    	<div class="col-md-8">
    	    <input type="submit" value="<?=$region['translations']['Contact Form Submit']; ?>">
    	</div>
    </div>
    <br>
		<label><span><?=$region['translations']['Contact Form Required']; ?></span></label>
    <input type="hidden" name="uri" value="/contact" >
</form>