<?php if (isset($region['blocks'])): ?>
	<?php foreach($region['blocks'] as $key => $block): ?>
		<section class="row top-bottom-margin-large" style="background: url(<?=ROOT.'/'.MEDIA_PATH.'/'.$block['contents'][1][0]->media_path?>); background-position:center;background-size:cover; ">
			<div class="banner-overlay-small">
				<div class="content-width-inner">
					<div class="row">
						<div class="<?=$block['contents'][0]?>">
							<div class="row">
								<?=$block['contents'][1][0]->content?>
							</div>
							<div class="row">
								<div class="button">
									<a href="<?=SUBFOLDER.$block['contents'][1][0]->link?>"><?=$block['contents'][1][0]->title?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endforeach; ?>
<?php endif; ?>