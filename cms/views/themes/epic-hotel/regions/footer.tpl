<footer>
	<div class="content-padding-xlarge">
		<div class="row top">
			<div class="row">
				<div class="col-md-2 connect">
					<div class="logo">
						<a href="<?=ROOT?>/<?=$GLOBALS['default_locale']?>/"></a>
					</div>
				</div>
				<?php if (isset($region['menus'])): ?>
					<?php foreach($region['menus'] as $menu): ?>
							<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'title'=>'Footer Menu','menu' => $menu['contents'][1]]); ?>	
					<?php endforeach ?>
				<?php endif; ?>
				<?php if (isset($region['blocks'])): ?>
					<?php foreach($region['blocks'] as $block): ?>
							<?php $this->insert('components/block', ['classes' => $block['contents'][0],'block' => $block['contents'][1]]); ?>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</footer>