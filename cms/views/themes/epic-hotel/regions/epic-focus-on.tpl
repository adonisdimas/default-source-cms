<div class="content-padding-xlarge">
	<div class="row title-align-mobile">
		<div class="row">
			<h2><?=$region['translations']['Focus Title 1']; ?></h2>
		</div>
	</div>
</div>
<div class="tree-wrapper background-color8">
	<div class="content-padding-xlarge">
		<div class="row">
			<div class="row title-align-mobile">
				<h2><?=$region['translations']['Focus Title 2']; ?></h2>
				<h3><?=$region['translations']['Focus Title 3']; ?></h3>
			</div>
		</div>
		<? if (isset($region['blocks'][0])): ?>
			<?php
				$this->insert('components/block', ['classes'=>$region['blocks'][0]['contents'][0],'block' => $region['blocks'][0]['contents'][1]]); 
			?>
		<? endif; ?>
	</div>
	<div class="row top-bottom-padding-large">
		<div class="row top-bottom-padding title-align-mobile">
			<div class="content-padding-xlarge">
				<h3 class="white"><?=$region['translations']['Focus Title 4']; ?></h3>
				<? if (isset($region['blocks'][1])): ?>
					<?php
						$this->insert('components/block', ['classes'=>$region['blocks'][1]['contents'][0],'block' => $region['blocks'][1]['contents'][1]]); 
					?>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>