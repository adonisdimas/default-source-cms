<div class="row fixed-info">
	<?=$region['translations']['Slider Info Bar']; ?>
	<div class="copyright"><p>Created by - dProject</p></div>
</div>
<div class="row">
	<div class="row caption">
		<?=$region['translations']['Slider Caption'];?>
	</div>
</div>
<?php if(isset($sliders)): ?>
	<?php foreach($sliders as $key => $slider): ?>
 		<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>
	<?php endforeach ?>
<?php endif; ?>