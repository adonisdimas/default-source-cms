<div class="row">
	<div class="row">
		<h2><?=$region['translations']['Consulting Services Title']; ?></h2>
	</div>
	<div class="row">
		<?php if (isset($region['blocks'])): ?>
			<?php foreach($region['blocks'] as $key => $block): ?>
				<div class="<?=$block['contents'][0]?>">
					<div class="teaser-image">
						<a href="<?=SUBFOLDER.$block['contents'][1][0]->link?>"><span class="image-border"></span></a>
						<img src="<?=ROOT.'/'.MEDIA_PATH.'/'.$block['contents'][1][0]->media_path?>" />
					</div>
					<div class="teaser-info">
						<a href="<?=SUBFOLDER.$block['contents'][1][0]->link?>"><h3><span><?=$block['contents'][1][0]->title?></span></h3></a>
					</div>
					<?=$block['contents'][1][0]->ceontent?>
				</div>
			<?php endforeach ?>
		<?php endif; ?>
	</div>
</div>