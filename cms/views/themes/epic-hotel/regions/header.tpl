<header>
	<div class="content-padding-xlarge">
		<div class="row">
			<div class="logo">
				<a href="<?=ROOT?>/<?=$GLOBALS['default_locale']?>/"></a>
			</div>
			<div id="dropdown">
				<span>MENU</span>
			</div>
			<div class="navigation-box">
				<div class="close-menu"><span></span></div>
				<nav class="menu">
					<?php if (isset($region['menus'])): ?>
						<ul class="<?=$region['menus'][0]['contents'][0]?>">
							<li class="<?php if($page_title =='Home'){ echo 'active';}?>"><a href="<?=SUBFOLDER?><?php echo $region['menus'][0]['contents'][1][0]->link;?>"><?php echo $region['menus'][0]['contents'][1][0]->title;?></a><span>/01</span></li>
							<li class="submenu opened <?php if($page_title =='Home'){ echo 'active';}?>"><a href="<?=SUBFOLDER?><?php echo $region['menus'][0]['contents'][1][1]->link;?>"><?php echo $region['menus'][0]['contents'][1][1]->title;?></a><span>/02</span>
								<ul>
									<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][1]['contents'][1][0]->link;?>"><?php echo $region['menus'][1]['contents'][1][0]->title;?></a></li>
								</ul>
							</li>
							<li class="submenu opened <?php if($page_title =='Home'){ echo 'active';}?>"><a href="<?php echo $region['menus'][0]['contents'][1][2]->link;?>"><?php echo $region['menus'][0]['contents'][1][2]->title;?></a><span>/03</span>
								<ul>
									<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][2]['contents'][1][0]->link;?>"><?php echo $region['menus'][2]['contents'][1][0]->title;?></a></li>
									<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][2]['contents'][1][1]->link;?>"><?php echo $region['menus'][2]['contents'][1][1]->title;?></a></li>
									<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][2]['contents'][1][2]->link;?>"><?php echo $region['menus'][2]['contents'][1][2]->title;?></a></li>
									<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][2]['contents'][1][3]->link;?>"><?php echo $region['menus'][2]['contents'][1][3]->title;?></a></li>
								</ul>
							</li>
							<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][0]['contents'][1][3]->link;?>"><?php echo $region['menus'][0]['contents'][1][3]->title;?></a><span>/04</span></li>
							<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][0]['contents'][1][4]->link;?>"><?php echo $region['menus'][0]['contents'][1][4]->title;?></a><span>/05</span></li>
							<li><a href="<?=SUBFOLDER?><?php echo $region['menus'][0]['contents'][1][5]->link;?>"><?php echo $region['menus'][0]['contents'][1][5]->title;?></a><span>/06</span></li>
						</ul>
					<?php endif; ?>
				</nav>
				<div class="row">
					<?php if (isset($region['blocks'])): ?>
						<?php foreach($region['blocks'] as $block): ?>
							<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
						<?php endforeach ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</header>