<section class="row top-bottom-padding-large">
	<div class="content-padding-xlarge">
		<div class="col-md-12">
			<h2><?=$region['translations']['Map Title']; ?></h2>
		</div>
		<div class="col-md-12">
			<div class="map-wrapper">
				<div class="map-top-shadow"></div>
				<div class="map">
					<div id="map-canvas"></div>
				</div>
			</div>
		</div>
	</div>
</section>