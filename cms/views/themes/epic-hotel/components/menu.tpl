<?php if ($title=='Footer Menu'): ?>
	<div class="<?=$classes?>">
		<h3>Menu</h3>
		<nav class="quick-menu">
			<ul class="col-xs-12 col-md-12">
				<?php foreach($menu as $key => $menu_item): ?>
					<li <?php if(count($menu)==$key+1){echo 'class="last"';}?>>
						<a href="<?=SUBFOLDER?><?=$menu_item->link?>" <?=$this->uri($menu_item->link, 'class="active"')?>><?=$menu_item->title?></a>
					</li>
				<?php endforeach ?>
			</ul>
		</nav>
	</div>
<?php else: ?>
	<ul class="<?=$classes?>">
		<?php foreach($menu as $key => $menu_item): ?>
			<li <?php if(count($menu)==$key+1){echo 'class="last"';}?>>
				<a href="<?=SUBFOLDER?><?=$menu_item->link?>" <?=$this->uri($menu_item->link, 'class="active"')?>><?=$menu_item->title?></a>
			</li>
	 	<?php endforeach ?>
 	</ul>
<?php endif; ?>
