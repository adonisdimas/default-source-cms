<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<head>
  <title><?=$this->e($page_translation->meta_title)?></title>
	<meta charset="utf-8" />
	<meta content="IE=edge" http-equiv="X-UA-Compatible" />
	<meta name="keywords" content="<?=$page_translation->meta_keywords?>" />
	<meta name="description" content="<?=$page_translation->meta_description?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php foreach($theme_assets['styles'] as $asset): ?>
		<link href="<?=ROOT?>/<?=$theme_assets_path?>/css/<?=$asset;?>" rel="stylesheet" />
 	<?php endforeach ?>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,400italic,700italic,300italic&subset=latin,greek' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700&amp;subset=latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500&amp;subset=latin-ext" rel="stylesheet">
	<?=$this->section('head')?>
</head>
<body>
  <?=$this->section('modal')?>
	<?=$this->section('header')?>
	<?=$this->section('main-content')?>
	<?=$this->section('footer')?>
	<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/lib/jquery.min.js"></script>
	<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/lib/jquery-ui.min.js"></script>
	<?php foreach($theme_assets['scripts'] as $script): ?>
		<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/<?=$script;?>"></script>
 	<?php endforeach ?>
 	<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/main.js"></script>
	<?=$this->section('scripts')?>
</body>
</html>