<footer>
	<div class="row">
		<div class="content-width content-padding">
			<div class="row">
				<?php if (isset($region['blocks'])): ?>
				  <div class="row">
					<?php foreach($region['blocks'] as $block): ?>
							<?php $this->insert('components/block', ['classes' => $block['contents'][0],'block' => $block['contents'][1]]); ?>
					<?php endforeach ?>
				  </div>
				<?php endif; ?>
				<?php if (isset($region['menus'])): ?>
				 	<div class="row">
						<?php foreach($region['menus'] as $menu): ?>
								<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>	
						<?php endforeach ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>	
</footer>