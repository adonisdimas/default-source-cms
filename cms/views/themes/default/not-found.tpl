<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<head>
    <title><?=$this->e($title)?></title>
	<meta charset="utf-8" />
	<meta content="IE=edge" http-equiv="X-UA-Compatible" />
	<meta name="keywords" content="<?=$page_translation->meta_keywords?>" />
	<meta name="description" content="<?=$page_translation->meta_description?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="/<?=$theme_assets_path?>/css/style.css" rel="stylesheet" />
  	<link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300,700,500&subset=latin,greek' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,400italic,700italic,300italic&subset=latin,greek' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i&amp;subset=cyrillic,latin-ext" rel="stylesheet">
	<?=$this->section('head')?>
</head>
<body>
	<?=$this->section('header')?>
	<div class="container">
		<div class="content-width content-padding">
		    <div class="row">
		        <div class="col-md-12">
		            <div class="error-template">
		                <h1>
		                    Oops!</h1>
		                <h2>
		                    404 Not Found</h2>
		                <div class="error-details">
		                    Sorry, an error has occured, Requested page not found!
		                </div>
		                <div class="error-actions">
		                    <a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
		                        Take Me Home </a>
		                </div>
		            </div>
		        </div>
		    </div>
	    </div>
	</div>
	<?=$this->section('footer')?>
	<script src="/<?=$theme_assets_path?>/js/lib/jquery.min.js"></script>
	<script src="/<?=$theme_assets_path?>/js/lib/jquery-ui.min.js"></script>
	<?=$this->section('scripts')?>
</body>
</html>
