<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<div class="row slider-wrapper">
		<?php if (isset($sliders['pages'])): ?>	
			<?php foreach($sliders['pages'] as $key => $slider): ?>
	  				<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>						
	 		<?php endforeach ?>
		<?php endif; ?>
		<?php if (isset($regions['book-online'])): ?>
				<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'theme_assets_path' => $theme_assets_path]); ?>			
		<?php endif; ?>
	</div>
	<!--  Main-content Section -->
	<div class="row scroll-section background1">
		<div class="content-width content-padding">
			<div class="row top-bottom-padding">
				<div class="main-content col-md-12">
					<h1 class="center-align"><span class="decor-l"></span><?=$page_translation->title;?><span class="decor-r"></span></h1>
					<?php $this->insert('components/page-contents', ['page_contents' => $page_contents]); ?>
				</div>				
			</div>
			<?php if (isset($regions['contact-form'])): ?>
				<div class="row">
					<?php $this->insert('regions/contact-form', ['region' =>$regions['contact-form']]); ?>	
				</div>				
			<?php endif; ?>	
		</div>
		<div class="row top-bottom-padding">
			<?php if (isset($galleries['pages'])): ?>	
				<?php foreach($galleries['pages'] as $key => $gallery): ?>
		  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>						
		 		<?php endforeach ?>
				<div class="col-md-12 center-align">
					<div class="button"><a href=""><?php if (isset($translations['Button title'])){ echo $translations['Button title']; }?></a></div>
				</div>		 		
			<?php endif; ?>			
		</div>
	</div>
	<?php if (isset($regions['content'])): ?>
		<div class="row">
			<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['map'])): ?>
		<div class="row top-bottom-padding background-color6">
			<?php $this->insert('regions/map', ['region' =>$regions['map']]); ?>
		</div>			
	<?php endif; ?>	
	<div class="row top-bottom-padding background-color6"></div>		
</main>
<?php $this->stop() ?>