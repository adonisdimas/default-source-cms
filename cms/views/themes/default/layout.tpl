<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<head>
    <title><?=$this->e($page_translation->meta_title)?></title>
	<meta charset="utf-8" />
	<meta content="IE=edge" http-equiv="X-UA-Compatible" />
	<meta name="keywords" content="<?=$page_translation->meta_keywords?>" />
	<meta name="description" content="<?=$page_translation->meta_description?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php foreach($theme_assets['styles'] as $asset): ?>
		<link href="<?=$theme_assets_path?>/css/<?=$asset;?>" rel="stylesheet" />
 	<?php endforeach ?>
	<?php foreach($sliders_assets['styles'] as $asset): ?>
		<link href="<?=$asset;?>" rel="stylesheet" />
 	<?php endforeach ?>
 	<?php foreach($galleries_assets['styles'] as $asset): ?>
		<link href="<?=$asset;?>" rel="stylesheet" />
 	<?php endforeach ?>
  	<link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300,700,500&subset=latin,greek' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,400italic,700italic,300italic&subset=latin,greek' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i&amp;subset=cyrillic,latin-ext" rel="stylesheet">
	<?=$this->section('head')?>
</head>
<body>
    <?=$this->section('modal')?>
	<?=$this->section('header')?>
	<?=$this->section('main-content')?>
	<?=$this->section('footer')?>
	<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/lib/jquery.min.js"></script>
	<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/lib/jquery-ui.min.js"></script>
	<?php foreach($theme_assets['scripts'] as $script): ?>
		<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/<?=$script;?>"></script>
 	<?php endforeach ?>
	<?php foreach($sliders_assets['scripts'] as $script): ?>
		<script src="<?=ROOT?>/<?=$script;?>"></script>
 	<?php endforeach ?>
	<?php foreach($galleries_assets['scripts'] as $script): ?>
		<script src="<?=ROOT?>/<?=$script;?>"></script>
 	<?php endforeach ?>
 	<script src="<?=ROOT?>/<?=$theme_assets_path?>/js/main.js"></script>
 	<?php if (isset($settings['Google MAP API KEY'])): ?>
 		<script src="https://maps.googleapis.com/maps/api/js?key=<?=$settings['Google MAP API KEY'];?>" type="text/javascript" defer></script>
 	<?php endif ?>
 	<?php if (isset($settings['Google Analytics ID'])): ?>
		<script type="text/javascript">var _gaq = _gaq || [];_gaq.push(['_setAccount', <?=$settings['Google Analytics ID'];?>]);_gaq.push(['_trackPageview']);
		    (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);         })();
		</script> 	
 	<?php endif ?>
	<?=$this->section('scripts')?>
</body>
</html>