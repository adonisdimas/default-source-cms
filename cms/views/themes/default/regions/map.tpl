<div class="content-padding content-width content-padding">
	<div class="col-md-12 center-align"><h2><?php if (isset($region['translations'][0])): ?><?php echo $region['translations'][0]->value; ?></h2><?php endif; ?></div>		
</div>
<div class="col-md-12">
	<div class="map-wrapper">
		<div class="map-top-shadow"></div>
		<div class="map">
			<div id="map-canvas"></div>
		</div>
		<div class="map-bottom-shadow"></div>
	</div>
	<div class="location-box">
		<?php if (isset($region['blocks'])): ?>
			<div class="contents">
				<?php foreach($region['blocks'] as $block): ?>
					<?php $this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			</div>
		<?php endif; ?>
	</div>
</div>