<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <a href="#" class="animated bounce infinite scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<div class="row scroll-section">
		<?php if (isset($regions['book-online'])): ?>
			<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
		<?php endif; ?>
		<!--  Main-content Section -->
		<div class="row">
			<div class="row main-content top-bottom-padding-large">
				<div class="row center-align top-bottom-padding scroll-section"><h1><?=$page_translation->title;?></h1></div>
				<div class="row center-align ">
					<div class="col-md-12 background-color5 top-bottom-padding-large">
						<div class="content-width-inner">
							<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>
						</div>
					</div>
				</div>
			</div>
			<?php if($page_contents[1]):?>
				<div class="row main-content top-bottom-padding-large">
					<div class="content-width-inner row">
						<?php $this->insert('components/page-content', ['page_content' => $page_contents[1]]); ?>
					</div>
				</div>
			<?php endif;?>
			<?php if (isset($galleries['pages'])): ?>
				<div class="row center-align top-bottom-padding-large">
					<div class="col-md-12">
						<?php foreach($galleries['pages'] as $key => $gallery): ?>
					  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>
				 		<?php endforeach ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if($page_contents[2]):?>
				<div class="row main-content top-bottom-padding-large">
					<div class="content-width-inner row">
						<?php $this->insert('components/page-content', ['page_content' => $page_contents[2]]); ?>
					</div>
				</div>
			<?php endif;?>
			<?php if (isset($page_contents[3])): ?>
			<!--  OTHER ROOMS -->
			<section class="row accommodation-links top-bottom-padding background-color5">
				<div class="row center-align">
					<h2><?=$translations['Other Rooms Title'];?></h2>
				</div>
				<div class="row center-align top-bottom-padding">
					<div class="col-md-12 boxes">
						<div class="content-width-inner wrapper">
							<?=$page_contents[3]->content?>
						</div>
					</div>
				</div>
			</section>
			<?php endif; ?>
	</div>
	<!--  HIGHLIGHTS-->
	<?php if (isset($regions['highlights'])): ?>
		<?php $this->insert('regions/highlights', ['region' =>$regions['highlights']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['why-book'])): ?>
		<?php $this->insert('regions/why-book', ['region' =>$regions['why-book']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
	<?php endif; ?>
	<div class="row top-bottom-padding-large"></div>
</main>
<?php $this->stop() ?>