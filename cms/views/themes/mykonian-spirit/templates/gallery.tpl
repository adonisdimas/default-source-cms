<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <?php if (isset($regions['book-online'])): ?>
					<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
			  <?php endif; ?>
			  <a href="#" class="animated bounce infinite scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<section class="row gallery scroll-section">
		<div class="row center-align scroll-section">
			<h1><?=$page_translation->title;?></h1>
			<?php foreach($page_contents as $key => $page_content): ?>
				<div class="row content-width-inner main-content center-align">
					<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>
				</div>
			<?php endforeach ?>
		</div>
		<?php if (isset($galleries['pages'])): ?>
			<?php foreach($galleries['pages'] as $key => $gallery): ?>
				<div class="row">
					<div class="col-md-12 main-content top-bottom-padding <?php if($key % 2 == 0){echo 'background-color5';}?>">
						<div class="row center-align"><h2 class="decor"><?=$gallery[0]->title;?></h2></div>
						<div class="content-width-inner top-bottom-padding">
				  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif; ?>
	</section>
	<?php if (isset($regions['highlights'])): ?>
		<?php $this->insert('regions/highlights', ['region' =>$regions['highlights']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['why-book'])): ?>
		<?php $this->insert('regions/why-book', ['region' =>$regions['why-book']]); ?>
	<?php endif; ?>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
	<?php endif; ?>
	<div class="row top-bottom-padding-large"></div>
</main>
<?php $this->stop() ?>





