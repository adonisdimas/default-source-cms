<!--  HIGHLIGHTS-->
<section class="row highlights">
	<div class="content-width-inner center-align">
		<h2><?php echo $region['translations']['Highlights Title']; ?></h2>
	</div>
	<div class="main-content">
		<div class="content-width-inner row">
			<div class="col-md-2 icon">
				<div class="hightlight1"></div>
				<p><?php echo $region['translations']['Highlights 1']; ?></p>
			</div>
			<div class="col-md-2 icon">
				<div class="hightlight2"></div>
				<p><?php echo $region['translations']['Highlights 2']; ?></p>
			</div>
			<div class="col-md-2 icon active">
				<div class="hightlight3"></div>
				<p><?php echo $region['translations']['Highlights 3']; ?></p>
			</div>
			<div class="col-md-2 icon">
				<div class="hightlight4"></div>
				<p><?php echo $region['translations']['Highlights 4']; ?></p>
			</div>
			<div class="col-md-2 icon">
				<div class="hightlight5"></div>
				<p><?php echo $region['translations']['Highlights 5']; ?></p>
			</div>
			<div class="col-md-2 icon">
				<div class="hightlight6"></div>
				<p><?php echo $region['translations']['Highlights 6']; ?></p>
			</div>
		</div>
	</div>
</section>