<header class="<?php if($page->page_id==6 or $page->page_id==4 or $page->page_id==13 or $page->page_id==14 or $page->page_id==12){ echo 'no-slider';}?>">
	<div class="content-width-outer">
		<div class="row">
			<div class="col-md-4 logo">
				<a href="<?=ROOT?>/<?=$GLOBALS['default_locale']?>/"></a>
			</div>
			<div class="col-md-8 navigation content-padding">
				<div class="nav-bar1">
					<div id="dropdown"><span class="glyphicon glyphicon-menu-hamburger fa-2x" aria-hidden="true"></span></div>
					<nav class="menu">
						<?php if (isset($region['menus'])): ?>
							<ul class="row">
								<?php foreach($region['menus'][0]['contents'][1] as $key => $menu_item): ?>
							  		<li class="<?php if($key==1 or $key==3){echo 'submenu';}?> <?php if(count($menu)==$key+1){echo 'last';}?>">
							  			<a href="<?=ROOT?><?=$menu_item->link?>" <?php if($_SERVER['REQUEST_URI'] == SUBFOLDER.$menu_item->link){ echo 'class="active"';} ?>><?=$menu_item->title?></a>
							  		<?php
							  			//Load submenu in location
							  			if(isset($region['menus'][1])&&$key==1){
								  			$this->insert('components/menu', ['classes'=>$region['menus'][1]['contents'][0],'menu' => $region['menus'][1]['contents'][1]]);
							  			}
							  			//Load submenu in villas
							  			if(isset($region['menus'][2])&&$key==3){
								  			$this->insert('components/menu', ['classes'=>$region['menus'][2]['contents'][0],'menu' => $region['menus'][2]['contents'][1]]);
							  			}
							  		?>
							  		</li>
							 	<?php endforeach ?>
							</ul>
						<?php endif; ?>
					</nav>
				</div>
			</div>
		</div>
		<?php if (isset($region['blocks'])): ?>
			<div class="row">
				<?php foreach($region['blocks'] as $block): ?>
					<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			</div>
		<?php endif; ?>
	</div>
</header>