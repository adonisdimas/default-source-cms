<?php if (isset($region['blocks'])): ?>
	<?php foreach($region['blocks'] as $key => $block): ?>
		<div class="<?=$block['contents'][0]?>">
			<div class="row content-width-inner top-bottom-padding-large">
				<div class="col-md-8 image">
					<a href="<?=$block['contents'][1][0]->link?>"><span class="image-border"></span></a>
					<img src="<?=ROOT.'/'.MEDIA_PATH.'/'.$block['contents'][1][0]->media_path?>" />
				</div>
				<div class="col-md-4 text">
					<h3><a href="<?=$block['contents'][1][0]->link?>"><?=$block['contents'][1][0]->title?></a></h3>
					<div class="line"></div>
					<?=$block['contents'][1][0]->content?>
				</div>
			</div>					
		</div>
	<?php endforeach ?>
<?php endif; ?>






