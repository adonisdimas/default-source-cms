<!--  WHY BOOK WITH US-->
<section class="row why-book top-bottom-padding">
	<div class="content-width-inner2 center-align">
		<h2><?php echo $region['translations']['Why Book Title']; ?></h2>
	</div>
	<div class="main-content">
		<div class="content-width-inner row">
			<div class="col-md-4">
				<div class="check"></div>
				<p><?php echo $region['translations']['Why Book 1']; ?></p>
				<div class="tooltip">
					<span class="tooltip-inner">
					<?php echo $region['translations']['Why Book 1 tooltip']; ?>
					</span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="check"></div>
				<p><?php echo $region['translations']['Why Book 2']; ?></p>
				<div class="tooltip">
					<span class="tooltip-inner">
					<?php echo $region['translations']['Why Book 2 tooltip']; ?>
					</span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="check"></div>
				<p><?php echo $region['translations']['Why Book 3']; ?></p>
				<div class="tooltip">
					<span class="tooltip-inner">
					<?php echo $region['translations']['Why Book 3 tooltip']; ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</section>