<footer>
	<div class="row top">
		<div class="content-width-inner content-padding">
			<?php if (isset($region['blocks'])): ?>
				<?php foreach($region['blocks'] as $block): ?>
						<?php $this->insert('components/block', ['classes' => $block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			<?php endif; ?>		
		</div>
	</div>
	<div class="row bottom">
		<div class="content-width-inner content-padding">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6 copy">
					<p> &copy; <?php echo date("Y") ?> <?php echo $region['translations']['Copyright']; ?></p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 created">
					<p><?php echo $region['translations']['Created by']; ?> <a target="new" href="http://www.dproject.gr">DPROJECT</a></p>
				</div>
			</div>
		</div>
	</div>	
</footer>