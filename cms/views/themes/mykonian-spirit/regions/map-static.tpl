<section class="row top-bottom-padding">
	<div class="content-width-inner center-align">
		<h2><?php echo $region['translations']['Map Title']; ?></h2>
	</div>
	<div class="col-md-12">
	<div class="location-box">
			<div class="contents">
				<?php foreach($region['blocks'] as $block): ?>
					<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			</div>
		</div>
		<div class="map-wrapper">
			<div class="map-top-shadow"></div>
			<div class="map">
				<div id="map-canvas"></div>
			</div>
		</div>
	</div>
</section>

