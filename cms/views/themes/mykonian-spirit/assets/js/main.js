/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.manageStickyHeader();
			app.manageOffer();
			app.manageAnimations();
			app.manageSlider();
			app.manageGalleries();
			app.manageDropdown();
			app.manageBookOnline();
			app.manageScroll();
			app.manageGooglemap();
			$(window).on('resize', app.onResize);
		},
		'manageStickyHeader' : function() {
			var header = $('header');
		    $(window).scroll(function () {
	    		if ($(this).scrollTop() > 100) {
		            header.addClass("sticky");
	    		}else{
	    			header.removeClass("sticky");
	    		}
		    });
		},
		'manageOffer' : function() {
			if ( $("#offer-modal" ).length ) {
				$('#offer-modal').modal('show');
			}
		},
		'manageAnimations' : function() {
			$('.fotorama__caption').hide();
			 if($(window).width()>992){
				setTimeout(function(){
					$('.slider-wrapper .caption').addClass('animated zoomIn');
					$('.slider-wrapper .caption').show();
					$('.fotorama__caption').addClass('animated fadeInUp');
					$('.fotorama__caption').show();
				}, 1400);
			 }
		},
		'manageDropdown' : function() {
			$("#dropdown").on("click", function(){
				$("header .menu").slideToggle();
				$(this).toggleClass("active");
			});
		},
		'manageSlider' : function() {
			    if($('.fotorama').length){
			    	if($(window).width()>992){
					 $('.fotorama').fotorama({
						  maxwidth: '100%',
						  width: '100%',
						  height:'90%',
						  maxheight: '100%',
						  fit:'cover',
						  allowfullscreen: false,
						  transition : 'fade',
						  nav:false,
						  loop: true,
						  autoplay:true,
						  arrows:true
					  });
					}else{
						 $('.fotorama').fotorama({
							  maxwidth: '100%',
							  width: '100%',
							  height:'70%',
							  maxheight: '100%',
							  fit:'cover',
							  allowfullscreen: false,
							  transition : 'fade',
							  nav:false,
							  loop: true,
							  autoplay:true,
							  arrows:true
						  });
					}
				  }
				  if($('.bxslider').length){
					var slider = $('.bxslider').bxSlider({
						captions: true,
						auto: ($('.bxslider').children().length > 1) ? true : false,infiniteLoop :  true,
						autoControls: false,
						preventDefaultSwipeX: false,
						hideControlOnEnd: false,
						touchEnabled: true,
	                    preventDefaultSwipeY:false,
	                    swipeThreshold:1,
	                    useCSS: false,
	                    easing: 'swing',
	                    responsive:true,
	                    speed:1250,
	                    preloadImages:'all',
	                    swipeRight: function(event, direction, distance, duration, fingerCount) {
							slider.goToPrevSlide();
						},
						swipeLeft: function(event, direction, distance, duration, fingerCount) {
							slider.goToNextSlide();
						},
						pager:false,
					});
				  }
				if($(".lightslider > li").length>1){
				    $('.lightslider').lightSlider({
				    	item:1,
				        loop:true,
				        slideMove:1,
				    	autoWidth:true,
				        adaptiveHeight: true,
				        pager:true
				    });
				}
		},
		'manageGalleries' : function() {
			 if ($('.fancybox').length){
					$('a.fancybox[rel=group]').fancybox({
		        		'transitionIn'		: 'elastic',
		        		'transitionOut'		: 'fade',
		        		'titlePosition' 	: 'over',
		        		'padding' : 0,
		        		'overlayColor' : '#000000',
		        		'overlayOpacity' : '0.6',
		        		'autoScale'		: true
					});
			 }
			 if ($('.light-gallery').length){
				 $('.light-gallery').lightGallery({
					    mode: 'lg-fade',
					    cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
					    thumbnail:true,
					    animateThumb: false,
					    showThumbByDefault: false
					});
			 }
         if ($('.masonry-gallery').length){
	          	var $container = $('.masonry-gallery');
				// initialize Masonry after all images have loaded
				$container.imagesLoaded( function() {
					  $container.masonry({
		               itemSelector: '.gallery-item',
		               columnWidth:1,
		               gutter:1,
		               fitWidth: true
		             });
				});
         }
         if ($('.carousel-gallery').length){
				  $(".carousel-gallery").owlCarousel({
					  	items:6,
					    margin:5,
					    loop:true,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });
         }
			$('a.gallery-thumb').fancybox({
     		'transitionIn'		: 'elastic',
     		'transitionOut'		: 'fade',
     		'titlePosition' 	: 'over',
     		'padding' : 0,
     		'overlayColor' : '#000000',
     		'overlayOpacity' : '0.6',
     		'autoScale'		: true
			});
         if ($('.teaser-gallery').length){
				$(".teaser-gallery .teaser-item").on("click", function(e){
					e.preventDefault();
					$(this).find('a')[0].click();
				});
				  $(".teaser-gallery").owlCarousel({
					  	items:6,
					    margin:2,
					    loop:true,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });

         }
		},
		'manageBookOnline' : function() {
			$("a.book-mobile").on("click", function(e){
				 e.preventDefault();
				$(".book-online-form").toggle();
				//$("header .menu").css( "display", "block");
				$(this).toggleClass("active");
			});
            $('#datepicker1').datepicker({
                format: 'mm dd yyyy',
                minDate: 0
            });
            $('#datepicker2').datepicker({
                format: 'mm dd yyyy',
                minDate: 0
            });
		},
		'manageScroll' : function() {
			//Check to see if the window is top if not then display button
			$(window).scroll(function(){
				if($(window).width()>1200){
					if ($(this).scrollTop() > 100) {
						$('.scrollToTop').fadeIn();
					} else {
						$('.scrollToTop').fadeOut();
					}
			     }
			});
			//Click event to scroll to top
			$('.scrollToTop').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : 0},800);
				return false;
			});
			//Click event to scroll to top
			$('.scroll-down').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : $(".scroll-section").offset().top-130},800);
				return false;
			});
		},
		'manageGooglemap' : function() {
			if ( $( "#map-canvas" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	 var infowindow = new google.maps.InfoWindow({
                		    content: "<strong>Myconian Spirit Villas</strong> <br>Houlakia, Mykonos - Greece"
                	 });
               	 	window.directionsService =new google.maps.DirectionsService;
               	 	window.directionsDisplay = new google.maps.DirectionsRenderer;
					var mapOptions = {
						zoom: 13,
						center: new google.maps.LatLng(37.483291, 25.314950),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false
					};
					var map = new google.maps.Map(document.getElementById('map-canvas'),
					  mapOptions);
					window.map = map;
					window.directionsDisplay.setMap(map);
					var customIcon = 'http://www.mykonianspirit.com/views/themes/mykonian-spirit/assets/img/icons/pin.png';
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(37.482823, 25.314703),
						icon: customIcon,
						map: map
					});
	          		marker.addListener('click', function() {
	          		   infowindow.open(map, marker);
	          		});
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        var styledMapType = new google.maps.StyledMapType(
			          [
			            {
			              stylers: [
			                { hue: '#00ffe6' },
			                { saturation: -20 }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'geometry',
			              stylers: [
			                { lightness: 100 },
			                { visibility: 'simplified' }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'labels',
			              stylers: [
			                { visibility: 'off' }
			              ]
			            }
			          ],
			          {name: 'Styled Map'});
					var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					map.mapTypes.set("Map Style", mapType);
					map.setMapTypeId("Map Style");
					};

                window.onload = function() {setupMap()};

    			$(".map-tabs li a").on("click", function(){
    				var latlng = $(this).data("value").split(",");
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(37.452945, 25.348367),new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])));

    			});
    		      function calculateAndDisplayRoute(directionsService, directionsDisplay,start,end) {
    		          directionsService.route({
    		            origin: start,
    		            destination: end,
    		            travelMode: 'DRIVING'
    		          }, function(response, status) {
    		            if (status === 'OK') {
    		              directionsDisplay.setDirections(response);
    		            } else {
    		              window.alert('Directions request failed due to ' + status);
    		            }
    		          });
    		        }
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}
		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
				if ( !$('#dropdown').hasClass('active') ) {
					 if($(window).width()>1200){
						$("header .menu").css( "display", "block");
				    }else{
						$("header .menu").css( "display", "none");
				     }
				}
			}, 10);
		}
	};
	$(document).on('ready', app.init);
})(jQuery);
