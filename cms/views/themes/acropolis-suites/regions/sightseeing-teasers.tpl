<?php if (isset($region['blocks'])): ?>
	<?php foreach($region['blocks'] as $key => $block): ?>
		<div class="<?=$block['contents'][0]?>">
			<div class="content-width-inner">
				<div class="image <?php if($key % 2 == 0){echo 'left';}else{echo 'right';}?>"><img src="<?=ROOT.'/'.MEDIA_PATH.'/'.$block['contents'][1][0]->media_path?>" /></div>
				<div class="row">
					<div class="col-md-12 border1">
						<div class="row">
							<div class="col-md-5 special-text1 <?php if($key % 2 == 0){echo 'right';}else{echo 'left';}?>">
								<div class="wrapper">
									<h3><?=$block['contents'][1][0]->title?></h3>
									<p><?=$block['contents'][1][0]->content?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>
	<?php endforeach ?>
<?php endif; ?>











