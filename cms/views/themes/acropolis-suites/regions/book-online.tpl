<div class="book-online">
	<form class="book-online-form content-width-inner form-inline" id="booking_form" name="booking_form" method="post" action="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>" target="new">
		<div class="form-group date">
	      <input type="text" placeholder="<?php echo $region['translations']['Book Online form label 1'];?>" class="calender" id="datepicker1">
	    </div>
		<div class="form-group date">
	      <input type="text" placeholder="<?php echo $region['translations']['Book Online form label 2'];?>" class="calender" id="datepicker2">
	    </div>
		<div class="form-group number">
			<input type="number" min="1" max="7" step="1" placeholder="<?php echo $region['translations']['Book Online form label 4'];?>" name="adults" class="form-select" id="adults">
		</div>
		<div class="form-group number">
			<input type="number" min="0" max="6" step="1" placeholder="<?php echo $region['translations']['Book Online form label 5'];?>" name="children" class="form-select last" id="children">
		</div>
	   	<div class="form-group number">
			<input type="number" min="0" max="1" step="1" placeholder="<?php echo $region['translations']['Book Online form label 3'];?>" name="infants" class="form-select" id="infants">
		</div>
		<input type="submit" value="<?php echo $region['translations']['Book Online button label'];?>" class="" onclick="if (ValidateDates11a('<?php echo $language ?>')){document.getElementById('booking_form').submit();}">
	</form>
	<div class="online-button"><a href="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>" target="new" title="overlay" style=""></a></div>
	<a id="book-online-resp" target="new" href="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>"><?php echo $region['translations']['Book Online button label'];?></a>
</div>
<!-- <div class="language-selector-buttons">
	<ul>
		<li><a href="<?=ROOT?>/en/" class="<?php if ($language=='en'){ echo 'active';} ?>">EN</a></li>
		<li class="last"><a href="<?=ROOT?>/el/" class="<?php if ($language=='el'){ echo 'active';} ?>">GR</a></li>
	</ul>
</div> -->