<div class="row  services-section top-bottom-padding background-color9">
	<div class="content-width-inner row main-content">
		<div class="col-md-3">
			<h2><?php echo $region['translations']['Services Title']; ?></h2>
			<p>
			<?php echo $region['translations']['Services Content']; ?>
			</p>
			<div class="button2">
				<a href="services"><?php echo $region['translations']['Services Read More']; ?></a>
			</div>
		</div>
		<div class="col-md-9 boxes">
			<div class="col-md-2 box">
				<p><?php echo $region['translations']['Services 1']; ?></p>
			</div>
			<div class="col-md-2 box">
				<p><?php echo $region['translations']['Services 2']; ?></p>
			</div>
			<div class="col-md-2 box">
				<p><?php echo $region['translations']['Services 3']; ?></p>
			</div>
			<div class="col-md-2 box last">
				<p><?php echo $region['translations']['Services 4']; ?></p>
			</div>
			<div class="col-md-2 box last">
				<p><?php echo $region['translations']['Services 5']; ?></p>
			</div>
		</div>
	</div>
</div>