<header>
	<div class="content-width-inner">
		<div class="row">
			<div class="col-md-2 col-xs-2 col-sm-2 logo">
				<a href="<?=ROOT?>/<?=$GLOBALS['default_locale']?>/"></a>
			</div>
			<div class="col-md-10 navigation content-padding">
				<div class="nav-bar1">
					<div id="dropdown"><span class="glyphicon glyphicon-menu-hamburger fa-2x" aria-hidden="true"></span></div>
					<nav class="menu">
						<?php if (isset($region['menus'])): ?>
							<?php foreach($region['menus'] as $menu): ?>
								<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>
							<?php endforeach ?>
						<?php endif; ?>
					</nav>
				</div>
			</div>
			<!-- <div class="col-md-1 hidden-xs col-sm-2 book-now">
				<a target="_new" href="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>"><?php echo $region['translations']['Book Online Title'];?></a>
			</div> -->
		</div>
		<?php if (isset($region['blocks'])): ?>
			<div class="row">
				<?php foreach($region['blocks'] as $block): ?>
					<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			</div>
		<?php endif; ?>
	</div>
</header>