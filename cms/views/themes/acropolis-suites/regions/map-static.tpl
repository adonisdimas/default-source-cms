<section class="row">
	<div class="row">
		<div class="content-padding content-width">
			<div class="col-md-12 center-align"><h2><?php echo $region['translations']['Map Title']; ?></h2></div>
		</div>
		<div class="col-md-12">
			<div class="map-wrapper">
				<div class="map-top-shadow"></div>
				<div class="map">
					<div id="map-canvas"></div>
					<div class="location-box">
						<div class="contents">
							<h3 class="sightseeing"><?php echo $region['translations']['Title 1']; ?></h3>
							<ul class="nav nav-tabs map-tabs end">
							  <li><a class="mmap-end active" data-toggle="tab" data-value="37.971600, 23.725878">ACROPOLIS</a></li>
							  <li><a class="mmap-end" data-toggle="tab" data-value="37.968453, 23.728539">ACROPOLIS MUSEUM</a></li>
							  <li><a class="mmap-end" data-toggle="tab" data-value="37.973070, 23.727246">PLAKA</a></li>
							</ul>
							<h3 class="suites"><?php echo $region['translations']['Title 2']; ?></h3>
							<ul class="nav nav-tabs map-tabs start">
							  <li><a class="mmap-start" data-toggle="tab" data-value="37.970536, 23.731851"><span>1</span>ACROPOLIS LUXURY SUITE</a></li>
							  <li><a class="mmap-start active" data-toggle="tab" data-value="37.967818,23.729854"><span>2</span>ACROPOLIS BOUTIQUE SUITE</a></li>
							  <li><a class="mmap-start" data-toggle="tab" data-value="37.964943,23.722741"><span>3</span>ACROPOLIS VIEW LUX SUITE</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-bottom-padding-large"></div>
</section>


