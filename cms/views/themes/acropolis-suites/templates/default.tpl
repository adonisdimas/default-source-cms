<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <?php if (isset($regions['book-online'])): ?>
					<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<!--  Main-content Section -->
	<div class="row scroll-section">
		<div class="row top-bottom-padding-large background-color1">
			<div class="content-width-inner main-content row">
				<h1><?=$page_translation->title;?></h1>
				<div class="row two-column">
					<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>
				</div>
				<?php if (isset($page_contents[1])): ?>
				<div class="row">
					<div id="collapse" class="collapse">
						<?php $this->insert('components/page-content', ['page_content' => $page_contents[1]]); ?>
					</div>
					<div class="button2" data-target="#collapse" data-toggle="collapse"><a href="javascript: void(0)"><?=$translations['Read More Title'];?></a></div>
				</div>
				<?php endif; ?>

			</div>
		</div>
		<div class="row top-bottom-padding-large">
			<div class="content-width-inner main-content row">
				<div class="row right-align"><h2><?=$translations['At A Glance Title'];?></h2></div>
				<div class="row">
					<?php if (isset($galleries['pages'])): ?>
						<?php foreach($galleries['pages'] as $key => $gallery): ?>
				  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>
				 		<?php endforeach ?>
					<?php endif; ?>
				</div>
				<div class="row">
						<div class="row" id="video-lightgallery">
						  <div class=" col-md-6 video gallery-item" data-src="https://www.youtube.com/watch?v=amJjBG_vgwM"
						   data-poster="https://img.youtube.com/vi/amJjBG_vgwM/0.jpg">
						    <a class"" href="">
						      <img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/youtube1.png" />
						    <span></span>
						    </a>

						  </div>
						  <div class=" col-md-6 video gallery-item" data-src="https://www.youtube.com/watch?v=fL3Cy7VJEHU"
						   data-poster="https://img.youtube.com/vi/fL3Cy7VJEHU/0.jpg">
						    <a class"" href="">
						      <img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/youtube2.png" />
    <span></span>
						    </a>
						  </div>
						</div>
						<div class="row two-column">
							<p><?=$translations['At a Glance Text'];?></p>
						</div>
				</div>
			</div>
		</div>
		<?php if (isset($regions['services'])): ?>
			<?php $this->insert('regions/services', ['region' =>$regions['services']]); ?>
		<?php endif; ?>
		<?php if (isset($regions['accommodation-teasers'])): ?>
			<section class="row accommodation-teasers top-bottom-padding-large">
				<div class="row center-align">
					<h2 class="special-title"><?php echo $regions['accommodation-teasers']['translations']['Title']; ?></h2>
					<div class="heading-line ">
					</div>
				</div>
				<?php $this->insert('regions/accommodation-teasers', ['region' =>$regions['accommodation-teasers']]); ?>
			</section>
		<?php endif; ?>
	</div>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
	<?php endif; ?>
</main>
<?php $this->stop() ?>