<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <?php if (isset($regions['book-online'])): ?>
					<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<section class="row services top-bottom-padding-large ">
		<div class="row center-align scroll-section">
			<h1><?=$page_translation->title;?></h1>
			<div class="heading-line "></div>
		</div>
		<?php if (isset($regions['services'])): ?>
			<div class="row  services-section top-bottom-padding background-color9">
				<div class="content-width-inner row main-content">
					<div class="col-md-3">
						<h2><?php echo $regions['services']['translations']['Services Title']; ?></h2>
						<p>
						<?php echo $regions['services']['translations']['Services Content']; ?>
						</p>
					</div>
					<div class="col-md-9 boxes">
						<div class="col-md-2 box">
							<p><?php echo $regions['services']['translations']['Services 1']; ?></p>
						</div>
						<div class="col-md-2 box">
							<p><?php echo $regions['services']['translations']['Services 2']; ?></p>
						</div>
						<div class="col-md-2 box">
							<p><?php echo $regions['services']['translations']['Services 3']; ?></p>
						</div>
						<div class="col-md-2 box last">
							<p><?php echo $regions['services']['translations']['Services 4']; ?></p>
						</div>
						<div class="col-md-2 box last">
							<p><?php echo $regions['services']['translations']['Services 5']; ?></p>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="row main-content top-bottom-padding-large">
			<div class="content-width-inner content-padding service-box row">
				<!-- <span class="edge1"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/edge-1.png" /></span>  -->
				<!-- <span class="edge2"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/edge-2.png" /></span> -->
				<div class="col-md-12">
					<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>
					<?php $this->insert('components/page-content', ['page_content' => $page_contents[1]]); ?>
				</div>
			</div>
		</div>
	</section>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
	<?php endif; ?>
</main>
<?php $this->stop() ?>