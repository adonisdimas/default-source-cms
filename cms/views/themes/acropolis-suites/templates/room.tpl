<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <?php if (isset($regions['book-online'])): ?>
					<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<section class="row scroll-section accommodation top-bottom-padding-large">
		<div class="row center-align">
			<h1><?=$page_translation->title;?></h1>
			<div class="heading-line ">
			</div>
		</div>
		<div class="row center-align top-bottom-padding">
			<div class="col-md-12">
				<div class="content-width-inner content-padding">
					<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>
				</div>
			</div>
		</div>
		<div class="row center-align top-bottom-padding">
			<div class="col-md-12">
				<div class="content-width-inner content-padding">
					<div class="row">
						<div class="col-md-12">
							<?php $this->insert('components/page-content', ['page_content' => $page_contents[1]]); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if (isset($galleries['pages'])): ?>
			<div class="row center-align top-bottom-padding">
				<div class="col-md-12">
					<div class="content-width-inner">
							<?php foreach($galleries['pages'] as $key => $gallery): ?>
					  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>
					 		<?php endforeach ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($page_contents[2])): ?>
			<div class="row center-align top-bottom-padding">
				<div class="col-md-12">
					<div class="content-width-inner content-padding">
						<div class="row">
							<div class="col-md-12">
								<?php $this->insert('components/page-content', ['page_content' => $page_contents[2]]); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
	<?php if (isset($regions['map-static'])): ?>
		<section class="row">
			<div class="row">
				<div class="content-padding content-width">
					<div class="col-md-12 center-align"><h2><?php echo $regions['map-static']['translations']['Map Title']; ?></h2></div>
				</div>
				<div class="col-md-12">
					<div class="map-wrapper">
						<div class="map-top-shadow"></div>
						<div class="map">
							<?php if ($page_translation->page_id == 8): ?>
						  		<div id="map-canvas1"></div>
							<?php elseif ($page_translation->page_id == 9): ?>
						  		<div id="map-canvas2"></div>
							<?php else: ?>
						  		<div id="map-canvas3"></div>
							<?php endif; ?>
							<div class="location-box">
								<div class="contents">
									<h3 class="sightseeing"><?php echo $regions['map-static']['translations']['Title 1']; ?></h3>
									<ul class="nav nav-tabs map-tabs end">
										<?php if ($page_translation->page_id == 8): ?>
											<li><a class="mmap-end active" data-toggle="tab" data-value="37.971600, 23.725878">ACROPOLIS 4' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.968453, 23.728539">ACROPOLIS MUSEUM 8' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.973070, 23.727246">PLAKA 2' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.975865, 23.734787">SYNTAGMA 6' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.977002, 23.719288">THISIO 15' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.978003, 23.724195">MONASTIRAKI 15' WALK</a></li>
										<?php endif; ?>
										<?php if ($page_translation->page_id == 9): ?>
											<li><a class="mmap-end active" data-toggle="tab" data-value="37.971600, 23.725878">ACROPOLIS 2' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.968453, 23.728539">ACROPOLIS MUSEUM 7' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.973070, 23.727246">PLAKA 5' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.970923, 23.724580">HEROD ATTICUS ODEON 3' WALK</a></li>
										<?php endif; ?>
										<?php if ($page_translation->page_id == 10): ?>
											<li><a class="mmap-end active" data-toggle="tab" data-value="37.971600, 23.725878">ACROPOLIS 8' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.968453, 23.728539">ACROPOLIS MUSEUM 8' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.973070, 23.727246">PLAKA 12' WALK</a></li>
											<li><a class="mmap-end" data-toggle="tab" data-value="37.970923, 23.724580">HEROD ATTICUS ODEON 10' WALK</a></li>
										<?php endif; ?>
									</ul>
									<h3 class="suites"><?php echo $regions['map-static']['translations']['Title 2']; ?></h3>
									<ul class="nav nav-tabs map-tabs start">
										<?php if ($page_translation->page_id == 8): ?>
									  		<li><a class="mmap-start" data-toggle="tab" data-value="37.970536, 23.731851"><span>1</span>ACROPOLIS VIEW SUITE</a></li>
										<?php elseif ($page_translation->page_id == 9): ?>
									  		<li><a class="mmap-start active" data-toggle="tab" data-value="37.967818,23.729854"><span>2</span>ACROPOLIS BOUTIQUE SUITE</a></li>
										<?php else: ?>
									  		<li><a class="mmap-start" data-toggle="tab" data-value="37.964943,23.722741"><span>3</span>ACROPOLIS VIEW LUX SUITE</a></li>
										<?php endif; ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="top-bottom-padding-large"></div>
		</section>
	<?php endif; ?>
	<?php if (isset($page_contents[3])): ?>
	<!--  OTHER ROOMS -->
	<section class="row accommodation-links top-bottom-padding">
		<div class="row center-align">
			<h2><?=$translations['Other Rooms Title'];?></h2>
		</div>
		<div class="row center-align top-bottom-padding">
			<div class="col-md-12 boxes">
				<div class="content-width-inner2 wrapper">
					<?=$page_contents[3]->content?>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
</main>
<?php $this->stop() ?>