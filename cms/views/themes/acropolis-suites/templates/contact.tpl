<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <?php if (isset($regions['book-online'])): ?>
					<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<section class="row contact top-bottom-padding-large ">
		<div class="row center-align scroll-section">
			<h1 class=""><?=$page_translation->title;?></h1>
			<div class="heading-line "></div>
		</div>
		<div class="row center-align content-width-inner top-bottom-padding">
			<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>
		</div>
		<div class="row main-content top-bottom-padding-large">
			<div class="content-width-inner content-padding row">
				<div class="col-md-8">
					<?php if (isset($regions['contact-form'])): ?>
						<?php $this->insert('regions/contact-form', ['region' =>$regions['contact-form']]); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-4 info">
					<?php $this->insert('components/page-content', ['page_content' => $page_contents[1]]); ?>
				</div>
			</div>
		</div>
	</section>
	<?php if (isset($regions['map-static'])): ?>
		<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
	<?php endif; ?>
</main>
<?php $this->stop() ?>