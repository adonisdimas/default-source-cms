/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.manageStickyHeader();
			app.manageOffer();
			app.manageAnimations();
			app.manageSlider();
			app.manageGalleries();
			app.manageDropdown();
			app.manageBookOnline();
			app.manageScroll();
			app.manageGooglemap();
			$(window).on('resize', app.onResize);
		},
		'manageStickyHeader' : function() {
			var header = $('header');
		    $(window).scroll(function () {
	    		if ($(this).scrollTop() > 100) {
		            header.addClass("sticky");
	    		}else{
	    			header.removeClass("sticky");
	    		}
		    });
		},
		'manageOffer' : function() {
			if ( $("#offer-modal" ).length ) {
				$('#offer-modal').modal('show');
			}
		},
		'manageAnimations' : function() {
			 if($(window).width()>992){
				setTimeout(function(){
					$('.slider-wrapper .caption').addClass('animated zoomIn');
					$('.slider-wrapper .caption').show();
				}, 1200);
			 }
		},
		'manageDropdown' : function() {
			$("#dropdown").on("click", function(){
				$("header .menu").slideToggle();
				$(this).toggleClass("active");
			});
		},
		'manageSlider' : function() {
			    if($('.fotorama').length){
					 $('.fotorama').fotorama({
						  maxwidth: '100%',
						  width: '100%',
						  height:'auto',
						  maxheight: '100%',
						  fit:'cover',
						  allowfullscreen: false,
						  transition : 'fade',
						  nav:false,
						  loop: true,
						  autoplay:true,
						  arrows:true
					  });
				  }
				  if($('.bxslider').length){
					var slider = $('.bxslider').bxSlider({
						captions: true,
						auto: ($('.bxslider').children().length > 1) ? true : false,infiniteLoop :  true,
						autoControls: false,
						preventDefaultSwipeX: false,
						hideControlOnEnd: false,
						touchEnabled: true,
	                    preventDefaultSwipeY:false,
	                    swipeThreshold:1,
	                    useCSS: false,
	                    easing: 'swing',
	                    responsive:true,
	                    speed:1250,
	                    preloadImages:'all',
	                    swipeRight: function(event, direction, distance, duration, fingerCount) {
							slider.goToPrevSlide();
						},
						swipeLeft: function(event, direction, distance, duration, fingerCount) {
							slider.goToNextSlide();
						},
						pager:false,
					});
				  }
				if($(".lightslider > li").length>1){
				    $('.lightslider').lightSlider({
				    	item:1,
				        loop:true,
				        slideMove:1,
				    	autoWidth:true,
				        adaptiveHeight: true,
				        pager:true
				    });
				}
		},
		'manageGalleries' : function() {
			 if ($('.fancybox').length){
					$('a.fancybox[rel=group]').fancybox({
		        		'transitionIn'		: 'elastic',
		        		'transitionOut'		: 'fade',
		        		'titlePosition' 	: 'over',
		        		'padding' : 0,
		        		'overlayColor' : '#000000',
		        		'overlayOpacity' : '0.6',
		        		'autoScale'		: true
					});
			 }
			 if ($('.light-gallery').length){
				 $('.light-gallery').lightGallery({
					    mode: 'lg-fade',
					    cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
					    thumbnail:true,
					    animateThumb: false,
					    showThumbByDefault: false
					});
			 }

			 if ($('#video-lightgallery').length){
				 $('#video-lightgallery').lightGallery();
			 }
         if ($('.masonry-gallery').length){
         	var $container = $('.masonry-gallery');
			// initialize Masonry after all images have loaded
			$container.imagesLoaded( function() {
				  $container.masonry({
	               itemSelector: '.gallery-item',
	               columnWidth:1,
	               gutter:1,
	               fitWidth: true
	             });
			});
         }
         if ($('.masonry-gallery2').length){

         	var $container = $('.masonry-gallery2');
			// initialize Masonry after all images have loaded
			$container.imagesLoaded( function() {
				  $container.masonry({
	               	itemSelector: '.gallery-item',
	                gutter:0,
	                columnWidth: 165,
	                fitWidth: true
	             });
			});
          }
         if ($('.carousel-gallery').length){
				  $(".carousel-gallery").owlCarousel({
					  	items:6,
					    margin:10,
					    loop:true,
					    dots: false,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });
         }
			$('a.gallery-thumb').fancybox({
     		'transitionIn'		: 'elastic',
     		'transitionOut'		: 'fade',
     		'titlePosition' 	: 'over',
     		'padding' : 0,
     		'overlayColor' : '#000000',
     		'overlayOpacity' : '0.6',
     		'autoScale'		: true
			});
         if ($('.teaser-gallery').length){
				$(".teaser-gallery .teaser-item").on("click", function(e){
					e.preventDefault();
					$(this).find('a')[0].click();
				});
				  $(".teaser-gallery").owlCarousel({
					  	items:6,
					    margin:10,
					    loop:true,
					    dots: false,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });

         }
		},
		'manageBookOnline' : function() {
			$("a.book-mobile").on("click", function(e){
				 e.preventDefault();
				$(".book-online-form").toggle();
				//$("header .menu").css( "display", "block");
				$(this).toggleClass("active");
			});
			var datepicker1 = $("#datepicker1").datepicker({
			    dateFormat: 'dd/mm/yy',
			    changeMonth: true,
			    minDate: new Date(),
			    maxDate: '+2y',
			    onSelect: function(date){
					var parts =date.split('/');
					//please put attention to the month (parts[1]), Javascript counts months from 0:
					// January - 0, February - 1, etc
			        var selectedDate = new Date(parts[2],parts[1]-1,parts[0]);
			        var msecsInADay = 86400000;
			        var endDate = new Date(selectedDate.getTime() + msecsInADay);
			       //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
			        $("#datepicker2").datepicker( "option", "minDate", endDate );
			        $("#datepicker2").datepicker( "option", "maxDate", '+2y' );

			    }
			});
			var datepicker2 = $("#datepicker2").datepicker({
			    dateFormat: 'dd/mm/yy',
			    changeMonth: true
			});
			var currentDate = new Date();
			//datepicker1.datepicker("setDate", currentDate);
			//currentDate.setDate(currentDate.getDate()+2);
			//datepicker2.datepicker("setDate", currentDate);
		},
		'manageScroll' : function() {
			//Check to see if the window is top if not then display button
			$(window).scroll(function(){
				if($(window).width()>1160){
					if ($(this).scrollTop() > 100) {
						$('.scrollToTop').fadeIn();
					} else {
						$('.scrollToTop').fadeOut();
					}
			     }
			});
			//Click event to scroll to top
			$('.scrollToTop').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : 0},800);
				return false;
			});
			//Click event to scroll to top
			$('.scroll-down').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : $(".scroll-section").offset().top-130},800);
				return false;
			});
		},
		'manageGooglemap' : function() {
			if ( $( "#map-canvas" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	var infowindow = new google.maps.InfoWindow({});
                    var locations = [
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin1.png','Acropolis Luxury Suite', 37.970536, 23.731851],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin2.png','Acropolis Boutique Suite', 37.967818,23.729854],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin3.png','Acropolis View Luxury Suite', 37.964943,23.722741],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin4.png','Acropolis', 37.971600, 23.725878],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin5.png','Acropolis Museum', 37.968453, 23.728539],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin6.png','Plaka', 37.973070, 23.727246]
                    ];

               	 	window.directionsService =new google.maps.DirectionsService;
               	 	window.directionsDisplay = new google.maps.DirectionsRenderer;
               	    window.directionsDisplay.setOptions( { suppressMarkers: true } );
					var mapOptions = {
						zoom: 15,
						center: new google.maps.LatLng(37.968453,23.728539),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false
					};
					var map = new google.maps.Map(document.getElementById('map-canvas'),
					  mapOptions);
					window.map = map;
					window.directionsDisplay.setMap(map);
				    for (i = 0; i < locations.length; i++) {
					      marker = new google.maps.Marker({
					        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
					        map: map,
					        icon: locations[i][0]
					      });
					      google.maps.event.addListener(marker, 'click', (function(marker, i) {
					        return function() {
					          infowindow.setContent(
					        		  "<div><strong>" +locations[i][1]+
					        		  "</strong><br/></div>"
					        		  );
					          infowindow.open(map, marker);
					        }
					      })(marker, i));
					}
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        var styledMapType = new google.maps.StyledMapType(
			          [
			            {
			              stylers: [
			                { hue: '#00ffe6' },
			                { saturation: -20 }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'geometry',
			              stylers: [
			                { lightness: 100 },
			                { visibility: 'simplified' }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'labels',
			              stylers: [
			                { visibility: 'off' }
			              ]
			            }
			          ],
			          {name: 'Styled Map'});
					var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					map.mapTypes.set("Map Style", mapType);
					map.setMapTypeId("Map Style");
					};

                window.onload = function() {setupMap()};

    			$(".map-tabs li a.mmap-end").on("click", function(){
    				var latlngend = $(this).data("value").split(",");
    				var latlngstart;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.start').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngstart = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    			$(".map-tabs li a.mmap-start").on("click", function(){
    				var latlngstart = $(this).data("value").split(",");
    				var latlngend;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.end').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngend = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    		      function calculateAndDisplayRoute(directionsService, directionsDisplay,start,end) {
    		          directionsService.route({
    		            origin: start,
    		            destination: end,
    		            travelMode: 'DRIVING'
    		          }, function(response, status) {
    		            if (status === 'OK') {
    		              directionsDisplay.setDirections(response);
    		            } else {
    		              window.alert('Directions request failed due to ' + status);
    		            }
    		          });
    		        }
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}
			if ( $( "#map-canvas1" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	var infowindow = new google.maps.InfoWindow({});
                    var locations = [
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin1.png','Acropolis View Suite', 37.970536, 23.731851],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin4.png','Acropolis', 37.971600, 23.725878],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin5.png','Acropolis Museum', 37.968453, 23.728539],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin6.png','Plaka', 37.973070, 23.727246],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin7.png','Syntagma', 37.975865, 23.734787],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin8.png','Thisio', 37.977002, 23.719288],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin9.png','Monastiraki', 37.978003, 23.724195]
                    ];

               	 	window.directionsService =new google.maps.DirectionsService;
               	 	window.directionsDisplay = new google.maps.DirectionsRenderer;
               	    window.directionsDisplay.setOptions( { suppressMarkers: true } );
					var mapOptions = {
						zoom: 15,
						center: new google.maps.LatLng(37.972600, 23.724878),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false
					};
					var map = new google.maps.Map(document.getElementById('map-canvas1'),
					  mapOptions);
					window.map = map;
					window.directionsDisplay.setMap(map);
				    for (i = 0; i < locations.length; i++) {
					      marker = new google.maps.Marker({
					        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
					        map: map,
					        icon: locations[i][0]
					      });
					      google.maps.event.addListener(marker, 'click', (function(marker, i) {
					        return function() {
					          infowindow.setContent(
					        		  "<div><strong>" +locations[i][1]+
					        		  "</strong><br/></div>"
					        		  );
					          infowindow.open(map, marker);
					        }
					      })(marker, i));
					}
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        var styledMapType = new google.maps.StyledMapType(
			          [
			            {
			              stylers: [
			                { hue: '#00ffe6' },
			                { saturation: -20 }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'geometry',
			              stylers: [
			                { lightness: 100 },
			                { visibility: 'simplified' }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'labels',
			              stylers: [
			                { visibility: 'off' }
			              ]
			            }
			          ],
			          {name: 'Styled Map'});
					var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					map.mapTypes.set("Map Style", mapType);
					map.setMapTypeId("Map Style");
					};

                window.onload = function() {setupMap()};

    			$(".map-tabs li a.mmap-end").on("click", function(){
    				var latlngend = $(this).data("value").split(",");
    				var latlngstart;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.start').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngstart = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    			$(".map-tabs li a.mmap-start").on("click", function(){
    				var latlngstart = $(this).data("value").split(",");
    				var latlngend;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.end').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngend = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    		      function calculateAndDisplayRoute(directionsService, directionsDisplay,start,end) {
    		          directionsService.route({
    		            origin: start,
    		            destination: end,
    		            travelMode: 'DRIVING'
    		          }, function(response, status) {
    		            if (status === 'OK') {
    		              directionsDisplay.setDirections(response);
    		            } else {
    		              window.alert('Directions request failed due to ' + status);
    		            }
    		          });
    		        }
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}
			if ( $( "#map-canvas2" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	var infowindow = new google.maps.InfoWindow({});
                    var locations = [
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin2.png','Acropolis Boutique Suite', 37.967818,23.729854],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin4.png','Acropolis', 37.971600, 23.725878],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin5.png','Acropolis Museum', 37.968453, 23.728539],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin6.png','Plaka', 37.973070, 23.727246],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin4.png','Herod Atticus Odeon', 37.970923, 23.724580]
                    ];

               	 	window.directionsService =new google.maps.DirectionsService;
               	 	window.directionsDisplay = new google.maps.DirectionsRenderer;
               	    window.directionsDisplay.setOptions( { suppressMarkers: true } );
					var mapOptions = {
						zoom: 15,
						center: new google.maps.LatLng(37.969770, 23.725090),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false
					};
					var map = new google.maps.Map(document.getElementById('map-canvas2'),
					  mapOptions);
					window.map = map;
					window.directionsDisplay.setMap(map);
				    for (i = 0; i < locations.length; i++) {
					      marker = new google.maps.Marker({
					        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
					        map: map,
					        icon: locations[i][0]
					      });
					      google.maps.event.addListener(marker, 'click', (function(marker, i) {
					        return function() {
					          infowindow.setContent(
					        		  "<div><strong>" +locations[i][1]+
					        		  "</strong><br/></div>"
					        		  );
					          infowindow.open(map, marker);
					        }
					      })(marker, i));
					}
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        var styledMapType = new google.maps.StyledMapType(
			          [
			            {
			              stylers: [
			                { hue: '#00ffe6' },
			                { saturation: -20 }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'geometry',
			              stylers: [
			                { lightness: 100 },
			                { visibility: 'simplified' }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'labels',
			              stylers: [
			                { visibility: 'off' }
			              ]
			            }
			          ],
			          {name: 'Styled Map'});
					var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					map.mapTypes.set("Map Style", mapType);
					map.setMapTypeId("Map Style");
					};

                window.onload = function() {setupMap()};

    			$(".map-tabs li a.mmap-end").on("click", function(){
    				var latlngend = $(this).data("value").split(",");
    				var latlngstart;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.start').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngstart = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    			$(".map-tabs li a.mmap-start").on("click", function(){
    				var latlngstart = $(this).data("value").split(",");
    				var latlngend;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.end').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngend = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    		      function calculateAndDisplayRoute(directionsService, directionsDisplay,start,end) {
    		          directionsService.route({
    		            origin: start,
    		            destination: end,
    		            travelMode: 'DRIVING'
    		          }, function(response, status) {
    		            if (status === 'OK') {
    		              directionsDisplay.setDirections(response);
    		            } else {
    		              window.alert('Directions request failed due to ' + status);
    		            }
    		          });
    		        }
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}
			if ( $( "#map-canvas3" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	var infowindow = new google.maps.InfoWindow({});
                    var locations = [
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin3.png','Acropolis View Luxury Suite', 37.964943,23.722741],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin4.png','Acropolis', 37.971600, 23.725878],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin5.png','Acropolis Museum', 37.968453, 23.728539],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin6.png','Plaka', 37.973070, 23.727246],
                        ['http://www.acropolisuites.com/views/themes/acropolis-suites/assets/img/icons/pin4.png','Herod Atticus Odeon', 37.970923, 23.724580]
                    ];

               	 	window.directionsService =new google.maps.DirectionsService;
               	 	window.directionsDisplay = new google.maps.DirectionsRenderer;
               	    window.directionsDisplay.setOptions( { suppressMarkers: true } );
					var mapOptions = {
						zoom: 15,
						center: new google.maps.LatLng(37.969770, 23.725090),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false
					};
					var map = new google.maps.Map(document.getElementById('map-canvas3'),
					  mapOptions);
					window.map = map;
					window.directionsDisplay.setMap(map);
				    for (i = 0; i < locations.length; i++) {
					      marker = new google.maps.Marker({
					        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
					        map: map,
					        icon: locations[i][0]
					      });
					      google.maps.event.addListener(marker, 'click', (function(marker, i) {
					        return function() {
					          infowindow.setContent(
					        		  "<div><strong>" +locations[i][1]+
					        		  "</strong><br/></div>"
					        		  );
					          infowindow.open(map, marker);
					        }
					      })(marker, i));
					}
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        var styledMapType = new google.maps.StyledMapType(
			          [
			            {
			              stylers: [
			                { hue: '#00ffe6' },
			                { saturation: -20 }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'geometry',
			              stylers: [
			                { lightness: 100 },
			                { visibility: 'simplified' }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'labels',
			              stylers: [
			                { visibility: 'off' }
			              ]
			            }
			          ],
			          {name: 'Styled Map'});
					var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					map.mapTypes.set("Map Style", mapType);
					map.setMapTypeId("Map Style");
					};

                window.onload = function() {setupMap()};

    			$(".map-tabs li a.mmap-end").on("click", function(){
    				var latlngend = $(this).data("value").split(",");
    				var latlngstart;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.start').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngstart = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    			$(".map-tabs li a.mmap-start").on("click", function(){
    				var latlngstart = $(this).data("value").split(",");
    				var latlngend;
    				//get the selected aria explanded a href data point for start
    				var start = $('.map-tabs.end').find('a[aria-expanded]');
    				if (typeof start.data("value") !== 'undefined') {
    					console.log(start.data("value"));
    					latlngend = start.data("value").split(",");
    				}
    				calculateAndDisplayRoute(window.directionsService, window.directionsDisplay,new google.maps.LatLng(parseFloat(latlngstart[0]), parseFloat(latlngstart[1])),new google.maps.LatLng(parseFloat(latlngend[0]), parseFloat(latlngend[1])));
    			});
    		      function calculateAndDisplayRoute(directionsService, directionsDisplay,start,end) {
    		          directionsService.route({
    		            origin: start,
    		            destination: end,
    		            travelMode: 'DRIVING'
    		          }, function(response, status) {
    		            if (status === 'OK') {
    		              directionsDisplay.setDirections(response);
    		            } else {
    		              window.alert('Directions request failed due to ' + status);
    		            }
    		          });
    		        }
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}


		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
				if ( !$('#dropdown').hasClass('active') ) {
					 if($(window).width()>1160){
						$("header .menu").css( "display", "block");
				    }else{
						$("header .menu").css( "display", "none");
				     }
				}
			}, 10);
		}
	};
	$(document).on('ready', app.init);
})(jQuery);
