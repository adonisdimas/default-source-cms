// JavaScript Document
function search_field_text() {
	if (document.search_form.search_field.value == "Search") {
		document.search_form.search_field.value = "";
	} else if (document.search_form.search_field.value == "") {
		document.search_form.search_field.value = "Search";
	}
}

function showButtons(loc) {
	if (loc == "root") {
		return '<div id="scrollLinks" ><img src="images/arrow_down.png" border="0" onmousedown="Scroll(\'wn\',1);"  onmouseover="Scroll(\'wn\',1);" onmouseup="Scroll(\'wn\');"  onmouseout="Scroll(\'wn\');" /><img src="images/arrow_up.png" border="0"  onmousedown="Scroll(\'wn\',-1,1);" onmouseover="Scroll(\'wn\',-1,1);" onmouseup="Scroll(\'wn\');"  onmouseout="Scroll(\'wn\');" />  </div>';
	} else {
		return '<div id="scrollLinks" ><img src="../images/arrow_down.png" border="0" onmousedown="Scroll(\'wn\',1);"  onmouseover="Scroll(\'wn\',1);" onmouseup="Scroll(\'wn\');"  onmouseout="Scroll(\'wn\');" /><img src="../images/arrow_up.png" border="0"  onmousedown="Scroll(\'wn\',-1,1);" onmouseover="Scroll(\'wn\',-1,1);" onmouseup="Scroll(\'wn\');"  onmouseout="Scroll(\'wn\');" />  </div>';
	}
}
function showButtons2(loc) {
	if (loc == "root") {
		return '<div id="scrollLinks" ><img src="images/button_scroll_up.png" border="0"  onmousedown="Scroll(\'wn2\',-1,1);" onmouseover="Scroll(\'wn2\',-1,1);" onmouseup="Scroll(\'wn2\');"  onmouseout="Scroll(\'wn2\');" /> <img src="images/button_scroll_down.png" border="0" onmousedown="Scroll(\'wn2\',1);"  onmouseover="Scroll(\'wn2\',1);" onmouseup="Scroll(\'wn2\');"  onmouseout="Scroll(\'wn2\');" /> </div>';
	} else {
		return '<div id="scrollLinks" ><img src="../images/button_scroll_up.png" border="0"  onmousedown="Scroll(\'wn2\',-1,1);" onmouseover="Scroll(\'wn2\',-1,1);" onmouseup="Scroll(\'wn2\');"  onmouseout="Scroll(\'wn2\');" /> <img src="../images/button_scroll_down.png" border="0" onmousedown="Scroll(\'wn2\',1);"  onmouseover="Scroll(\'wn2\',1);" onmouseup="Scroll(\'wn2\');"  onmouseout="Scroll(\'wn2\');" /> </div>';
	}
}
function showButtons3(loc) {
	if (loc == "root") {
		return '<div id="scrollLinks" ><img src="images/button_scroll_up.png" border="0"  onmousedown="Scroll(\'wn3\',-1,1);" onmouseover="Scroll(\'wn3\',-1,1);" onmouseup="Scroll(\'wn3\');"  onmouseout="Scroll(\'wn3\');" /> <img src="images/button_scroll_down.png" border="0" onmousedown="Scroll(\'wn3\',1);"  onmouseover="Scroll(\'wn3\',1);" onmouseup="Scroll(\'wn3\');"  onmouseout="Scroll(\'wn3\');" /> </div>';
	} else {
		return '<div id="scrollLinks" ><img src="../images/button_scroll_up.png" border="0"  onmousedown="Scroll(\'wn3\',-1,1);" onmouseover="Scroll(\'wn3\',-1,1);" onmouseup="Scroll(\'wn3\');"  onmouseout="Scroll(\'wn3\');" /> <img src="../images/button_scroll_down.png" border="0" onmousedown="Scroll(\'wn3\',1);"  onmouseover="Scroll(\'wn3\',1);" onmouseup="Scroll(\'wn3\');"  onmouseout="Scroll(\'wn3\');" /> </div>';
	}
}
function showButtons4(loc) {
	if (loc == "root") {
		return '<div id="scrollLinks" ><img src="images/button_scroll_up.png" border="0"  onmousedown="Scroll(\'wn4\',-1,1);" onmouseover="Scroll(\'wn4\',-1,1);" onmouseup="Scroll(\'wn4\');"  onmouseout="Scroll(\'wn4\');" /> <img src="images/button_scroll_down.png" border="0" onmousedown="Scroll(\'wn4\',1);"  onmouseover="Scroll(\'wn4\',1);" onmouseup="Scroll(\'wn4\');"  onmouseout="Scroll(\'wn4\');" /> </div>';
	} else {
		return '<div id="scrollLinks" ><img src="../images/button_scroll_up.png" border="0"  onmousedown="Scroll(\'wn4\',-1,1);" onmouseover="Scroll(\'wn4\',-1,1);" onmouseup="Scroll(\'wn4\');"  onmouseout="Scroll(\'wn4\');" /> <img src="../images/button_scroll_down.png" border="0" onmousedown="Scroll(\'wn4\',1);"  onmouseover="Scroll(\'wn4\',1);" onmouseup="Scroll(\'wn4\');"  onmouseout="Scroll(\'wn4\');" /> </div>';
	}
}

function CreateBookmarkLink() {
	title = "Acropolis Spirit Hotel";
	url = "https://acropolisuites.reserve-online.net";
	if (window.sidebar) {
 		// Mozilla Firefox Bookmark
 		window.sidebar.addPanel(title, url,"");
	} else if( window.external ) {
 		// IE Favorite
 		window.external.AddFavorite( url, title); }
 		else if(window.opera && window.print) {
 		// Opera Hotlist
 		return true; }
 }

 function ValidateDates11a(lang) {
				//arrival_date="" + ;
        d1 = new Date(document.getElementById("datepicker1").value);
        d2 = new Date(document.getElementById("datepicker2").value);
        arrival_date="" + d1.getDate() + "/" + (d1.getMonth()+1) + "/" +  d1.getFullYear();
				departure_date="" + d2.getDate() + "/" + (d2.getMonth()+1) + "/" +  d2.getFullYear();
				myrooms = document.getElementById("rooms").value;
				myadults = document.getElementById("adults").value;
				mykids = document.getElementById("children").value;
        myinfants = document.getElementById("infants").value;
        if(myrooms.length==0){
          myrooms=1
        }
        if(myadults.length==0){
          myadults=2
        }
        if(mykids.length==0){
          mykids=0
        }
        console.log( arrival_date );
        console.log( departure_date );
        console.log( myrooms );
        console.log( myadults );
        console.log( mykids );
        console.log( lang );
				switch(lang) {
				case "en":
					document.getElementById('booking_form').action = unescape("https://acropolisuites.reserve-online.net");
				break;
				case "el":
					document.getElementById('booking_form').action = unescape("https://acropolisuites.reserve-online.net");
				break;

			}

				//var d1 = new Date(arrival_date.substr(3,2) + "/" + arrival_date.substr(0,2) + "/" + arrival_date.substr(6,4));
				//var d2 = new Date(departure_date.substr(3,2) + "/" + departure_date.substr(0,2) + "/" + departure_date.substr(6,4));
				var todayD=new Date();

				var d1_month = d1.getMonth() + 1; if(d1_month<10) d1_month="0" + d1_month;
				var d1_day = d1.getDate(); if(d1_day<10) d1_day="0" + d1_day;
				var d1_year = d1.getFullYear();	var d1_full=d1_year + '' + d1_month + '' + d1_day;

				var d2_month = d2.getMonth() + 1; if(d2_month<10) d2_month="0" + d2_month;
				var d2_day = d2.getDate(); if(d2_day<10) d2_day="0" + d2_day;
				var d2_year = d2.getFullYear();	var d2_full=d2_year + '' + d2_month + '' + d2_day;

				var todayD_month = todayD.getMonth() + 1; if(todayD_month<10) todayD_month="0" + todayD_month;
				var todayD_day = todayD.getDate(); if(todayD_day<10) todayD_day="0" + todayD_day;
				var todayD_year = todayD.getFullYear();	var todayD_full=todayD_year + '' + todayD_month + '' + todayD_day;
			  console.log( d1 );
        console.log( d2 );
			//	alert(d1_full + "," + d2_full + "," + todayD_full + "-" + todayD);
				if(d2_full<=d1_full) {
					if (lang=="el") {
						alert("������ ��� ������������ ��� ����������� �����");
					} else {
						alert("Please confirm that you enter the dates correctly");
					}
					return false;
				}
				if(d2_full<todayD_full) {
					if (lang=="el") {
						alert("������ ��� ������������ ��� ����������� �����");
					} else {
						alert("Please confirm that you enter the dates correctly");
					}
					return false;
				}
				if(d1_full<todayD_full) {
					if (lang=="el") {
						alert("������ ��� ������������ ��� ����������� �����");
					} else {
						alert("Please confirm that you enter the dates correctly");
					}
					return false;
				}

				return true;
			}

function slideSwitch() {
    var $active = $('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');

    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}

/*code for scrollable divs start*/
function hookEvent(element, eventName, callback)
{
  if(typeof(element) == "string")
    element = document.getElementById(element);
  if(element == null)
    return;
  if(element.addEventListener)
  {
    if(eventName == 'mousewheel')
      element.addEventListener('DOMMouseScroll', callback, false);
    element.addEventListener(eventName, callback, false);
  }
  else if(element.attachEvent)
    element.attachEvent("on" + eventName, callback);
}

function unhookEvent(element, eventName, callback)
{
  if(typeof(element) == "string")
    element = document.getElementById(element);
  if(element == null)
    return;
  if(element.removeEventListener)
  {
    if(eventName == 'mousewheel')
      element.removeEventListener('DOMMouseScroll', callback, false);
    element.removeEventListener(eventName, callback, false);
  }
  else if(element.detachEvent)
    element.detachEvent("on" + eventName, callback);
}

function cancelEvent(e)
{
  e = e ? e : window.event;
  if(e.stopPropagation)
    e.stopPropagation();
  if(e.preventDefault)
    e.preventDefault();
  e.cancelBubble = true;
  e.cancel = true;
  e.returnValue = false;
  return false;
}

function Scroll(id,ud,spd){
 var obj=document.getElementById(id);
 clearTimeout(obj.to);
 if (ud){
  obj.scrollTop=obj.scrollTop+ud;
  obj.to=setTimeout(function(){ Scroll(id,ud,spd); },spd||10)
 }
}

function ScrollMouse(id,ud,spd){
 var obj=document.getElementById(id);

 if (ud){
	for(i=0;i<20;i++){
  		obj.scrollTop=obj.scrollTop+ud;
	}
 }
}

function test(e)
{
  e = e ? e : window.event;
  var wheelData = e.detail ? e.detail * -1 : e.wheelDelta / 40;
  if (wheelData < 0) {
	ScrollMouse('wn',1,10);
  } else {
  	ScrollMouse('wn',-1,10);
  }

  return cancelEvent(e);
}
function test2(e)
{
  e = e ? e : window.event;
  var wheelData = e.detail ? e.detail * -1 : e.wheelDelta / 40;
  if (wheelData < 0) {
	ScrollMouse('wn2',1,10);
  } else {
  	ScrollMouse('wn2',-1,10);
  }

  return cancelEvent(e);
}
function test3(e)
{
  e = e ? e : window.event;
  var wheelData = e.detail ? e.detail * -1 : e.wheelDelta / 40;
  if (wheelData < 0) {
	ScrollMouse('wn3',1,10);
  } else {
  	ScrollMouse('wn3',-1,10);
  }

  return cancelEvent(e);
}
function test4(e)
{
  e = e ? e : window.event;
  var wheelData = e.detail ? e.detail * -1 : e.wheelDelta / 40;
  if (wheelData < 0) {
	ScrollMouse('wn4',1,10);
  } else {
  	ScrollMouse('wn4',-1,10);
  }

  return cancelEvent(e);
}
/*code for scrollable divs end*/