<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>	
	<?php if (isset($regions['slider'])): ?>
	<div class="row slider-wrapper">
		<div class="content-width-outer">
		  <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>			
		</div>
	</div>
	<?php endif; ?>
	<div class="row scroll-section">
		<div class="center-align row">
		    <div class="col-md-12 top-bottom-padding">
	    		<span class="decor"><img src="<?=ROOT?>/<?=$theme_assets_path?>/img/icons/decor.png" /></span>
	    		<h1><?=$page_translation->title;?></h1>				    
		    </div>
			<div class="col-md-12 top-bottom-padding">
				<div class="content-width content-padding">
					<?php $this->insert('components/page-contents', ['page_contents' => $page_contents]); ?>						
				</div>
			</div>	
			<?php if (isset($galleries['pages'])): ?>	
				<?php foreach($galleries['pages'] as $key => $gallery): ?>
		  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>						
		 		<?php endforeach ?>	 		
			<?php endif; ?>				
		</div>
	</div>
	<?php if (isset($regions['content'])): ?>
		<div class="row">
			<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['accommodation-teaser'])): ?>
		<div class="row">
			<?php $this->insert('regions/accommodation-teaser', ['region' =>$regions['accommodation-teaser']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['how-to-reach'])): ?>
		<div class="row">
			<?php $this->insert('regions/how-to-reach', ['region' =>$regions['how-to-reach']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['map-static'])): ?>
		<div class="row">
			<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
		</div>	
	<?php endif; ?>	
	<?php if (isset($regions['page-teaser'])): ?>
		<div class="row">
			<?php $this->insert('regions/page-teaser', ['region' =>$regions['page-teaser']]); ?>
		</div>	
	<?php endif; ?>	
	<div class="row top-bottom-padding"></div>
</main>
<?php $this->stop() ?>