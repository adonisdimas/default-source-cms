<div class="col-md-12 center-align">
	<h2 class="center-align">AREA MAP</h2>				    
</div>
<div class="col-md-12 bg-location top-bottom-padding">
	<div class="row content-width">
		<?php if (isset($region['blocks'])): ?>
				<?php foreach($region['blocks'] as $block): ?>
					<?php $this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
		<?php endif; ?>		
	</div>
</div>