  <div class="modal" tabindex="-1" role="dialog" id="offer-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
		<?php foreach($region['blocks'] as $block): ?>
			<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
		<?php endforeach ?>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"></button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->