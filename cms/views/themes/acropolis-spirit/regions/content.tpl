<? if (isset($region['blocks'])): ?>
	<div class="row">
		<?php foreach($region['blocks'] as $block): ?>
			<?php if($block['contents'][1][0]->hide_title!=1): ?>
				<div class="col-md-12 center-align top-bottom-padding-large"><h2 class="left-line right-line"><?=$block['contents'][1][0]->title?></h2></div>
			<?php endif ?>
			<div class="<?=$block['contents'][0]?>">
				<?=$block['contents'][1][0]->content?>
			</div>
		<?php endforeach ?>
	</div>
<? endif; ?>
<?php if (isset($region['menus'])): ?>
	<div class="row">
		<?php foreach($region['menus'] as $menu): ?>
			<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>						
		<?php endforeach ?>
	</div>
<?php endif; ?>