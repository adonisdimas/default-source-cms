<header>
	<div class="row">
		<div class="logo">
			<a href="<?=ROOT?>"></a>
		</div>
		<!-- <ul id="lang-menu">
			<select onchange="location = this.options[this.selectedIndex].value;" class="selectpicker">
			  <option value="<?=ROOT?>/en/">EN</option>
			  <option value="<?=ROOT?>/el/">GR</option>
			</select>
		</ul> -->
		<div id="dropdown"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></div>
		<div id="book-now"><a href="<?php echo $settings['Book Online Link']; ?>" target="new"><?php echo $region['translations']['Book Online label'];?></a></div>
		<div class="navigation-box">
			<div class="close-menu"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><p><?php echo $region['translations']['Close menu label'];?></p></div>
			<nav class="menu">
				<?php if (isset($region['menus'])): ?>
					<?php foreach($region['menus'] as $menu): ?>
						<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>
					<?php endforeach ?>
				<?php endif; ?>
			</nav>
		</div>
	</div>
	<?php if (isset($region['blocks'])): ?>
		<div class="row">
			<?php foreach($region['blocks'] as $block): ?>
				<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
			<?php endforeach ?>
		</div>
	<?php endif; ?>
</header>