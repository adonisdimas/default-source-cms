<!--  Hotel Highlights -->
<div class="content-width-inner">
	<h2 class="right-line-big"><?php echo $region['translations']['Hotel highlights title']; ?></h2>
</div>
<div class="col-md-12 highlights top-bottom-margin">
	<div class="row">
		<div class="content-width content-padding">
			<div class="row highlight-icons">
				<?php foreach($region['blocks'] as $block): ?>
					<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</div>