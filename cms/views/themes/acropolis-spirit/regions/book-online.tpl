<div class="book-online">
	<form class="book-online-form content-width form-inline" id="booking_form" name="booking_form" method="post" action="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>" target="new">
		<div class="form-group date">
	      <label for="datepicker1"><?php echo $region['translations']['Book Online form label 1'];?></label>
	      <input type="text" placeholder="DD MM YYYY" class="calender" id="datepicker1">
	    </div>
		<div class="form-group date">
	      <label for="datepicker2"><?php echo $region['translations']['Book Online form label 2'];?></label>
	      <input type="text" placeholder="DD MM YYYY" class="calender" id="datepicker2">
	    </div>
	   	<div class="form-group select">
	   		<label for="rooms"><?php echo $region['translations']['Book Online form label 3'];?></label>
			<input type="number" min="1" max="3" step="1" value="1" name="rooms" class="form-select" id="rooms">
		</div>
		<div class="form-group select">
	   		<label for="adults"><?php echo $region['translations']['Book Online form label 4'];?></label>
			<input type="number" min="1" max="5" step="1" value="1" name="adults" class="form-select" id="adults">
		</div>
		<div class="form-group select">
	   		<label for="children"><?php echo $region['translations']['Book Online form label 5'];?></label>
			<input type="number" min="0" max="4" step="1" value="0" name="children" class="form-select last" id="children">
		</div>
		<input type="submit" value="CHECK AVAILABILITY" class="" onclick="if (ValidateDates11a('<?php echo $language ?>')){document.getElementById('booking_form').submit();}">
	</form>
	<div class="online-button"><a href="" target="new" title="overlay" style=""></a></div>
	<a id="book-online-resp" target="new" href="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>"><?php echo $region['translations']['Book Online button label'];?></a>
</div>