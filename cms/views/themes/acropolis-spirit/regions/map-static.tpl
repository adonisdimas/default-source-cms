<div class="content-padding content-width">
	<div class="col-md-12 center-align"><h2 class="left-line right-line"><?php echo $region['translations']['Map title']; ?></h2></div>		
</div>
<div class="col-md-12">
	<div class="map-wrapper">
		<div class="map-top-shadow"></div>
		<div class="map">
			<div id="map-canvas"></div>
		</div>
		<div class="map-bottom-shadow"></div>
	</div>
</div>