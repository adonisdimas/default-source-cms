<form class="contact-form" method="post" action='<?=ADMIN_PATH?>/mailer'>
    <div class="form-group row">
	    <div class="col-md-6">
	    	<div class="col-md-12">
	    		<label><?php echo $region['translations']['Contact Form Name']; ?></label>
	    		<input type="text" name="firstname" placeholder="" required="">
	    	</div>
		    <div class="col-md-12">
		    	<label><?php echo $region['translations']['Contact Form Email']; ?></label>
		    	<input type="email" name="email" placeholder="" required="">
		    </div>
	    	<div class="col-md-12">
	    		<label><?php echo $region['translations']['Contact Form Code']; ?></label>
	    		<input type="text" name="code" id="sec_code" placeholder="" class="textfield_2">
	    		<img class="sec_code" src="<?=ROOT.ADMIN_PATH?>/captcha?randstring=<?php echo rand(999,9999); ?>" alt="verification image, type it in the box" name="ver_img" width="40" height="18" vspace="0" align="top" id="ver_img">
 				<?php if(isset( $_GET['send'])&&$_GET['send'] == -1){?>
			    <div style="background-color:#D70000; color:#FFFFFF; height:30px; padding:0px; padding-left:5px;width:185px;margin-left:0px;font-size:10px;font-size: 10px;position:absolute;top: 45px;
			    right: 30px;">
			    <div align="center"><?php echo $translations['Contact Form Failure Message']; ?></div>
			    </div>
			    <?php ;}?>
			    <?php if(isset( $_GET['send'])&&$_GET['send'] == 1){?>
			    <div style="background-color:#83831f; color:#FFFFFF; padding:0px; height:30px; padding-left:5px;width:185px;margin-left:0px;position: absolute;font-size: 10px;position:absolute;top: 45px;
			    right: 30px;">
			    <div><?php echo $translations['Contact Form Success Message']; ?></div>
			    </div>
			    <?php ;}?>		    	

	    	</div>
	    </div>
	    <div class="col-md-6">
	         <label><?php echo $region['translations']['Contact Form Message']; ?></label>
	    	<textarea name="message" rows="10" cols="30" placeholder="" required=""></textarea>
	    </div>
    </div>
    <div class="form-group row">
    	<div class="col-md-12 center-align">
    		<input type="submit" value="<?php echo $region['translations']['Contact Form Submit']; ?>">
    	</div>
    </div>
    <input type="hidden" name="uri" value="/contact" >
    <br>
</form>