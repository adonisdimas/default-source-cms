<footer>
	<div class="row top">
		<div class="content-width-inner content-padding">
			<div class="col-md-4 quick-links-box">
				<h3><?php echo $region['translations']['Footer menu title']; ?></h3>
				<nav class="quick-menu">
					<?php if (isset($region['menus'])): ?>
						<?php foreach($region['menus'] as $menu): ?>
							<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>
						<?php endforeach ?>
					<?php endif; ?>
				</nav>
			</div>
			<?php if (isset($region['blocks'])): ?>
				<?php foreach($region['blocks'] as $block): ?>
						<?php $this->insert('components/block', ['classes' => $block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="row bottom">
		<div class="content-width-inner content-padding">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6 copy">
					<p> &copy; <?php echo date("Y") ?> <?php echo $region['translations']['Copyright']; ?></p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 created">
					<p><?php echo $region['translations']['Created by']; ?> <a target="new" href="#">PROJECT</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>