<div class="col-md-12 center-align"><h2 class="left-line right-line"><?php echo $region['translations']['Rooms and suites title']; ?></h2><p class="highlight-text"><?php echo $region['translations']['Rooms and suites text']; ?></p></div>
<div class="col-md-12 rooms-teasers">
	<div class="content-width content-padding">
		<div class="row">
			<?php foreach($region['blocks'] as $block): ?>
				<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
			<?php endforeach ?>
		</div>
	</div>
</div>