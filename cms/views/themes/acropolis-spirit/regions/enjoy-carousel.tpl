<div class="col-md-12 center-align top-bottom-padding-large"><h2 class="left-line right-line"><?php echo $region['translations']['Enjoy title']; ?></h2></div>
<div class="col-md-12 enjoy-section top-bottom-padding">
	<div class="owl-carousel teasers-carousel content-width-inner">
		<?php foreach($region['blocks'] as $block): ?>
			<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
		<?php endforeach ?>
	</div>
</div>