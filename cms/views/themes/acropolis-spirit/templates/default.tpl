<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <?php if (isset($regions['book-online'])): ?>
					<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<!--  Main-content Section -->
	<div class="row scroll-section home-section">
		<div class="content-width-inner">
			<div class="row top-bottom-padding-large">
				<h1><?=$page_translation->title;?> </h1>
				<span class="line1"></span>
				<div class="main-image" style="background: url(<?=ROOT?>/<?=$theme_assets_path?>/img/home-teaser.png) no-repeat left;background-size:cover;"></div>
				<div class="main-content col-md-12">
					<?php $this->insert('components/page-contents', ['page_contents' => $page_contents]); ?>
				</div>
				<span class="stars2"></span>
				<span class="line2"></span>
			</div>
			<?php if (isset($galleries['pages'])): ?>
				<?php foreach($galleries['pages'] as $key => $gallery): ?>
		  			<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>
		 		<?php endforeach ?>
			<?php endif; ?>
		</div>
	</div>
	<?php if (isset($regions['hotel-highlights'])): ?>
		<div class="row top-bottom-padding">
			<?php $this->insert('regions/hotel-highlights', ['region' =>$regions['hotel-highlights']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['rooms-and-suites'])): ?>
		<div class="row top-bottom-padding">
			<?php $this->insert('regions/rooms-and-suites', ['region' =>$regions['rooms-and-suites']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['content'])): ?>
		<div class="row top-bottom-padding-large">
			<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['enjoy-carousel'])): ?>
		<div class="row top-bottom-padding-large">
			<?php $this->insert('regions/enjoy-carousel', ['region' =>$regions['enjoy-carousel']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['map-static'])): ?>
		<div class="row">
			<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
		</div>
	<?php endif; ?>
</main>
<?php $this->stop() ?>