<?php $this->layout('base', ['page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
<main>
	<?php if (isset($regions['slider'])): ?>
		<div class="row slider-wrapper">
		 	  <?php if (isset($regions['slider'])): ?>
			  	    <?php $this->insert('regions/slider', ['region' =>$regions['slider'],'theme_assets_path'=>$theme_assets_path,'sliders_path'=>$sliders_path,'sliders'=>$sliders['pages']]); ?>
			  <?php endif; ?>
			  <?php if (isset($regions['book-online'])): ?>
					<?php $this->insert('regions/book-online', ['region' =>$regions['book-online'],'settings' => $settings,'theme_assets_path' => $theme_assets_path, 'language'=>$this->e($locale)]); ?>
			  <?php endif; ?>
			  <a href="#" class="scroll-down"><img src="<?=ROOT?>/<?=$theme_assets_path;?>/img/icons/scroll-down.png" /></a>
		</div>
	<?php endif; ?>
	<!--  Section -->
	<div class="row scroll-section accommodation-section">
		<div class="row top-bottom-padding-large">
			<div class="col-md-12 center-align"><h1 class="left-line right-line"><?=$page_translation->title;?> </h1><p class="highlight-text"><?php echo $translations['Accommodation text']; ?></p></div>
			<div class="col-md-12">
				<div class="content-width-inner content-padding">
					<div class="row">
						<?php foreach($page_contents as $page_content): ?>
							<div class="accommodation row top-bottom-padding-large">
									<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
		<?php if (isset($galleries['pages'])): ?>
			<?php foreach($galleries['pages'] as $key => $gallery): ?>
					<?php $this->insert($galleries_path.'/theme/'.$gallery[0]->type, ['galleries_path' =>$galleries_path,'gallery' => $gallery[0],'images' => $gallery[1]['images'],'thumbnails' => $gallery[1]['thumbnails']]); ?>
				<?php endforeach ?>
		<?php endif; ?>
	</div>
	<?php if (isset($regions['hotel-highlights'])): ?>
		<div class="row top-bottom-padding">
			<?php $this->insert('regions/hotel-highlights', ['region' =>$regions['hotel-highlights']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['rooms-and-suites'])): ?>
		<div class="row top-bottom-padding">
			<?php $this->insert('regions/rooms-and-suites', ['region' =>$regions['rooms-and-suites']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['content'])): ?>
		<div class="row top-bottom-padding-large">
			<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['enjoy-carousel'])): ?>
		<div class="row top-bottom-padding-large">
			<?php $this->insert('regions/enjoy-carousel', ['region' =>$regions['enjoy-carousel']]); ?>
		</div>
	<?php endif; ?>
	<?php if (isset($regions['map-static'])): ?>
		<div class="row">
			<?php $this->insert('regions/map-static', ['region' =>$regions['map-static']]); ?>
		</div>
	<?php endif; ?>
</main>
<?php $this->stop() ?>