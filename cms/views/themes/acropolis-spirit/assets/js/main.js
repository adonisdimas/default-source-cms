/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.manageStickyHeader();
			app.manageOffer();
			app.manageAnimations();
			app.manageSlider();
			app.manageGalleries();
			app.manageDropdown();
			app.manageBookOnline();
			app.manageScroll();
			app.manageGooglemap();
			$(window).on('resize', app.onResize);
		},
		'manageStickyHeader' : function() {
			var header = $('header');
		    $(window).scroll(function () {
	    		if ($(this).scrollTop() > 100) {
		            header.addClass("sticky");
	    		}else{
	    			header.removeClass("sticky");
	    		}
		    });
		},
		'manageOffer' : function() {
			if ( $("#offer-modal" ).length ) {
				$('#offer-modal').modal('show');
			}
		},
		'manageAnimations' : function() {
			if ( $(".slider-wrapper .caption" ).length ) {
			 if($(window).width()>992){
				setTimeout(function(){
					$('.slider-wrapper .caption').addClass('animated zoomIn');
					$('.slider-wrapper .caption').show();
				}, 1200);
			 }
			}
		},
		'manageDropdown' : function() {
			$("#dropdown").on("click", function(){
				$(".navigation-box").css( "right", "0");
			});
			$(".navigation-box .close-menu span").on("click", function(){

				$(".navigation-box").css( "right", "-500px");
			});
		},
		'manageSlider' : function() {
			    if($('.fotorama').length){
					 $('.fotorama').fotorama({
						  maxwidth: '100%',
						  width: '100%',
						  height:'auto',
						  maxheight: '100%',
						  fit:'cover',
						  allowfullscreen: false,
						  transition : 'fade',
						  autoplay : '3000',
						  loop: true,
						  nav:false,
						  arrows:true
					  });
				  }
				  if($('.bxslider').length){
					var slider = $('.bxslider').bxSlider({
						captions: true,
						auto: ($('.bxslider').children().length > 1) ? true : false,infiniteLoop :  true,
						autoControls: false,
						preventDefaultSwipeX: false,
						hideControlOnEnd: false,
						touchEnabled: true,
	                    preventDefaultSwipeY:false,
	                    swipeThreshold:1,
	                    useCSS: false,
	                    easing: 'swing',
	                    responsive:true,
	                    speed:1250,
	                    preloadImages:'all',
	                    swipeRight: function(event, direction, distance, duration, fingerCount) {
							slider.goToPrevSlide();
						},
						swipeLeft: function(event, direction, distance, duration, fingerCount) {
							slider.goToNextSlide();
						},
						pager:false,
					});
				  }
				if($(".lightslider > li").length>1){
				    $('.lightslider').lightSlider({
				    	item:1,
				        loop:true,
				        slideMove:1,
				    	autoWidth:true,
				        adaptiveHeight: true,
				        pager:true
				    });
				}
		},
		'manageGalleries' : function() {
			 if ($('.fancybox').length){
					$('a.fancybox[rel=group]').fancybox({
		        		'transitionIn'		: 'elastic',
		        		'transitionOut'		: 'fade',
		        		'titlePosition' 	: 'over',
		        		'padding' : 0,
		        		'overlayColor' : '#000000',
		        		'overlayOpacity' : '0.6',
		        		'autoScale'		: true
					});
			 }
			 if ($('.light-gallery').length){
				 $('.light-gallery').lightGallery({
					    mode: 'lg-fade',
					    cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
					    thumbnail:true,
					    animateThumb: false,
					    showThumbByDefault: false
					});
			 }
         if ($('.masonry-gallery').length){
         	$('.masonry-gallery').masonry({
               itemSelector: '.gallery-item',
               columnWidth:1,
               gutter:5,
               fitWidth: true
             });
         }
         if ($('.teasers-carousel').length){
			  $(".teasers-carousel").owlCarousel({
				  	items:1,
				    margin:0,
				    loop:true,
				    nav: true,
				    pagination : true,
			        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
			  });
         }
         if ($('.carousel-gallery').length){
				  $(".carousel-gallery").owlCarousel({
					  	items:6,
					    margin:0,
					    loop:true,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });
         }
			$('a.gallery-thumb').fancybox({
     		'transitionIn'		: 'elastic',
     		'transitionOut'		: 'fade',
     		'titlePosition' 	: 'over',
     		'padding' : 0,
     		'overlayColor' : '#000000',
     		'overlayOpacity' : '0.6',
     		'autoScale'		: true
			});
         if ($('.teaser-gallery').length){
				$(".teaser-gallery .teaser-item").on("click", function(e){
					e.preventDefault();
					$(this).find('a')[0].click();
				});
				  $(".teaser-gallery").owlCarousel({
					  	items:6,
					    margin:2,
					    loop:true,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:2,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:4,
					            nav:false
					        },
					        999:{
					            items:6,
					            nav:false
					        },
					    }

				  });

         }
		},
		'manageBookOnline' : function() {
			$("a.book-mobile").on("click", function(e){
				 e.preventDefault();
				$(".book-online-form").toggle();
				//$("header .menu").css( "display", "block");
				$(this).toggleClass("active");
			});
			var datepicker1 = $("#datepicker1").datepicker({
			    dateFormat: 'dd-M-yy',
			    changeMonth: true,
			    minDate: new Date(),
			    maxDate: '+2y',
			    onSelect: function(date){

			        var selectedDate = new Date(date);
			        var msecsInADay = 86400000;
			        var endDate = new Date(selectedDate.getTime() + msecsInADay);

			       //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
			        $("#datepicker2").datepicker( "option", "minDate", endDate );
			        $("#datepicker2").datepicker( "option", "maxDate", '+2y' );

			    }
			});
			var datepicker2 = $("#datepicker2").datepicker({
			    dateFormat: 'dd-M-yy',
			    changeMonth: true
			});
			var currentDate = new Date();
			datepicker1.datepicker("setDate", currentDate);
			currentDate.setDate(currentDate.getDate()+2);
			datepicker2.datepicker("setDate", currentDate);
		},
		'manageScroll' : function() {
			//Check to see if the window is top if not then display button
			$(window).scroll(function(){
				if($(window).width()>1200){
					if ($(this).scrollTop() > 100) {
						$('.scrollToTop').fadeIn();
					} else {
						$('.scrollToTop').fadeOut();
					}
			     }
			});
			//Click event to scroll to top
			$('.scrollToTop').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : 0},800);
				return false;
			});
			//Click event to scroll to top
			$('.scroll-down').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : $(".scroll-section").offset().top-130},800);
				return false;
			});
		},
		'manageGooglemap' : function() {
			if ( $( "#map-canvas" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	 var infowindow = new google.maps.InfoWindow({
                		    content: "<strong>Acropolis Spirit</strong> , Greece"
                	 });
					var mapOptions = {
						zoom: 16,
						center: new google.maps.LatLng(37.967875, 23.731217),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false,
	    				styles:[
	    				        {
	    				            "featureType": "water",
	    				            "elementType": "geometry",
	    				            "stylers": [
	    				                {
	    				                    "color": "#e9e9e9"
	    				                },
	    				                {
	    				                    "lightness": 17
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "landscape",
	    				            "elementType": "geometry",
	    				            "stylers": [
	    				                {
	    				                    "color": "#f5f5f5"
	    				                },
	    				                {
	    				                    "lightness": 20
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "road.highway",
	    				            "elementType": "geometry.fill",
	    				            "stylers": [
	    				                {
	    				                    "color": "#ffffff"
	    				                },
	    				                {
	    				                    "lightness": 17
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "road.highway",
	    				            "elementType": "geometry.stroke",
	    				            "stylers": [
	    				                {
	    				                    "color": "#ffffff"
	    				                },
	    				                {
	    				                    "lightness": 29
	    				                },
	    				                {
	    				                    "weight": 0.2
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "road.arterial",
	    				            "elementType": "geometry",
	    				            "stylers": [
	    				                {
	    				                    "color": "#ffffff"
	    				                },
	    				                {
	    				                    "lightness": 18
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "road.local",
	    				            "elementType": "geometry",
	    				            "stylers": [
	    				                {
	    				                    "color": "#ffffff"
	    				                },
	    				                {
	    				                    "lightness": 16
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "poi",
	    				            "elementType": "geometry",
	    				            "stylers": [
	    				                {
	    				                    "color": "#f5f5f5"
	    				                },
	    				                {
	    				                    "lightness": 21
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "poi.park",
	    				            "elementType": "geometry",
	    				            "stylers": [
	    				                {
	    				                    "color": "#dedede"
	    				                },
	    				                {
	    				                    "lightness": 21
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "elementType": "labels.text.stroke",
	    				            "stylers": [
	    				                {
	    				                    "visibility": "on"
	    				                },
	    				                {
	    				                    "color": "#ffffff"
	    				                },
	    				                {
	    				                    "lightness": 16
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "elementType": "labels.text.fill",
	    				            "stylers": [
	    				                {
	    				                    "saturation": 36
	    				                },
	    				                {
	    				                    "color": "#333333"
	    				                },
	    				                {
	    				                    "lightness": 40
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "elementType": "labels.icon",
	    				            "stylers": [
	    				                {
	    				                    "visibility": "off"
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "transit",
	    				            "elementType": "geometry",
	    				            "stylers": [
	    				                {
	    				                    "color": "#f2f2f2"
	    				                },
	    				                {
	    				                    "lightness": 19
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "administrative",
	    				            "elementType": "geometry.fill",
	    				            "stylers": [
	    				                {
	    				                    "color": "#fefefe"
	    				                },
	    				                {
	    				                    "lightness": 20
	    				                }
	    				            ]
	    				        },
	    				        {
	    				            "featureType": "administrative",
	    				            "elementType": "geometry.stroke",
	    				            "stylers": [
	    				                {
	    				                    "color": "#fefefe"
	    				                },
	    				                {
	    				                    "lightness": 17
	    				                },
	    				                {
	    				                    "weight": 1.2
	    				                }
	    				            ]
	    				        }
	    				    ]
					};
					var map = new google.maps.Map(document.getElementById('map-canvas'),
					  mapOptions);
					window.map = map;
					var customIcon = 'https://acropolisspirithotel.com/views/themes/acropolis-spirit/assets/img/icons/pin.png';
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(37.967875, 23.731217),
						icon: customIcon,
						map: map
					});
	          		marker.addListener('click', function() {
	          		   infowindow.open(map, marker);
	          		});
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        //var styledMapType = new google.maps.StyledMapType(
			        	//	[],
			          //{name: 'Styled Map'});
					//var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					//map.mapTypes.set("Map Style", mapType);
					//map.setMapTypeId("Map Style");
					};

                window.onload = function() {setupMap()};
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}
		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
			}, 10);
		}
	};
	$(document).on('ready', app.init);
})(jQuery);
