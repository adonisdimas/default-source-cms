<ul class="<?=$classes?>">
	<?php foreach($menu as $key => $menu_item): ?>
  		<li <?php if(count($menu)==$key+1){echo 'class="last"';}?>>
  			<a href="<?=$menu_item->link?>" <?=$this->uri($menu_item->link, 'class="active"')?>><?=$menu_item->title?></a>
  		</li>
 	<?php endforeach ?>
</ul>