<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname(dirname($vendorDir));

return array(
    'Symfony\\Polyfill\\Php55\\' => array($vendorDir . '/symfony/polyfill-php55'),
    'Symfony\\Polyfill\\Php54\\' => array($vendorDir . '/symfony/polyfill-php54'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Spot\\' => array($vendorDir . '/vlucas/spot2/lib'),
    'SpotTest\\' => array($vendorDir . '/vlucas/spot2/tests'),
    'Sabre\\Event\\' => array($vendorDir . '/sabre/event/lib'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'League\\Route\\' => array($vendorDir . '/league/route/src'),
    'League\\Plates\\' => array($vendorDir . '/league/plates/src'),
    'League\\Glide\\' => array($vendorDir . '/league/glide/src'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'League\\Container\\' => array($vendorDir . '/league/container/src'),
    'Intervention\\Image\\' => array($vendorDir . '/intervention/image/src/Intervention/Image'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib/Doctrine/Common'),
);
