<?php
	use Symfony\Component\Finder\Finder;
	/* get server protocol */
	function get_server_protocol(){
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
			return 'https';
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ||
				!empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
					return 'https';
		}
		return 'http';
	}
	function session_init($username,$userrole){
		$_SESSION['auth_username'] = $username;
		$_SESSION['auth_userrole'] = $userrole;
	}
	/* Check if session username are set */
	function session_check(){
		if($_SESSION['auth_username']){
			return true;
		}else{
			return false;
		}
	}
	/* logout by unsetting the session variable and redirecting to login */
	function logout(){
		session_start();
		session_unset();
		redirect(ADMIN_PATH.'/login');
	}
	/* debugger: print argument and then exit*/
	function debug($data){
		var_dump($data);
		exit;
	}
	/*redirect function*/
	function redirect($url){
		if (!headers_sent()) {
			header('Location: '.ROOT.$url, TRUE, 302);
			exit;
		}
	}
	function set_default_locale($locale){
		$GLOBALS['default_locale'] = $locale;
	}
	function get_locale($uri){
		$parts = explode('/',$uri);
		$lang = $parts[1];
		if(strlen($lang)==2){
			return $lang;
		}else{
			return $GLOBALS['default_locale'];
		}
	}
	function remove_locale($locale,$uri){
		$count =1;
		if(strlen($locale)==2){
			return str_replace($locale.'/', "", $uri, $count);
		}else{
			return $uri;
		}
	}
	function generate_captcha($width,$height,$color,$rand_string){
		$image = imagecreatetruecolor($width, $height);
		imagefill($image, 0, 0, $color);
		for ($c = 0; $c < 40; $c++){
			$x = rand(0,$width-1);
			$y = rand(0,$height-1);
			imagesetpixel($image, $x, $y, $color);
		}
		$x = 5;
		$y = 5;
		imagestring($image, 5, $x, $y, $rand_string, 0x000000);		
		return $image;
	}
	/*File handling functions*/
	function upload_files($ffiles,$dir){
		foreach ( $ffiles as $files ) {
			foreach ( $files as $file ) {
				$fileName = $file->getClientOriginalName();
				$file->move($dir, $fileName)->isValid;
			}
		}
		return true;
	}
	function get_dir_files($dir){
		$directory=dirname(realpath(__file__)).$dir;
		$files = new DirectoryIterator($directory);
		$files=sort_files($files);
		return $files;
	}
	function get_dir_folder_names($dir){
		foreach (scandir($dir) as $item) {
			if($item!=="." && $item!==".." && !(is_file($item))){
				$folders[] = $item;
			}
		}
		return $folders;
	}
	function get_dir_file_names($dir){
		$dir = new DirectoryIterator(realpath($dir));
		foreach ($dir as $fileInfo) {
			if($fileInfo->isFile()) {
				$names[] = basename($fileInfo->getFilename(), ".tpl");
			}
		}
		return $names;
	}
	/*Check if directory is empty*/
	function is_dir_invalid($dir) {
		if (!is_readable($dir)) return TRUE;
		return false;
	}
	/*Check if directory is empty*/
	function is_dir_empty($dir) {
		if (!is_readable($dir)) return TRUE;
		$files = new DirectoryIterator($dir);
		foreach ($files as $file) {
			if ($file->isFile()){
				return FALSE;
			}
		}
		return TRUE;
	}
	function sort_by_order_asc($item1,$item2)
	{
		if ($item1['order'] == $item2['order']) return 0;
		return ($item1['order'] > $item2['order']) ? 1 : -1;
	}
	function sort_by_order_desc($item1,$item2)
	{
		if ($item1['order'] == $item2['order']) return 0;
		return ($item1['order'] < $item2['order']) ? 1 : -1;
	}
	function sort_files($dir){
		$files = array();
		foreach ($dir as $file) {
			if ($file->isFile()){
				$files[] = $file->getFilename();
			}
		}
		sort($files);
		return $files;
	}
	function mkdir_r($dirName, $rights=0777){
	    $dirs = explode('/', $dirName);
	    $dir='';
	    foreach ($dirs as $part) {
	        $dir.=$part.'/';
	        if (!is_dir($dir) && strlen($dir)>0)
	            mkdir($dir, $rights);
	    }
	}
	function delete_dir($dirName) {
		if (! is_dir($dirName)) {
			return false;
		}
		if (substr($dirName, strlen($dirName) - 1, 1) != '/') {
			$dirName .= '/';
		}
		$files = glob($dirName . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				delete_dir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirName);
		return true;
	}
	function paginate_items($items,$pagination){
		$pages = array ();
		$itemsperpage = array ();
		$i = 0;
		foreach ( $items as $item ) {
			if ($i == $pagination) {
				$pages [] = $itemsperpage;
				unset ( $itemsperpage );
				$itemsperpage = array ();
				$i = 0;
			}
			$itemsperpage [] = $item;
			$i = $i + 1;
		}
		$pages [] = $itemsperpage;
		return $pages;
	}
	function db_connect() {
		static $db;
		$cfg = new \Spot\Config();
		if($db === null) {
			$cfg->addConnection('mysql', [
					'dbname' => DB_NAME,
					'user' => DB_USER,
					'password' => DB_PASSWORD,
					'host' => DB_HOST,
					'driver' => DB_DRIVER,
					'charset' => DB_CHARSET
			]);
			$db = new \Spot\Locator($cfg);
		}
		return $db;
	}
	function require_once_all($path){
		$files = glob($path);
		foreach ($files as $file) {
			require_once($file);
		}
	}
	/*Appending website scripts and styles*/
	function append_style($styles,$path){
		if (!in_array($path, $styles)) {
			$styles[]=$path;
		}
		return $styles;
	}
	function get_stylesheets_all($dir){
		$styles;
		$finder = new Finder ();
		$finder->files ()->in ( $dir )->name('*.css');
		foreach ($finder as $file) {
			$styles[]=$file->getRelativePathname();
		}
		return $styles;
	}
	function append_script($scripts,$path){
		if (!in_array($path, $scripts)) {
			$scripts[]=$path;
		}
		return $scripts;
	}
	function get_scripts_all($dir){
		$scripts;
		$finder = new Finder ();
		$finder->files ()->in ( $dir )->name('*.js');
		$main;
		foreach ($finder as $file) {
			if(strcmp($file->getRelativePathname(), "main.js") !== 0 && strcmp($file->getRelativePathname(),"lib\jquery.min.js") !== 0){
				$scripts[]=$file->getRelativePathname();
			}
		}
		return $scripts;
	}
	/*Return an array of all the table names of the classes that are instances of $name*/
	function get_tables($name){
		$entities = get_classes($name);
		foreach ($entities as $entity) {
			$instance = new $entity();
			$tables[] =$instance->get_table();
		}
		return $tables;
	}
	/*Return an array of the classes names that are declared as instances of $name*/
	function get_classes($name){
		$declared_classes = get_declared_classes();
		foreach ($declared_classes as $class) {
			//if class name starts with name and is not a Mapper
			if (0 === strpos($class, $name)&&(strpos($class, 'Mapper') == false)) {
				$classes[] = $class;
			}
		}
		return $classes;
	}
?>