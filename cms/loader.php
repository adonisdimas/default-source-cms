<?php
require_once 'functions.php';
require_once 'configuration.php';
require_once VENDOR_PATH.'/autoload.php';
require_once_all(MODELS_PATH . '/*.php');
require_once_all(CONTROLLERS_PATH . '/*.php');
require_once 'router.php';
$router = new Router();
?>