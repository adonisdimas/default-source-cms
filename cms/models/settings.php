<?php
namespace Entity;
use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Setting extends \Spot\Entity
{
	protected static $table = 'settings';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
			'id'          	=> ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
			'title'       	=> ['type' => 'text', 'required' => true ],
			'value'       	=> ['type' => 'string', 'required' => true]
		];
	}
	public function insert_default($db)
	{
		$mapper =$db->mapper('Entity\Setting');
		$mapper->insert([
				'id' => 1,
				'title' => 'Active Theme',
				'value' => 'default'
		]);
		$mapper->insert([
				'id' => 2,
				'title' => 'Default Language',
				'value' => 'en'
		]);
		$mapper->insert([
				'id' => 3,
				'title' => 'Contact Form Send To',
				'value' => 'info@site.com'
		]);
		$mapper->insert([
				'id' => 4,
				'title' => 'Contact Form Email Subject',
				'value' => 'Email Send from Site'
		]);
		$mapper->insert([
				'id' => 5,
				'title' => 'Book Online Link',
				'value' => 'https://site.reserve-online.net/	'
		]);
		$mapper->insert([
				'id' => 6,
				'title' => 'Google MAP API KEY',
				'value' => 'XXXXXXXX'
		]);
		$mapper->insert([
				'id' => 7,
				'title' => 'Google Analytics ID',
				'value' => 'UA-XXXXXXXX'
		]);
	}
}
class User extends \Spot\Entity
{
	protected static $table = 'users';
	protected static $mapper = 'Entity\Mapper\User';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'            => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'username'      => ['type' => 'string', 'required' => true, 'unique' => true],
				'password'      => ['type' => 'text', 'required' => true],
				'salt'			=> ['type' => 'string', 'required' => true],
				'email'   		=> ['type' => 'string', 'required' => true],
				'role'      	=> ['type' => 'string', 'required' => false, 'unique' => false, 'default' => 'admin'],
				'status'    	=> ['type' => 'integer','default' => 0],
		];
	}
	public function insert_default($db)
	{
		$mapper =$db->mapper('Entity\User');
		$entity = $mapper->insert([
				'id' => 1,
				'username' => 'admin',
				'password' => password_hash('1234', PASSWORD_BCRYPT),
				'salt' => PASSWORD_BCRYPT,
				'email' => 'adonisdim1@yahoo.gr',
				'status' => 1,
				'role' => 'admin'
		]);
	}
}
class Language extends \Spot\Entity
{
	protected static $table = 'languages';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'locale'       => ['type' => 'text', 'required' => true],
				'title'        => ['type' => 'text', 'required' => true]
		];
	}
	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'page_translation_id' => $mapper->hasOne($entity, 'Entity\Page_Translation', 'lang_id'),
				'block_content_id' => $mapper->hasOne($entity, 'Entity\Block_Content', 'lang_id'),
				'menu_item_id' => $mapper->hasOne($entity, 'Entity\Menu_Item', 'lang_id'),
		];
	}
	public function insert_default($db)
	{
		$mapper =$db->mapper('Entity\Language');
		$mapper->insert([
				'id' => 1,
				'locale' => 'en',
				'title' => 'English'
		]);
		$mapper->insert([
				'id' => 2,
				'locale' => 'el',
				'title' => 'Ελληνικά'
		]);
	}
}
class Translation extends \Spot\Entity
{
	protected static $table = 'translations';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'lang_id'      => ['type' => 'integer', 'required' => true],
				'title'        => ['type' => 'text', 'required' => true],
				'region_title' => ['type' => 'string', 'required' => false],
				'value'        => ['type' => 'string', 'required' => true]
				
		];
	}
	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
			'id' => $mapper->belongsTo($entity, 'Entity\Language', 'lang_id')
		];
	}
	public function insert_default($db)
	{
		$mapper =$db->mapper('Entity\Translation');
		$mapper->insert([
				'id' => 1,
				'lang_id'      =>1,
				'title' => 'Created by',
				'region_title'  => 'footer',
				'value' => 'Created by'
		]);
		$mapper->insert([
				'id' => 2,
				'lang_id'      =>2,
				'title' => 'Created by',
				'region_title'  => 'footer',
				'value' => 'Created by'
		]);
		$mapper->insert([
				'id' => 3,
				'lang_id'      =>1,
				'title' => 'Copyright',
				'region_title'  => 'footer',
				'value' => '© 2017 | All rights reserved'
		]);
		$mapper->insert([
				'id' => 4,
				'lang_id'      =>2,
				'title' => 'Copyright',
				'region_title'  => 'footer',
				'value' => '© 2017 | All rights reserved'
		]);
		$mapper->insert([
				'id' => 5,
				'lang_id'      =>1,
				'title' => 'Book Online label',
				'region_title'  => 'book-online',
				'value' => 'CHECK AVAILABILITY'
		]);
		$mapper->insert([
				'id' => 6,
				'lang_id'      =>1,
				'title' => 'Book Online form label 1',
				'region_title'  => 'book-online',
				'value' => 'CHECK IN'
		]);
		$mapper->insert([
				'id' => 7,
				'lang_id'      =>1,
				'title' => 'Book Online form label 2',
				'region_title'  => 'book-online',
				'value' => 'CHECK OUT'
		]);
		$mapper->insert([
				'id' => 8,
				'lang_id'      =>1,
				'title' => 'Book Online form label 3',
				'region_title'  => 'book-online',
				'value' => 'ROOMS'
		]);
		$mapper->insert([
				'id' => 9,
				'lang_id'      =>1,
				'title' => 'Book Online form label 4',
				'region_title'  => 'book-online',
				'value' => 'ADULTS'
		]);
		$mapper->insert([
				'id' => 10,
				'lang_id'      =>1,
				'title' => 'Book Online form label 5',
				'region_title'  => 'book-online',
				'value' => 'CHILDREN'
		]);
		$mapper->insert([
				'id' => 11,
				'lang_id'      =>1,
				'title' => 'Book Online button label',
				'region_title'  => 'book-online',
				'value' => 'CHECK RATES & BOOK ONLINE'
		]);
		$mapper->insert([
				'id' => 12,
				'title' => 'Book Online label',
				'region_title'  => 'book-online',
				'lang_id'      =>2,
				'value' => 'CHECK AVAILABILITY'
		]);
		$mapper->insert([
				'id' => 13,
				'lang_id'      =>2,
				'title' => 'Book Online form label 1',
				'region_title'  => 'book-online',
				'value' => 'Check in'
		]);
		$mapper->insert([
				'id' => 14,
				'lang_id'      =>2,
				'title' => 'Book Online form label 2',
				'region_title'  => 'book-online',
				'value' => 'Check out'
		]);
		$mapper->insert([
				'id' => 15,
				'lang_id'      =>2,
				'title' => 'Book Online form label 3',
				'region_title'  => 'book-online',
				'value' => 'Rooms'
		]);
		$mapper->insert([
				'id' => 16,
				'lang_id'      =>2,
				'title' => 'Book Online form label 4',
				'region_title'  => 'book-online',
				'value' => 'Adults'
		]);
		$mapper->insert([
				'id' => 17,
				'lang_id'      =>2,
				'title' => 'Book Online form label 5',
				'region_title'  => 'book-online',
				'value' => 'Kids'
		]);
		$mapper->insert([
				'id' => 18,
				'lang_id'      =>2,
				'title' => 'Book Online button label',
				'region_title'  => 'book-online',
				'value' => 'CHECK RATES & BOOK ONLINE'
		]);	
		$mapper->insert([
				'id' => 19,
				'lang_id'      =>1,
				'title' => 'Contact Form Name',
				'region_title'  => 'contact-form',
				'value' => 'Your Name'
		]);
		$mapper->insert([
				'id' => 20,
				'lang_id'      =>1,
				'title' => 'Contact Form Email',
				'region_title'  => 'contact-form',
				'value' => 'Your Email'
		]);
		$mapper->insert([
				'id' => 21,
				'lang_id'      =>1,
				'title' => 'Contact Form Message',
				'region_title'  => 'contact-form',
				'value' => 'Your Message'
		]);
		$mapper->insert([
				'id' => 22,
				'lang_id'      =>1,
				'title' => 'Contact Form Code',
				'region_title'  => 'contact-form',
				'value' => 'Insert Code'
		]);
		$mapper->insert([
				'id' => 23,
				'lang_id'      =>1,
				'title' => 'Contact Form Submit',
				'region_title'  => 'contact-form',
				'value' => 'SEND'
		]);
		$mapper->insert([
				'id' => 24,
				'lang_id'      =>1,
				'title' => 'Contact Form Success Message',
				'region_title'  => 'contact-form',
				'value' => 'Your message was sent successfully'
		]);
		$mapper->insert([
				'id' => 25,
				'lang_id'      =>1,
				'title' => 'Contact Form Failure Message',
				'region_title'  => 'contact-form',
				'value' => 'Your message was not sent'
		]);
		$mapper->insert([
				'id' => 26,
				'lang_id'      =>1,
				'title' => 'Contact Form Failure Message Code',
				'region_title'  => 'contact-form',
				'value' => 'Your message was not sent, wrong code'
		]);		
		$mapper->insert([
				'id' => 27,
				'lang_id'      =>2,
				'title' => 'Contact Form Name',
				'region_title'  => 'contact-form',
				'value' => 'Your Name'
		]);
		$mapper->insert([
				'id' => 28,
				'lang_id'      =>2,
				'title' => 'Contact Form Email',
				'region_title'  => 'contact-form',
				'value' => 'Your Email'
		]);
		$mapper->insert([
				'id' => 29,
				'lang_id'      =>2,
				'title' => 'Contact Form Message',
				'region_title'  => 'contact-form',
				'value' => 'Your Message'
		]);
		$mapper->insert([
				'id' => 30,
				'lang_id'      =>2,
				'title' => 'Contact Form Code',
				'region_title'  => 'contact-form',
				'value' => 'Insert Code'
		]);
		$mapper->insert([
				'id' => 31,
				'lang_id'      =>2,
				'title' => 'Contact Form Submit',
				'region_title'  => 'contact-form',
				'value' => 'SEND'
		]);
		$mapper->insert([
				'id' => 32,
				'lang_id'      =>2,
				'title' => 'Contact Form Success Message',
				'region_title'  => 'contact-form',
				'value' => 'Your message was sent successfully'
		]);
		$mapper->insert([
				'id' => 33,
				'lang_id'      =>2,
				'title' => 'Contact Form Failure Message',
				'region_title'  => 'contact-form',
				'value' => 'Your message was not sent'
		]);
		$mapper->insert([
				'id' => 34,
				'lang_id'      =>2,
				'title' => 'Contact Form Failure Message Code',
				'region_title'  => 'contact-form',
				'value' => 'Your message was not sent, wrong code'
		]);		
		$mapper->insert([
				'id' => 35,
				'lang_id'      =>1,
				'title' => 'Offer Close',
				'region_title'  => 'offer',
				'value' => 'Close'
		]);
		$mapper->insert([
				'id' => 36,
				'lang_id'      =>2,
				'title' => 'Offer Close',
				'region_title'  => 'offer',
				'value' => 'Close'
		]);
	}
}
class Region extends \Spot\Entity
{
	protected static $table = 'regions';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'title'        => ['type' => 'string', 'required' => true]
		];
	}
	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'menu_id' => $mapper->hasOne($entity, 'Entity\Menu', 'region_id'),
				'block_id' => $mapper->hasOne($entity, 'Entity\Block', 'region_id'),
				'translation_id' => $mapper->hasOne($entity, 'Entity\Translation', 'region_id')
		];
	}
}
namespace Entity\Mapper;
use Spot\Mapper;

class User extends Mapper
{
	public function all()
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$users = (array)$this->query("SELECT * FROM `users`");
		return $users[chr(0).'*'.chr(0).'results'];
	}
}

?>