<?php
namespace Entity;
use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Gallery extends \Spot\Entity
{
	protected static $table = 'galleries';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'title'        => ['type' => 'string', 'required' => true],
				'type'         => ['type' => 'string', 'required' => true],
				'popup'        => ['type' => 'string', 'required' => true],
				'page_titles'  => ['type' => 'string', 'required' => false]
		];
	}
}
class Slider extends \Spot\Entity
{
	protected static $table = 'sliders';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           	  => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'title'        	  => ['type' => 'string', 'required' => true],
				'type'         	  => ['type' => 'string', 'required' => true],
				'page_titles'     => ['type' => 'string', 'required' => false]
		];
	}
}
class Slider_Item extends \Spot\Entity
{
	protected static $table = 'slider_items';
	protected static $mapper = 'Entity\Mapper\Slider_Item';
	
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'            => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'slider_id'     => ['type' => 'integer', 'required' => true],
				'lang_id'       => ['type' => 'integer', 'required' => true],
				'title'         => ['type' => 'string', 'required' => true],
				'caption'       => ['type' => 'string', 'required' => false],
				'media_path'    => ['type' => 'string', 'required' => false],
				'order'   		=> ['type' => 'integer', 'default' => 0, 'required' => false],
		];
	}

	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'id' => $mapper->belongsTo($entity, 'Entity\Slider', 'slider_id'),
				'id' => $mapper->belongsTo($entity, 'Entity\Language', 'lang_id')
		];
	}
}
namespace Entity\Mapper;
use Spot\Mapper;

class Slider_Item extends Mapper
{
	public function all_translated_slider_items($slider_id,$lang_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$results = (array)$this->query("SELECT * FROM `slider_items` WHERE `slider_id` = ".$slider_id." and `lang_id` = ".$lang_id."");
		return $results[chr(0).'*'.chr(0).'results'];
	}
}
?>