<?php
namespace Entity;
use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Menu extends \Spot\Entity
{
    protected static $table = 'menus';
    public function get_table() {
    	return self::$table;
    }
    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'title'        => ['type' => 'string', 'default' => '','required' => false],
        	'classes'      => ['type' => 'string', 'default' => '','required' => false],
        	'order'        => ['type' => 'integer', 'default' => 0, 'required' => false],
            'region_title'    => ['type' => 'string', 'required' => false]
        ];
    }
    public function insert_default($db)
    {
    	$mapper =$db->mapper('Entity\Menu');
    	$mapper->insert([
    			'title' => 'main-menu',
    			'classes' => 'menu',
    			'order' => 0,
    			'region_title' =>''
    	]);
    }
}
class Menu_Item extends \Spot\Entity
{
	protected static $table = 'menu_items';
	protected static $mapper = 'Entity\Mapper\Menu_Item';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'      => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'menu_id' => ['type' => 'integer', 'required' => true],
				'lang_id' => ['type' => 'integer', 'required' => true],
				'title'   => ['type' => 'string', 'required' => false],
				'link'    => ['type' => 'string', 'required' => false],
				'target'  => ['type' => 'string', 'required' => false],
				'order'   => ['type' => 'integer', 'default' => 0, 'required' => false],
		];
	}

	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'id' => $mapper->belongsTo($entity, 'Entity\Menu', 'menu_id'),
				'id' => $mapper->belongsTo($entity, 'Entity\Language', 'lang_id')
		];
	}

	public function insert_default($db)
	{
		$mapper =$db->mapper('Entity\Menu_Item');		
	}
}
namespace Entity\Mapper;
use Spot\Mapper;

class Menu_Item extends Mapper
{
	public function all_menu_items($menu_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$results = (array)$this->query("SELECT * FROM `menu_items` WHERE `menu_id` = ".$menu_id."");
		return $results[chr(0).'*'.chr(0).'results'];
	}
	public function all_translated_menu_items($menu_id,$lang_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$results = (array)$this->query("SELECT * FROM `menu_items` WHERE `menu_id` = ".$menu_id." and `lang_id` = ".$lang_id."");
		return $results[chr(0).'*'.chr(0).'results'];
	}
}
?>