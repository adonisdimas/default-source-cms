<?php

namespace Admin;

use League\Plates\Engine;
use League\Plates\Extension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use PHPMailer;
use Entity\User;
use Entity\Setting;
use Entity\Translation;
use Entity\Page;
use Entity\Page_Content;
use Entity\Template;
use Entity\Region;
use Entity\Menu;
use Entity\Menu_Item;
use Entity\Language;

class Controller {
	public $templates;
	public $locale;
	public $db;
	public $user;
	public $router;
	function __construct() {
		$this->templates = new Engine ( VIEWS_ADMIN_PATH, 'tpl' );
		$this->templates->loadExtension ( new Extension\URI ( $_SERVER ['REQUEST_URI'] ) );
		$this->locale = get_locale (URI);
		$this->db = db_connect ();
	}
	public function login(Request $request, Response $response) {
		$response->setContent ( $this->templates->render ( 'templates/login', [
				'title' => 'login',
				'locale' => $this->locale
		] ) );
		return $response;
	}
	public function authenticate(Request $request, Response $response) {
		$this->user = $this->db->mapper ( 'Entity\User' )->first ( [
				'username' => $request->request->get ( 'username' )
		] );
		if (password_verify ($request->request->get ( 'password' ), $this->user->password )) {
			if($this->user->status == 1){
				session_init ( $this->user->username, $this->user->role );
				redirect ( ADMIN_PATH.'/dashboard' );
			}else{
				redirect ( ADMIN_PATH.'/login?error' );
			}
		} else {
			redirect ( ADMIN_PATH.'/login?error' );
		}
		return $response;
	}
	public function logout(Request $request, Response $response) {
		logout ();
		redirect ( ADMIN_PATH.'/login' );
		return $response;
	}
	public function dashboard(Request $request, Response $response) {
		if (session_check ()) {
			$users = $this->db->mapper ( 'Entity\User' )->all ();
			$languages = $this->db->mapper ( 'Entity\Language' )->all ();
			$regions = $this->db->mapper ( 'Entity\Region' )->all ();
			$templates = $this->db->mapper ( 'Entity\Template' )->all ();
			$blocks= $this->db->mapper ( 'Entity\Block' )->all ();
			$menus= $this->db->mapper ( 'Entity\Menu' )->all ();
			$pages = $this->db->mapper ( 'Entity\Page' )->all ();
			$response->setContent ( $this->templates->render ( 'templates/dashboard', [
					'title' => 'Dashboard',
					'regions'=>$regions,
					'templates'=>$templates,
					'users'=>$users,
					'languages'=>$languages,
					'pages'=>$pages,
					'blocks'=>$blocks,
					'menus'=>$menus,
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'userrole' => $_SESSION ['auth_userrole'],
					'active' => [
							'dashboard',
							'dashboard'
					]
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function mailer(Request $request, Response $response) {
		if(md5($request->request->get ( 'code' )).'a4xn46$^' == $_COOKIE['Captcha']){
			$setting = $this->db->mapper ( 'Entity\Setting' )->where ( ['title' => 'Contact Form Send To'] )->first ();
			if(!$setting){
				$email_to = 'info@site.com';
			}else{
				$email_to = $setting->value;
			}
			$setting = $this->db->mapper ( 'Entity\Setting' )->where ( ['title' => 'Contact Form Email Subject'] )->first ();
			$mail = new PHPMailer;
			$mail->CharSet = 'UTF-8';
			$mail->setFrom($request->request->get ( 'email' ), $request->request->get ( 'firstname' ));
			$mail->addAddress($email_to);
			$mail->Subject = $setting->value;
			$mail->Body = "From: ".$request->request->get ( 'email' )."\r\n Name:".$request->request->get ( 'firstname' )."\r\n Surname:".$request->request->get ( 'surname' )."\r\n Telephone:".$request->request->get ( 'telephone' )."\r\n Message: ".$request->request->get ( 'message' );
			$mail->ClearReplyTos();
			if(!$request->request->get ( 'upload' )){
				$mail->AddAttachment($request->request->get ( 'upload' ));
			}
    		$mail->addReplyTo($request->request->get ( 'email' ),$request->request->get ( 'firstname' ));
			if (!$mail->send()) {
				redirect ( $request->request->get ('uri').'?send=0&&message=Mail Error: '.$mail->ErrorInfo.'');
			} else {
				redirect ( $request->request->get ('uri').'?send=1&&message=Message sent succesfully!');
			}
		}else{
			redirect ( $request->request->get ('uri').'?send=-1&&message=Wrong Code. Try again');
		}
		return $response;
	}
	public function captcha(Request $request, Response $response) {
		$rand_string = $request->query->get ('randstring');
		$captcha = generate_captcha(45,20,0xdbdbdb,$rand_string);
		setcookie('Captcha',(md5($rand_string).'a4xn46$^'));
		imagejpeg($captcha);
		imagedestroy($captcha);
		$response->setContent ($captcha);
		return $response;
	}
	public function media(Request $request, Response $response) {
		if (session_check ()) {
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ( MEDIA_PATH );
			$files = $finder->sortByType ();
			$pages = paginate_items($files,10);
			$response->setContent ( $this->templates->render ( 'templates/media', [
					'title' => 'All Media',
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'userrole' => $_SESSION ['auth_userrole'],
					'pages' => $pages,
					'active' => [
							'media',
							'media'
					]
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function media_box(Request $request, Response $response) {
		if (session_check ()) {
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ( MEDIA_PATH );
			$files = $finder->sortByType ();
			$pages = paginate_items($files,10);
			$response->setContent ( $this->templates->render ( 'templates/media', [
					'title' => 'All Media',
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'userrole' => $_SESSION ['auth_userrole'],
					'pages' => $pages,
					'box' => 1,
					'active' => [
							'media',
							'media'
					]
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function media_upload(Request $request, Response $response) {
		if (session_check ()) {
			if (isset ( $request->files )) {
				upload_files($request->files, $request->request->get('dir'));
				if($request->request->get('dir') != MEDIA_PATH){
					redirect ( ADMIN_PATH.'/galleries/media/'.$request->request->get('id').'?insert=1' );
				}else{
					redirect ( ADMIN_PATH.$request->request->get('redir').'?insert=1' );
				}
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	public function media_delete(Request $request , Response $response) {
		if (session_check ()) {
			$uri = str_replace(SUBFOLDER, "", URI);
			$parts = explode ( '/', $uri );
			$name = $parts [4] . '.' . $parts [5];
			if (unlink ( MEDIA_PATH . '/' . $name )) {
				redirect ( ADMIN_PATH.'/media?delete=1' );
			} else {
				redirect ( ADMIN_PATH.'/media?delete=0' );
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	public function media_box_delete(Request $request, Response $response) {
		if (session_check ()) {
			$uri = str_replace(SUBFOLDER, "", URI);
			$parts = explode ( '/', $uri );
			$name = $parts [4] . '.' . $parts [5];
			if (unlink ( MEDIA_PATH . '/' . $name )) {
				redirect ( ADMIN_PATH.'/media_box?delete=1' );
			} else {
				redirect ( ADMIN_PATH.'/media_box?delete=0' );
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	public function galleries_media(Request $request, Response $response) {
		if (session_check ()) {
			$mapper = $this->db->mapper ( 'Entity\Gallery' );
			$id = basename ( URI );
			$gallery = $mapper->where ( ['id' => [$id]] )->first ();
			$gallery_directory = GALLERIES_PATH.'/'.$gallery->title;
			$gallery_thumbs_directory = GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR;
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ($gallery_directory);
			$files = $finder->sortByType ();
			$pages1 = paginate_items($files,20);
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ($gallery_thumbs_directory);
			$files = $finder->sortByType ();
			$pages2 = paginate_items($files,20);
			$response->setContent ( $this->templates->render ( 'templates/media-galleries', [
					'title' => 'Gallery Media',
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'userrole' => $_SESSION ['auth_userrole'],
					'pages1' => $pages1,
					'dir1' =>$gallery_directory,
					'pages2' => $pages2,
					'dir2' =>$gallery_thumbs_directory,
					'gallery_title' =>$gallery->title,
					'gallery_id' =>$id,
					'active' => [
							'media',
							'galleries'
					]
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function galleries_media_delete(Request $request , Response $response) {
		if (session_check ()) {
			$uri = str_replace(SUBFOLDER, "", URI);
			$id = basename ( $uri );
			$parts = explode ( '/', $uri );
			$file = $parts [5] . '.' . $parts [6];
			$gallery_title = $parts [7];
			$gallery_id =$parts [8];
			$mode =$parts [9];
			if($mode==0){
				if(unlink ( GALLERIES_PATH.'/'.$gallery_title.'/'.$file )) {
					redirect (  ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=1&mode='.$mode);
				}else {
					redirect (  ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=0&mode='.$mode);
				}
			}else{
				if(unlink ( GALLERIES_PATH.'/'.$gallery_title.GALLERIES_THUMBS_DIR.'/'.$file )) {
					redirect (  ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=1&mode='.$mode);
				} else {
					redirect (   ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=0&mode='.$mode);
				}
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	// Showing all items
	public function all(Request $request, Response $response) {
		if (session_check ()) {
			switch (URI) {
				case SUBFOLDER.ADMIN_PATH."/users" :
					$entity = $this->db->mapper ( 'Entity\User' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/users', [
							'title' => 'Users',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'active' => [
									'settings',
									'users'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/languages" :
					$entity = $this->db->mapper ( 'Entity\Language' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/languages', [
							'title' => 'Languages',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'active' => [
									'settings',
									'languages'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/translations" :
						$entity = $this->db->mapper ( 'Entity\Translation' )->all ();
						$entity = paginate_items($entity,18);
						$regions =  $this->db->mapper ( 'Entity\Region' )->all ();
						$response->setContent ( $this->templates->render ( 'templates/translations', [
								'title' => 'Translations',
								'locale' => $this->locale,
								'username' => $_SESSION ['auth_username'],
								'userrole' => $_SESSION ['auth_userrole'],
								'regions' =>$regions,
								'entities' => $entity,
								'active' => [
										'settings',
										'translations'
								]
						] ) );
						break;
				case SUBFOLDER.ADMIN_PATH."/settings" :
					$entity = $this->db->mapper ( 'Entity\Setting' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/settings', [
							'title' => 'Settings',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'active' => [
									'settings',
									'settings'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/templates" :
					$entity = $this->db->mapper ( 'Entity\Template' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/templates', [
							'title' => 'Templates',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'active' => [
									'pages',
									'templates'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/regions" :
					$entity = $this->db->mapper ( 'Entity\Region' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/regions', [
							'title' => 'Regions',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'active' => [
									'settings',
									'regions'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/blocks" :
					$entity = $this->db->mapper ( 'Entity\Block' )->all ();
					$languages = $this->db->mapper ( 'Entity\Language' )->all ();
					$regions = $this->db->mapper ( 'Entity\Region' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/blocks', [
							'title' => 'Blocks',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'languages' => $languages,
							'regions' => $regions,
							'active' => [
									'blocks',
									'blocks'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/menus" :
					$entity = $this->db->mapper ( 'Entity\Menu' )->all ();
					$regions = $this->db->mapper ( 'Entity\Region' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/menus', [
							'title' => 'Menus',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'regions' => $regions,
							'active' => [
									'menus',
									'menus'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/pages" :
					$entity = $this->db->mapper ( 'Entity\Page' )->all ();
					$templates = $this->db->mapper ( 'Entity\Template' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/pages', [
							'title' => 'Pages',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'templates' => $templates,
							'active' => [
									'pages',
									'pages'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/galleries" :
						$entity = $this->db->mapper ( 'Entity\Gallery' )->all ();
						$response->setContent ( $this->templates->render ( 'templates/galleries', [
								'title' => 'Galleries',
								'locale' => $this->locale,
								'username' => $_SESSION ['auth_username'],
								'userrole' => $_SESSION ['auth_userrole'],
								'entities' => $entity,
								'active' => [
										'media',
										'galleries'
								]
						] ) );
				break;
				case SUBFOLDER.ADMIN_PATH."/sliders" :
					$entity = $this->db->mapper ( 'Entity\Slider' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/sliders', [
							'title' => 'Sliders',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entities' => $entity,
							'active' => [
									'media',
									'sliders'
							]
					] ) );
					break;
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	// Adding new item
	public function add(Request $request, Response $response) {
		if (session_check ()) {
			switch (URI) {
				case SUBFOLDER.ADMIN_PATH."/users/add" :
					$response->setContent ( $this->templates->render ( 'templates/users', [
							'title' => 'Add New User',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'active' => [
									'settings',
									'users/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/languages/add" :
					$response->setContent ( $this->templates->render ( 'templates/languages', [
							'title' => 'Add New Language',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'active' => [
									'settings',
									'languages/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/templates/add" :
					$response->setContent ( $this->templates->render ( 'templates/templates', [
							'title' => 'Add New Page Template',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'active' => [
									'pages',
									'templates/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/regions/add" :
					$response->setContent ( $this->templates->render ( 'templates/regions', [
							'title' => 'Add New Region',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'active' => [
									'settings',
									'regions/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/blocks/add" :
					$languages = $this->db->mapper ( 'Entity\Language' )->all ();
					$regions = $this->db->mapper ( 'Entity\Region' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/blocks', [
							'title' => 'Add New Block',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'languages' => $languages,
							'regions' => $regions,
							'active' => [
									'blocks',
									'blocks/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/menus/add" :
					$languages =  $this->db->mapper ( 'Entity\Language' )->all ();
					$regions = $this->db->mapper ( 'Entity\Region' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/menus', [
							'title' => 'Add New Menu',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'languages' => $languages,
							'regions' => $regions,
							'active' => [
									'menus',
									'menus/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/pages/add" :
					$languages = $this->db->mapper ( 'Entity\Page' )->all ();
					$templates = $this->db->mapper ( 'Entity\Template' )->all ();
					$regions = $this->db->mapper ( 'Entity\Region' )->all ();
					$languages = $this->db->mapper ( 'Entity\Language' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/pages', [
							'title' => 'Add New Page',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'languages' => $languages,
							'templates' => $templates,
							'regions' =>$regions,
							'active' => [
									'pages',
									'pages/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/galleries/add" :
					$pages = $this->db->mapper ( 'Entity\Page')->all ();
					$styles = get_dir_file_names(GALLERIES_PATH.'/theme');
					$popup = array(
							0 => "None",
							1 => "fancybox"
					);
					$response->setContent ( $this->templates->render ( 'templates/galleries', [
					'title' => 'Add New Gallery',
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'userrole' => $_SESSION ['auth_userrole'],
					'styles' => $styles,
					'popup' => $popup,
					'pages' => $pages,
					'active' => [
					'media',
					'galleries/add'
							]
							] ) );
				break;
				case SUBFOLDER.ADMIN_PATH."/sliders/add" :
					$pages = $this->db->mapper ( 'Entity\Page')->all ();
					$languages = $this->db->mapper ( 'Entity\Language' )->all ();
					$styles = get_dir_file_names(SLIDERS_PATH.'/theme');
					$response->setContent ( $this->templates->render ( 'templates/sliders', [
							'title' => 'Add New Slider',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'styles' => $styles,
							'pages' => $pages,
							'languages' => $languages,
							'active' => [
									'media',
									'sliders/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/translations/add" :
					$languages = $this->db->mapper ( 'Entity\Language' )->all ();
					$regions = $this->db->mapper ( 'Entity\Region' )->all ();
					$response->setContent ( $this->templates->render ( 'templates/translations', [
							'title' => 'Add New Translation',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'languages' => $languages,
							'regions' => $regions,
							'active' => [
									'settings',
									'translations/add'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/settings/add" :
					$response->setContent ( $this->templates->render ( 'templates/settings', [
							'title' => 'Add New Setting',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'active' => [
									'settings',
									'settings/add'
							]
					] ) );
					break;
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	// Editing item by id
	public function edit(Request $request, Response $response) {
		if (session_check ()) {
			$id = basename ( URI );
			switch (URI) {
				case SUBFOLDER.ADMIN_PATH."/users/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\User' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$response->setContent ( $this->templates->render ( 'templates/users', [
							'title' => 'Edit User',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'active' => [
									'settings',
									'users/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/languages/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Language' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$response->setContent ( $this->templates->render ( 'templates/languages', [
							'title' => 'Edit Language',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'active' => [
									'settings',
									'languages/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/templates/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Template' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$response->setContent ( $this->templates->render ( 'templates/templates', [
							'title' => 'Edit Template',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'active' => [
									'pages',
									'templates/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/regions/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Region' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$response->setContent ( $this->templates->render ( 'templates/regions', [
							'title' => 'Edit Region',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'active' => [
									'settings',
									'languages/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/blocks/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Block' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$mapper = $this->db->mapper ( 'Entity\Block_Content' );
					$contents = $mapper->all_translated_blocks ( $id );
					$mapper = $this->db->mapper ( 'Entity\Language' );
					$languages = $mapper->all ();
					$mapper = $this->db->mapper ( 'Entity\Region' );
					$regions = $mapper->all ();
					$response->setContent ( $this->templates->render ( 'templates/blocks', [
							'title' => 'Edit Block',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'contents' => $contents,
							'languages' => $languages,
							'regions' => $regions,
							'active' => [
									'blocks',
									'blocks/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/menus/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Menu' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$mapper = $this->db->mapper ( 'Entity\Region' );
					$regions = $mapper->all ();
					$response->setContent ( $this->templates->render ( 'templates/menus', [
							'title' => 'Edit Menu',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'regions' => $regions,
							'active' => [
									'menus',
									'menus/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/menu_items/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Language' );
					$languages = $mapper->all ();
					$mapper = $this->db->mapper ( 'Entity\Menu_Item' );
					$menu_items;
					foreach ( $languages as $key => $language ) {
						$menu_items [$key] = $mapper->all_translated_menu_items ( $id, $language->id );
					}
					$response->setContent ( $this->templates->render ( 'templates/menus', [
							'title' => 'Edit Menu Items',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'menu_items' => $menu_items,
							'languages' => $languages,
							'menu_id' => $id,
							'active' => [
									'menus',
									'menus/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/pages/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Page' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$mapper = $this->db->mapper ( 'Entity\Template' );
					$templates = $mapper->all ();
					$mapper = $this->db->mapper ( 'Entity\Region' );
					$regions = $mapper->all ();
					$response->setContent ( $this->templates->render ( 'templates/pages', [
							'title' => 'Edit Page',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'templates' => $templates,
							'regions' => $regions,
							'active' => [
									'pages',
									'pages/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/pages_translation/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Language' );
					$languages = $mapper->all ();
					$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
					$page_translations;
					foreach ( $languages as $key => $language ) {
						$page_translations [$key] = $mapper->get_page_translation ( $id, $language->id );
					}
					$mapper = $this->db->mapper ( 'Entity\Page_Content' );
					$page_contents;
					foreach ( $languages as $key => $language ) {
						$page_contents [$key] = $mapper->get_page_content ( $id, $language->id );
					}
					$response->setContent ( $this->templates->render ( 'templates/pages', [
							'title' => 'Edit Page Content',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'page_translations' => $page_translations,
							'page_contents' => $page_contents,
							'languages' => $languages,
							'page_id' => $id,
							'active' => [
									'pages',
									'pages/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/galleries/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Gallery' );
					$entity = $mapper->where ( ['id' => [$id]] )->first ();
					$mapper = $this->db->mapper ( 'Entity\Page');
					$pages = $mapper->all ();
					$styles = get_dir_file_names(GALLERIES_PATH.'/theme');
					$popup = array(
							0 => "None",
							1 => "fancybox"
					);
					$response->setContent ( $this->templates->render ( 'templates/galleries', [
							'title' => 'Edit Gallery',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'styles' =>$styles,
							'popup' =>$popup,
							'pages' => $pages,
							'active' => [
									'media',
									'galleries/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/sliders/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Slider' );
					$entity = $mapper->where ( ['id' => [$id]] )->first ();
					$mapper = $this->db->mapper ( 'Entity\Page');
					$pages = $mapper->all ();
					$styles = get_dir_file_names(SLIDERS_PATH.'/theme');
					$response->setContent ( $this->templates->render ( 'templates/sliders', [
							'title' => 'Edit Slider',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'styles' =>$styles,
							'pages' => $pages,
							'active' => [
									'media',
									'sliders/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/slider_items/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Language' );
					$languages = $mapper->all ();
					$mapper = $this->db->mapper ( 'Entity\Slider_Item' );
					$slider_items;
					foreach ( $languages as $key => $language ) {
						$slider_items [$key] = $mapper->all_translated_slider_items ( $id, $language->id );
					}
					$mapper = $this->db->mapper ( 'Entity\Slider' );
					$slider= $mapper->where ( ['id' => [$id]] )->first ();
					$response->setContent ( $this->templates->render ( 'templates/sliders', [
							'title' => 'Edit Slider Items',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'slider_items' => $slider_items,
							'slider' => $slider,
							'languages' => $languages,
							'active' => [
									'sliders',
									'sliders/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/translations/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Language' );
					$languages = $mapper->all ();
					$mapper = $this->db->mapper ( 'Entity\Translation' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$mapper = $this->db->mapper ( 'Entity\Region' );
					$regions = $mapper->all ();
					$response->setContent ( $this->templates->render ( 'templates/translations', [
							'title' => 'Edit Translation Items',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'languages' =>$languages,
							'regions' => $regions,
							'active' => [
									'settings',
									'translations/edit'
							]
					] ) );
					break;
				case SUBFOLDER.ADMIN_PATH."/settings/edit/" . $id :
					$mapper = $this->db->mapper ( 'Entity\Setting' );
					$entity = $mapper->where ( [
							'id' => [
									$id
							]
					] )->first ();
					$mapper = $this->db->mapper ( 'Entity\Language' );
					$languages = $mapper->all ();
					$response->setContent ( $this->templates->render ( 'templates/settings', [
							'title' => 'Edit Setting',
							'locale' => $this->locale,
							'username' => $_SESSION ['auth_username'],
							'userrole' => $_SESSION ['auth_userrole'],
							'entity' => $entity,
							'languages' =>$languages,
							'active' => [
									'settings',
									'settings/edit'
							]
					] ) );
					break;
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	// ALL CRUD OPERATIONS
	public function insert(Request $request, Response $response) {
		$response->setContent ( serialize ( $request->request ) );
		switch (URI) {
			case SUBFOLDER.ADMIN_PATH."/users/insert" :
				$mapper = $this->db->mapper ( 'Entity\User' );
				if (! $mapper->first ( [
						'username' => $request->request->get ( 'username' )
				] )) {
					$entity = $mapper->insert ( [
							'username' => $request->request->get ( 'username' ),
							'password' => password_hash ( $request->request->get ( 'password' ), PASSWORD_BCRYPT ),
							'salt' => PASSWORD_BCRYPT,
							'email' => $request->request->get ( 'email' ),
							'role' => $request->request->get ( 'role' ),
							'status' => $request->request->get ( 'status' ) != NULL ? 1 : 0
					] );
					if ($entity) {
						redirect ( ADMIN_PATH.'/users?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/users/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/users/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/languages/insert" :
				$mapper = $this->db->mapper ( 'Entity\Language' );
				if (! $mapper->first ( [
						'locale' => $request->request->get ( 'locale' )
				] )) {
					$entity = $mapper->insert ( [
							'locale' => $request->request->get ( 'locale' ),
							'title' => $request->request->get ( 'title' )
					] );
					if ($entity) {
						redirect ( ADMIN_PATH.'/languages?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/languages/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/languages/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/templates/insert" :
				$mapper = $this->db->mapper ( 'Entity\Template' );
				if (! $mapper->first ( [
						'title' => $request->request->get ( 'title' )
				] )) {
					$entity = $mapper->insert ( [
							'title' => $request->request->get ( 'title' )
					] );
					if ($entity) {
						redirect ( ADMIN_PATH.'/templates?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/templates/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/templates/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/regions/insert" :
				$mapper = $this->db->mapper ( 'Entity\Region' );
				if (! $mapper->first ( [
						'title' => $request->request->get ( 'title' )
				] )) {
					$entity = $mapper->insert ( [
							'title' => $request->request->get ( 'title' )
					] );
					if ($entity) {
						redirect ( ADMIN_PATH.'/regions?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/regions/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/regions/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/blocks/insert" :
				$mapper = $this->db->mapper ( 'Entity\Block' );
				if (! $mapper->first ( [ 'title' => $request->request->get ('title')])) {
					// insert block
					$id = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'classes' => $request->request->get ( 'classes' ),
							'order' => $request->request->get ( 'order' ),
							'region_title' => $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL
					] );
					if ($id) {
						$mapper = $this->db->mapper ( 'Entity\Block_Content' );
						foreach ( $request->files as $items ) {
							foreach ( $items as $files ) {
								foreach ( $files as $file ) {
									if($file){
										$fileName = $file->getClientOriginalName();
										$file->move(MEDIA_PATH.'/', $fileName)->isValid;
									}
								}
							}
						}
						// insert block content for all languages!
						foreach ( $request->request->get ( 'content' ) as $content ) {
							$entity = $mapper->insert ( [
									'hide_title' => $content ["hide_title"] != NULL ? 1 : 0,
									'title' => $content ["title"],
									'link' => $content ["link"],
									'media_path' => $content ["media_path"],
									'lang_id' => $content ["lang_id"],
									'block_id' => $id,
									'content' => $content ["content"]
							] );
						}
						redirect ( ADMIN_PATH.'/blocks?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/blocks/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/blocks/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/menus/insert" :
				$mapper = $this->db->mapper ( 'Entity\Menu' );
				if (! $mapper->first ( ['title' => $request->request->get ( 'title' ) ] )) {
					$id = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'classes' => $request->request->get ( 'classes' ),
							'order' => $request->request->get ( 'order' ),
							'region_title' => $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL
					] );
					if ($id) {
						$mapper = $this->db->mapper ( 'Entity\Menu_Item' );
						foreach ( $request->request->get ( 'menu_item' ) as $lang_id => $menu_items ) {
							$j = 0;
							for($i = 0; $i < count ( $menu_items ) - 3; $i = $i + 3) {
								$mapper->insert ( [
										'lang_id' => intval ( $lang_id ),
										'menu_id' => $id,
										'title' => $menu_items [$i] ["title"],
										'link' => $menu_items [$i + 1] ["link"],
										'target' => $menu_items [$i + 2] ["target"],
										'order' => $j
								] );
								$j = $j + 1;
							}
						}
						redirect ( ADMIN_PATH.'/menus?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/menus/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/menus/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/menu_items/insert" :
				$mapper = $this->db->mapper ( 'Entity\Menu_Item' );
				if ($mapper->delete ( [
						'menu_id' => [
								$request->request->get ( 'menu_id' )
						]
				] )) {
					foreach ( $request->request->get ( 'menu_item' ) as $lang_id => $menu_items ) {
						$j = 0;
						for($i = 0; $i < count ( $menu_items ) - 3; $i = $i + 3) {
							$mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'menu_id' => $request->request->get ( 'menu_id' ),
									'title' => $menu_items [$i] ["title"],
									'link' => $menu_items [$i + 1] ["link"],
									'target' => $menu_items [$i + 2] ["target"],
									'order' => $j
							] );
							$j = $j + 1;
						}
					}
				redirect ( ADMIN_PATH.'/menus?insert=1' );
				} else {
					redirect ( ADMIN_PATH.'/menus/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/pages/insert" :
				$mapper = $this->db->mapper ( 'Entity\Page' );
				if (! $mapper->first ( ['uri' => $request->request->get ( 'uri' )])) {
					$region_titles='';
					foreach ($request->request->get ('region_titles') as $region_title ){
						$region_titles = $region_titles.$region_title.',';
					}
					$id = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'uri' => $request->request->get ( 'uri' ),
							'template_title' => $request->request->get ( 'template_title' ) != 'None' ? $request->request->get ( 'template_title' ) : NULL,
							'region_titles' => $region_titles
					] );
					if ($id) {
						$page_translation = $request->request->get ( 'page_translation' );
						foreach ( $request->request->get ( 'page_content' ) as $lang_id => $page_content ) {
							$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
							$tid = $mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'page_id' => $id,
									'meta_title' => $page_translation [$lang_id] ["meta_title"],
									'meta_description' => $page_translation [$lang_id] ["meta_description"],
									'meta_keywords' => $page_translation [$lang_id] ["meta_keywords"],
									'title' => $page_translation [$lang_id] ["title"]
							] );
							if ($tid) {
								$mapper = $this->db->mapper ( 'Entity\Page_Content' );
								for($i = 0; $i < count ( $page_content) -1 ; $i = $i + 1) {
									$mapper->insert ( [
											'lang_id' => intval ( $lang_id ),
											'page_translation_id' => $tid,
											'page_id' => $id,
											'content' => $page_content [$i] ["content"] != '' ? $page_content [$i] ["content"] : '<p></p>'
									] );
								}
							} else {
								redirect ( ADMIN_PATH.'/pages/add?insert=-1' );
							}
						}
						redirect ( ADMIN_PATH.'/pages?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/pages/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/pages/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/galleries/insert" :
				$mapper = $this->db->mapper ( 'Entity\Gallery' );
				if (!$mapper->first ( ['title' => $request->request->get ('title')] )) {
					$mapper = $this->db->mapper ( 'Entity\Page' );
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					//Create the folders for the gallery media
					mkdir_r(GALLERIES_PATH.'/'.$request->request->get ('title').GALLERIES_THUMBS_DIR);
					$mapper = $this->db->mapper ( 'Entity\Gallery' );
					$id = $mapper->insert ( [
							'title' => $request->request->get ('title' ),
							'type' => $request->request->get ('type'),
							'popup' => $request->request->get ('popup'),
							'page_titles' => $page_titles
					] );
					if ($id) {
						redirect ( ADMIN_PATH.'/galleries?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/galleries/add?insert=-1' );
					}
				}else {
					redirect ( ADMIN_PATH.'/galleries/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/sliders/insert" :
				$mapper = $this->db->mapper ( 'Entity\Slider' );
				if (! $mapper->first ( ['title' => $request->request->get ( 'title' )] )) {
					$mapper = $this->db->mapper ( 'Entity\Page' );
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					//Create the folders for the sliders media
					mkdir_r(SLIDERS_PATH.'/'.$request->request->get ('title'));
					$mapper = $this->db->mapper ( 'Entity\Slider' );
					$id = $mapper->insert ( [
							'title' => $request->request->get ('title' ),
							'type' => $request->request->get ('type'),
							'page_titles' => $page_titles,
					] );
					if ($id) {
						$mapper = $this->db->mapper ( 'Entity\Slider_Item' );
						if (isset ( $request->files )) {
							//upload all the images in the slider directory
							foreach ( $request->files as $items ) {
								foreach ( $items as $files ) {
									foreach ( $files as $file ) {
										if($file["media_path"]!=NULL){
											$fileName = $file["media_path"]->getClientOriginalName();
											$file["media_path"]->move(SLIDERS_PATH.'/'.$request->request->get ('title'), $fileName)->isValid;
										}
									}
								}
							}
						}
						foreach ( $request->request->get ( 'slider_item' ) as $lang_id => $slider_items ) {
							$j = 0;
							for($i = 0; $i < count ( $slider_items ) - 3; $i = $i + 3) {
								$mapper->insert ( [
										'lang_id' => intval ( $lang_id ),
										'slider_id' => $id,
										'title' => $slider_items [$i] ["title"],
										'caption' => $slider_items [$i + 1] ["caption"],
										'media_path' => $slider_items [$i + 2] ["media_path"],
										'order' => $j
								] );
								$j = $j + 1;
							}
						}
						redirect ( ADMIN_PATH.'/sliders?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/sliders/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/sliders/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/slider_items/insert" :
				$mapper = $this->db->mapper ( 'Entity\Slider_Item' );
				if ($mapper->delete ( ['slider_id' => [$request->request->get ( 'slider_id' )]] )) {
					if (isset ( $request->files )) {
						//upload all the images in the slider directory
						foreach ( $request->files as $items ) {
							foreach ( $items as $files ) {
								foreach ( $files as $file ) {
									if($file["media_path"]!=NULL){
										$fileName = $file["media_path"]->getClientOriginalName();
										$file["media_path"]->move(SLIDERS_PATH.'/'.$request->request->get ('slider_title'), $fileName)->isValid;
									}
								}
							}
						}
					}
					foreach ( $request->request->get ( 'slider_item' ) as $lang_id => $slider_items ) {
						$j = 0;
						for($i = 0; $i < count ( $slider_items ) - 3; $i = $i + 3) {
							$mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'slider_id' => $request->request->get ( 'slider_id' ),
									'title' => $slider_items [$i] ['title'],
									'caption' => $slider_items [$i + 1] ['caption'],
									'media_path' => $slider_items [$i + 2] ['media_path'],
									'order' => $j
							] );
							$j = $j + 1;
						}
					}
					redirect ( ADMIN_PATH.'/sliders?insert=1' );
				} else {
					redirect ( ADMIN_PATH.'/sliders/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/translations/insert" :
				$mapper = $this->db->mapper ( 'Entity\Translation' );
				$entity = $mapper->insert ( [
						'title' => $request->request->get ( 'title' ),
						'value' => $request->request->get ( 'value' ),
						'lang_id' => $request->request->get ( 'lang_id' ),
						'region_title' => $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL
				] );
				if ($entity){
					redirect ( ADMIN_PATH.'/translations?insert=1' );
				} else {
					redirect ( ADMIN_PATH.'/translations/add?insert=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/settings/insert" :
				$mapper = $this->db->mapper ( 'Entity\Setting' );
				if (! $mapper->first ( ['title' => $request->request->get ( 'title' ) ] )) {
					$entity = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'value' => $request->request->get ( 'value' ),
					] );
					if ($entity){
						redirect ( ADMIN_PATH.'/settings?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/settings/add?insert=-1' );
					}
				}else{
					redirect ( ADMIN_PATH.'/settings/add?insert=0' );
				}
				break;
		}
		return $response;
	}
	public function delete(Request $request, Response $response) {
		$response->setContent ( serialize ( $request->request ) );
		$id = basename ( URI );
		switch (URI) {
			case SUBFOLDER.ADMIN_PATH."/users/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\User' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/users?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/users?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/languages/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Block_Content' );
				$mapper->delete ( [
						'lang_id' => [
								$id
						]
				] );
				$mapper = $this->db->mapper ( 'Entity\Language' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/languages?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/languages?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/templates/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Template' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/templates?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/templates?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/regions/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Region' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/regions?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/regions?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/blocks/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Block_Content' );
				$mapper->delete ( [
						'block_id' => [
								$id
						]
				] );
				$mapper = $this->db->mapper ( 'Entity\Block' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/blocks?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/blocks?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/blocks_content/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Block_Content' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/blocks?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/blocks?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/menus/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Menu_Item' );
				$mapper->delete ( [
						'menu_id' => [
								$id
						]
				] );
				$mapper = $this->db->mapper ( 'Entity\Menu' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/menus?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/menus?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/menu_items/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Menu_Item' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/menus?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/menus?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/pages/delete/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Page_Content' );
				$mapper->delete ( [
						'page_id' => [
								$id
						]
				] );
				$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
				$mapper->delete ( [
						'page_id' => [
								$id
						]
				] );
				$mapper = $this->db->mapper ( 'Entity\Page' );
				if ($mapper->delete ( [
						'id' => [
								$id
						]
				] )) {
					redirect ( ADMIN_PATH.'/pages?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/pages?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/galleries/delete/".$id :
				$mapper = $this->db->mapper ( 'Entity\Gallery' );
				//Remove the folder and the files of the gallery
				$entity = $mapper->first ( ['id' => $id] );
				if (delete_dir(GALLERIES_PATH.'/'.$entity->title) && $mapper->delete (['id' =>[$id]])) {
					redirect ( ADMIN_PATH.'/galleries?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/galleries?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/sliders/delete/".$id :
				$mapper = $this->db->mapper ( 'Entity\Slider' );
				//Remove the folder and the files of the gallery
				$entity = $mapper->first ( ['id' => $id] );
				$mapper = $this->db->mapper ( 'Entity\Slider_Item' );
				$mapper->delete ( ['slider_id' => [$id]] );
				$mapper = $this->db->mapper ( 'Entity\Slider' );
				if (delete_dir(SLIDERS_PATH.'/'.$entity->title) && $mapper->delete (['id' =>[$id]])) {
					redirect ( ADMIN_PATH.'/sliders?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/sliders?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/slider_items/delete/". $id :
				$mapper = $this->db->mapper ( 'Entity\Slider_Item' );
				if ($mapper->delete ( ['id' => [$id]] )) {
					redirect ( ADMIN_PATH.'/sliders?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/sliders?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/translations/delete/". $id :
				$mapper = $this->db->mapper ( 'Entity\Translation' );
				if ($mapper->delete ( ['id' => [$id]] )) {
					redirect ( ADMIN_PATH.'/translations?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/translations?delete=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/settings/delete/". $id :
				$mapper = $this->db->mapper ( 'Entity\Setting' );
				if ($mapper->delete ( ['id' => [$id]] )) {
					redirect ( ADMIN_PATH.'/settings?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/settings?delete=0' );
				}
				break;
		}
		return $response;
	}
	public function update(Request $request, Response $response) {
		$response->setContent ( serialize ( $request->request ) );
		$id = basename ( URI );
		switch (URI) {
			case SUBFOLDER.ADMIN_PATH."/users/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\User' );
				$entity = $mapper->first ( [
						'id' => $id
				] );
				if ($entity) {
					$entity->username = $request->request->get ( 'username' );
					$entity->password = password_hash ( $request->request->get ( 'password' ), PASSWORD_BCRYPT );
					$entity->salt = PASSWORD_BCRYPT;
					$entity->email = $request->request->get ( 'email' );
					$entity->role = $request->request->get ( 'role' );
					$entity->status = $request->request->get ( 'status' ) != NULL ? 1 : 0;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/users?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/users?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/languages/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Language' );
				$entity = $mapper->first ( [
						'id' => $id
				] );
				if ($entity) {
					$entity->locale = $request->request->get ( 'locale' );
					$entity->title = $request->request->get ( 'title' );
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/languages?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/languages?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/templates/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Template' );
				$entity = $mapper->first ( [
						'id' => $id
				] );
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/templates?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/templates?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/regions/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Region' );
				$entity = $mapper->first ( [
						'id' => $id
				] );
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/regions?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/regions?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/blocks/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Block' );
				$entity = $mapper->first ( [
						'id' => $id
				] );
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$entity->classes = $request->request->get ( 'classes' );
					$entity->order = $request->request->get ( 'order' );
					$entity->region_title = $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL;
					$mapper->update ( $entity );
					$mapper = $this->db->mapper ( 'Entity\Block_Content' );
					$contents = $mapper->all_translated_blocks ( $id );
					$counter = 0;
					foreach ( $request->files as $items ) {
						foreach ( $items as $files ) {
							foreach ( $files as $file ) {
								if($file){
									$fileName = $file->getClientOriginalName();
									$file->move(MEDIA_PATH.'/', $fileName)->isValid;
								}
							}
						}
					}
					foreach ( $contents as $key => $content ) {
						$content->title = $request->request->get ( 'content' ) [$key] ['title'];
						$content->link = $request->request->get ( 'content' ) [$key] ['link'];
						$content->hide_title = $request->request->get ( 'content' ) [$key] ['hide_title'] != NULL ? 1 : 0;
						$content->content = $request->request->get ( 'content' ) [$key] ['content'];
						$content->media_path = $request->request->get ( 'content' ) [$key] ['media_path'];
						$mapper->update ( $content );
						$counter = $counter + 1;

					}
					// if languages are added after created content-->insert additional content instead of update
					if ($counter < count ( $request->request->get ( 'content' ) )) {
						$mapper = $this->db->mapper ( 'Entity\Block_Content' );
						foreach ( $request->request->get ( 'content' ) as $key => $content ) {
							if ($key >= $counter) {
								$entity = $mapper->insert ( [
										'hide_title' => $request->request->get ( 'content' ) [$key] ['hide_title'] != NULL ? 1 : 0,
										'title' => $request->request->get ( 'content' ) [$key] ['title'],
										'link' => $request->request->get ( 'content' ) [$key] ['link'],
										'media_path' => $request->request->get ( 'content' ) [$key] ['media_path'],
										'lang_id' => $request->request->get ( 'content' ) [$key] ['lang_id'],
										'block_id' => $id,
										'content' => $request->request->get ( 'content' ) [$key] ['content']
								] );
							}
						}
					}
					// ---------------------------------------------------------------------------
					redirect ( ADMIN_PATH.'/blocks?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/blocks?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/menus/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Menu' );
				$entity = $mapper->first ( [ 'id' => $id ] );
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$entity->classes = $request->request->get ( 'classes' );
					$entity->order = $request->request->get ( 'order' );
					$entity->region_title = $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/menus?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/menus?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/pages/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Page' );
				$entity = $mapper->first ( [
						'id' => $id
				] );
				if ($entity) {
					$region_titles='';
					foreach ($request->request->get ('region_titles') as $region_title ){
						$region_titles = $region_titles.$region_title.',';
					}
					$entity->title = $request->request->get ( 'title' );
					$entity->uri = $request->request->get ( 'uri' );
					$entity->template_title = $request->request->get ( 'template_title' ) != 'None' ? $request->request->get ( 'template_title' ) : NULL;
					$entity->region_titles = $region_titles;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/pages?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/pages?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/pages_translation/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
				$entities = $mapper->where ( [ 'page_id' => $id ] );
				$counter = 0;
				foreach ( $entities as $key => $entity ) {
					$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
					if ($entity) {
						$entity->meta_title = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["meta_title"];
						$entity->meta_description = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["meta_description"];
						$entity->meta_keywords = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["meta_keywords"];
						$entity->title = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["title"];
						$mapper->update ( $entity );
						$mapper = $this->db->mapper ( 'Entity\Page_Content' );
						$mapper->delete ( [
								'page_translation_id' => $entity->id
						] );
						$page_content = $request->request->get ( 'page_content' ) [$entity->lang_id];
						for($i = 0; $i < count ( $page_content) -1; $i = $i + 1) {
							$mapper->insert ( [
									'lang_id' => $entity->lang_id,
									'page_translation_id' => $entity->id,
									'page_id' => $entity->page_id,
									'content' => $page_content [$i] ["content"] != '' ? $page_content [$i] ["content"] : '<p></p>'
							] );
						}
						$counter = $counter + 1;
					} else {
						redirect ( ADMIN_PATH.'/pages?update=0' );
					}
				}
				// if languages are added after created content-->insert additional content instead of update
				if ($counter < count ( $request->request->get ( 'page_translation' ) )) {
					$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
					$counter2 = 0;
					foreach ( $request->request->get ( 'page_translation' ) as $lang_id => $content ) {
						if ($counter2 >= $counter) {
							$tid = $mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'page_id' => $id,
									'meta_title' => $content["meta_title"],
									'meta_description' => $content ["meta_description"],
									'meta_keywords' => $content ["meta_keywords"],
									'title' => $content ["title"]
							] );
							if ($tid) {
								$mapper = $this->db->mapper ( 'Entity\Page_Content' );
								for($i = 0; $i < count ( $page_content) -1 ; $i = $i + 1) {
									$mapper->insert ( [
											'lang_id' => intval ( $lang_id ),
											'page_translation_id' => $tid,
											'page_id' => $id,
											'content' => $request->request->get ( 'page_content' )[$lang_id] [$i] ["content"] != '' ? $request->request->get ( 'page_content' )[$lang_id] [$i] ["content"] : '<p></p>'
									] );
								}
							}
						}
						$counter2 =$counter2+1;
					}
				}
				// ---------------------------------------------------------------------------

				redirect ( ADMIN_PATH.'/pages?update=1' );
				break;
			case SUBFOLDER.ADMIN_PATH."/galleries/update/".$id :
				$mapper = $this->db->mapper ( 'Entity\Gallery' );
				$entity = $mapper->first ( ['id' => $id] );
				if ($entity) {
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					$entity->type = $request->request->get ('type');
					$entity->popup = $request->request->get ('popup');
					$entity->page_titles = $page_titles;
					$mapper->update($entity);
					redirect ( ADMIN_PATH.'/galleries?update=1' );
				}else {
					redirect ( ADMIN_PATH.'/galleries?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/sliders/update/".$id :
				$mapper = $this->db->mapper ( 'Entity\Slider' );
				$entity = $mapper->first ( ['id' => $id] );
				if ($entity) {
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					$entity->type = $request->request->get ('type');
					$entity->page_titles = $page_titles;
					$mapper->update($entity);
					redirect ( ADMIN_PATH.'/sliders?update=1' );
				}else {
					redirect ( ADMIN_PATH.'/sliders?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/translations/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Translation' );
				$entity = $mapper->first ( [
						'id' => $id,
				] );
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$entity->value = $request->request->get ( 'value' );
					$entity->lang_id = $request->request->get ( 'lang_id' );
					$entity->region_title = $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/translations?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/translations?update=0' );
				}
				break;
			case SUBFOLDER.ADMIN_PATH."/settings/update/" . $id :
				$mapper = $this->db->mapper ( 'Entity\Setting' );
				$entity = $mapper->first (['id' => $id]);
				if ($entity) {
					if($entity->title =='Active Theme'){
						$this->update_theme($request->request->get ('value'));
					}else if($entity->title =='Default Language'){

					}
					$entity->title = $request->request->get ( 'title' ) != NULL ? $request->request->get ( 'title' ) : $entity->title;
					$entity->value = $request->request->get ('value');
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/settings?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/settings?update=0' );
				}
				break;
		}
		return $response;
	}
	// --------------------------------------------------------------------------------
	public function install(Request $request, Response $response) {
		$this->db_migrations();
		$this->db_default_insertions();
		$this->update_theme($GLOBALS['active_theme']);
		$response->setContent ( "CMS Database Installed Successfully!" );
		return $response;
	}
	public function migrations(Request $request, Response $response) {
		$this->db_migrations();
		$response->setContent ( "Migrations Successfull!" );
		return $response;
	}
	public function default_insertions(Request $request, Response $response) {
		$this->db_default_insertions();
	    $response->setContent("Default Insertions Successfull!");
		return $response;
	}
	public function generate_active_theme(Request $request, Response $response) {
		$this->update_theme($GLOBALS['active_theme']);
		$response->setContent ( "Regions and Templates of Active Theme Generated Succesfully!" );
		return $response;
	}
	public function db_migrations(){
		$entities = get_classes('Entity');
		foreach ($entities as $entity) {
			$mapper = $this->db->mapper ($entity);
			$mapper->migrate();
		}
	}
	public function db_default_insertions(){
		$user = new User ();
		$user->insert_default ( $this->db );
		$language = new Language();
		$language->insert_default($this->db);
		$setting = new Setting();
		$setting->insert_default($this->db);
		$translation = new Translation();
		$translation->insert_default($this->db);
	}
	public function update_theme($theme){
		//Load all the templates from the theme and update the database records
		$templates = get_dir_file_names(VIEWS_THEME_PATH.'/'.$theme.'/templates');
		$mapper = $this->db->mapper ( 'Entity\Template' );
		$mapper->delete();
		foreach ( $templates as $template ) {
			$mapper->insert ( ['title' => $template] );
		}
		//Load all the regions from the theme and update the database records
		$regions = get_dir_file_names(VIEWS_THEME_PATH.'/'.$theme.'/regions');
		$mapper = $this->db->mapper ( 'Entity\Region' );
		$mapper->delete();
		foreach ( $regions as $region ) {
			$mapper->insert ( ['title' => $region] );
		}
	}
}
?>