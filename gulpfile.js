var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');
var php = require('gulp-connect-php');
var reload  = browserSync.reload;

/* Development and production directories*/
var appDir = 'theme/app';
var devDir = 'theme/dev';
var phpDir = 'cms';

gulp.task('browser-sync', function() {
	  browserSync({
	    server: {
	      baseDir: appDir
	    },
	  })
	})
	
/*sass task: compile Sass into CSS and reload browser*/
gulp.task('sass', function() {
	  return gulp.src(devDir+'/sass/**/*.scss') // Gets all files ending with .scss in app/scss
	    .pipe(sass())
	    .pipe(gulp.dest(appDir+'/css'))
	    .pipe(browserSync.reload({stream: true}))
});

gulp.task('html', function() {
	  return gulp.src(devDir+'/*.html')
	  .pipe(gulp.dest(appDir+'/'))
	  .pipe(browserSync.reload({stream: true}))
})

gulp.task('js', function() {
	  return gulp.src(devDir+'/js/**/*.js') 
	    .pipe(gulp.dest(appDir+'/js'))
	    .pipe(browserSync.reload({stream: true}))
});

/*img task: Optimizing Images*/

gulp.task('img', function(){
	  return gulp.src(devDir+'/img/**/*.+(png|jpg|jpeg|gif|svg)')
	  .pipe(cache(imagemin({
	      interlaced: true
	    })))
	  .pipe(gulp.dest(appDir+'/img'))
});

/*Migrations*/
gulp.task('migrate-html', function() {
	  return gulp.src(devDir+'/*.html')
	  .pipe(gulp.dest(appDir+'/'))
})
gulp.task('migrate-css', function() {
	  return gulp.src(devDir+'/css/**/*')
	  .pipe(gulp.dest(appDir+'/css'))
})
gulp.task('migrate-js', function() {
	  return gulp.src(devDir+'/js/**/*')
	  .pipe(gulp.dest(appDir+'/js'))
})
gulp.task('migrate-fonts', function() {
	  return gulp.src(devDir+'/fonts/**/*')
	  .pipe(gulp.dest(appDir+'/css/fonts'))
})

/*watch task: run tasks when html,js css changes occurs*/
gulp.task('migrate-all', ['migrate-html','migrate-css','migrate-js','migrate-fonts']);

/*watch task: run tasks when html,js css changes occurs*/
gulp.task('watch-theme', ['html','sass','js','img','browser-sync'], function (){
	  gulp.watch(devDir+'/sass/**/*.scss', ['sass']); 
	  gulp.watch(devDir+'/*.html', ['html']); 
	  gulp.watch(devDir+'/js/**/*.js', ['js']); 
});

gulp.task('webserver', function() {
    php.server({ base: phpDir, port: 8014, keepalive: true});
});

gulp.task('browser-sync-web', function() {
    browserSync({
        proxy: '127.0.0.1:8014',
        port: 8084,
        open: true,
        notify: false
    });
});
/*watch dynamic_templates task*/
gulp.task('watch-cms', ['webserver','browser-sync-web'], function (){
	gulp.watch(phpDir+'/**/*.php').on("change", reload);
});