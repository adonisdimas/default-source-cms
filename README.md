# README #

Default Source files for DProject website projects.

*theme*

To build the theme of the project you need nodejs and npm installed on your computer.
Open the command line and run the following in the root of the folder.

* `npm install` (to npm to your computer)
* `npm init` (to generate the package.json file)
* `npm install -i` (to install every dependency from the package.json file)

Development workflow:

* Run the gulp development server: `gulp watch-theme`

Frequest tasks

* Add bower dependency: run `bower install [depedency name] --save`, then run `bower-installer`. You can load your depedency in html with the path `"vendor/[depedency name]/somefile.js"`
* Remove bower dependency: delete the depedency from `bower.json`, run `gulp clean:bower`, the run the above commands.

*cms*

To build the cms of the project you need composer installed on your computer.
Open the command line and run the following commands in the root of the folder.

* `php composer.phar` (to install composer to your computer)
* `composer init` (to generate the composer.json file)

Development workflow:

* Run the gulp development server: `gulp watch-cms`

References:

* [npm](https://www.npmjs.com/)
* [gulpjs](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
* [bower](http://bower.io/#getting-started) 
* [bower-installer](https://github.com/blittle/bower-installer) 
* [sass](http://sass-lang.com/documentation/file.SASS_REFERENCE.html) 

